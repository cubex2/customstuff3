package cubex2.cs3.ingame.gui;

import cubex2.cs3.asm.export.Exporter;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.ingame.gui.control.Button;
import cubex2.cs3.ingame.gui.control.Control;
import cubex2.cs3.ingame.gui.control.Label;
import cubex2.cs3.ingame.gui.control.TextBox;
import cubex2.cs3.lib.Strings;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class WindowExportPack extends Window
{
    private final BaseContentPack pack;
    private Button btnExport;
    private TextBox tbVersion;
    private Label lblMessage;

    public WindowExportPack(BaseContentPack pack)
    {
        super("Export", CANCEL, 150, 150);
        this.pack = pack;

        Label label = label("Version").at(7, 7).add();
        tbVersion = textBox().below(label).fillWidth(7).add();
        tbVersion.setText("1.0.0");
        label = label(Strings.INFO_EXPORT).below(tbVersion).add();
        lblMessage = label("").below(label,14).add();

        btnExport = button("Export").left(7).bottom(7).add();
    }

    @Override
    protected void controlClicked(Control c, int mouseX, int mouseY)
    {
        if (c == btnExport)
        {
            try
            {
                Exporter.export(pack, StringUtils.deleteWhitespace(tbVersion.getText()));
                lblMessage.setText("Export successful!");
            } catch (IOException e)
            {
                lblMessage.setText("There was an error!");
                e.printStackTrace();
            }
        } else
        {
            super.controlClicked(c, mouseX, mouseY);
        }
    }
}
