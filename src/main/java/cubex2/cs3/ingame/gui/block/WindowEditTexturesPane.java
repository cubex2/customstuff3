package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.util.math.BlockPos;

public class WindowEditTexturesPane extends WindowEditTexturesBase
{
    public WindowEditTexturesPane(WrappedBlock block)
    {
        super(block, new String[]{"bottom"});

        worldDisplay.camZ = 0.75f;

        world.setBlock(new BlockPos(1, 0, 0), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(-1, 0, 0), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(0, 0, 1), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(0, 0, -1), wrappedBlock.block.getDefaultState());
    }
}
