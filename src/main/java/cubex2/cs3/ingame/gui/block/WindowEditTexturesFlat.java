package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.util.math.BlockPos;

public class WindowEditTexturesFlat extends WindowEditTexturesBase
{
    public WindowEditTexturesFlat(WrappedBlock block)
    {
        super(block, new String[]{"bottom"});

        world.setBlock(BlockPos.ORIGIN, world.getBlockState(BlockPos.ORIGIN).getBlock().getStateFromMeta(1));
    }
}
