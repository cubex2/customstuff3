package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.block.BlockCSFluid;
import cubex2.cs3.block.attributes.FluidAttributes;
import cubex2.cs3.common.WrappedBlock;
import cubex2.cs3.ingame.gui.common.WindowEditInteger;

public class WindowEditDensity extends WindowEditInteger
{
    private final WrappedBlock wrappedBlock;

    public WindowEditDensity(WrappedBlock wrappedBlock)
    {
        super("density", null, -100000, 100000, wrappedBlock.container);
        this.wrappedBlock = wrappedBlock;
    }

    @Override
    protected void applyChangedValue()
    {
        ((BlockCSFluid) wrappedBlock.block).getFluid().setDensity(((FluidAttributes) wrappedBlock.container).density);
    }
}
