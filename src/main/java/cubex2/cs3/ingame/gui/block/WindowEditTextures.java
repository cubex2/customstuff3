package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.util.math.BlockPos;

public class WindowEditTextures extends WindowEditTexturesBase
{
    private static final String[] textures = new String[]{"bottom", "top", "north", "south", "east", "west"};

    public WindowEditTextures(WrappedBlock block)
    {
        super(block, textures, true, true, true);

        world.setBlock(new BlockPos(0, 0, -1), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(1, 0, -1), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(1, 0, 0), wrappedBlock.block.getDefaultState());
    }
}
