package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.block.BlockCS;
import cubex2.cs3.common.WrappedBlock;
import cubex2.cs3.ingame.gui.control.Button;
import cubex2.cs3.ingame.gui.control.Control;
import cubex2.cs3.ingame.gui.control.DropBox;
import cubex2.cs3.ingame.gui.control.IStringProvider;
import cubex2.cs3.lib.StepSounds;
import net.minecraft.block.SoundType;
import net.minecraft.client.audio.PositionedSoundRecord;

public class WindowEditStepSound extends WindowEditBlockAttribute implements IStringProvider<SoundType>
{
    private DropBox<SoundType> dbSounds;
    private Button btnPlayPlace;
    private Button btnPlayBreak;
    private Button btnPlayStep;

    public WindowEditStepSound(WrappedBlock block)
    {
        super(block, "stepSound", 200, 100);

        dbSounds = dropBox(StepSounds.getAllSounds()).top(7).fillWidth(7).add();
        dbSounds.setStringProvider(this);
        dbSounds.setSelectedValue(container.stepSound);

        btnPlayPlace = button("Play Place").top(7).below(dbSounds).width(60).add();
        btnPlayPlace.playSound = false;

        btnPlayBreak = button("Play Break").rightTo(btnPlayPlace).width(60).add();
        btnPlayBreak.playSound = false;

        btnPlayStep = button("Play Step").rightTo(btnPlayBreak).width(60).add();
        btnPlayStep.playSound = false;
    }

    @Override
    protected void controlClicked(Control c, int mouseX, int mouseY)
    {
        if (c == btnPlayPlace)
        {
            mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(dbSounds.getSelectedValue().getPlaceSound(), 1f));
        } else if (c == btnPlayBreak)
        {
            mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(dbSounds.getSelectedValue().getBreakSound(), 1f));
        } else if (c == btnPlayStep)
        {
            mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(dbSounds.getSelectedValue().getStepSound(), 1f));
        } else
        {
            handleDefaultButtonClick(c);
        }
    }

    @Override
    protected void applyChanges()
    {
        container.stepSound = dbSounds.getSelectedValue();
        if (wrappedBlock.block instanceof BlockCS)
        {
            ((BlockCS) wrappedBlock.block).setSoundType(dbSounds.getSelectedValue());
        }
    }

    @Override
    public String getStringFor(SoundType value)
    {
        return StepSounds.getStepSoundName(value);
    }
}
