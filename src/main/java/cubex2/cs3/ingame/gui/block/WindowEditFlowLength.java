package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.block.BlockCSFluid;
import cubex2.cs3.block.attributes.FluidAttributes;
import cubex2.cs3.common.WrappedBlock;
import cubex2.cs3.ingame.gui.common.WindowEditInteger;

public class WindowEditFlowLength extends WindowEditInteger
{
    private final WrappedBlock wrappedBlock;

    public WindowEditFlowLength(WrappedBlock wrappedBlock)
    {
        super("flowLength", null, 0, 15, wrappedBlock.container);
        this.wrappedBlock = wrappedBlock;
    }

    @Override
    protected void applyChangedValue()
    {
        ((BlockCSFluid) wrappedBlock.block).setQuantaPerBlock(((FluidAttributes) wrappedBlock.container).flowLength);
    }
}
