package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.util.math.BlockPos;

public class WindowEditTexturesFence extends WindowEditTexturesBase
{
    public WindowEditTexturesFence(WrappedBlock block)
    {
        super(block, DEFAULT_TEXTURES, false, true, false);

        world.setBlock(BlockPos.ORIGIN, null);
        world.setBlock(new BlockPos(0, 0, -1), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(1, 0, -1), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(1, 0, 0), wrappedBlock.block.getDefaultState());
    }
}
