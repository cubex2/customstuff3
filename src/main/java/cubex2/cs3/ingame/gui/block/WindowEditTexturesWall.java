package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.util.math.BlockPos;

public class WindowEditTexturesWall extends WindowEditTexturesBase
{
    public WindowEditTexturesWall(WrappedBlock block)
    {
        super(block, DEFAULT_TEXTURES, false, true, false);

        worldDisplay.setCam(-0.75f, 1.5f, 2.0f);
        worldDisplay.setLook(0.5f, 0.5f, 0.5f);

        world.setBlock(new BlockPos(-1, 0, 0), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(0, 0, 1), wrappedBlock.block.getDefaultState());
    }
}
