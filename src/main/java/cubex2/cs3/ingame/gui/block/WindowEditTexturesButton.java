package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;

public class WindowEditTexturesButton extends WindowEditTexturesBase
{
    public WindowEditTexturesButton(WrappedBlock block)
    {
        super(block, DEFAULT_TEXTURES, false, true, false);
        worldDisplay.lookX = 0.5f;
        worldDisplay.setCam(0.25f, 0.75f, 1.0f);

        world.setBlock(new BlockPos(0, 0, -1), Blocks.BEDROCK.getStateFromMeta(3));
        world.setMetadata(BlockPos.ORIGIN, 3);
    }
}
