package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;

public class WindowEditTexturesLadder extends WindowEditTexturesBase
{
    public WindowEditTexturesLadder(WrappedBlock block)
    {
        super(block, new String[]{"bottom"});
        worldDisplay.setCam(0.25f, 1.0f, 0.75f);
        worldDisplay.setLook(0.5f, 0.5f, 0.0f);

        world.setBlock(new BlockPos(0, 0, -1), Blocks.BEDROCK.getDefaultState());
        world.setMetadata(BlockPos.ORIGIN, 3);

    }
}
