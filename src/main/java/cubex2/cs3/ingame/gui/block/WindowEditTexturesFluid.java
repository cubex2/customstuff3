package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.util.math.BlockPos;

public class WindowEditTexturesFluid extends WindowEditTexturesBase
{
    private static final String[] TEXTURE_NAMES = new String[]{"still", "flowing"};

    public WindowEditTexturesFluid(WrappedBlock block)
    {
        super(block, TEXTURE_NAMES, false, false, false, true);

        world.setBlock(new BlockPos(0, -1, 0), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(1, -1, 0), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(0, -1, -1), wrappedBlock.block.getDefaultState());
        world.setBlock(new BlockPos(0, -1, -1), world.getBlockState(BlockPos.ORIGIN).getBlock().getStateFromMeta(1));
        world.setBlock(BlockPos.ORIGIN, null);

        worldDisplay.setCam(0.5f, 1.5f, 1.5f);
        worldDisplay.setLook(0.5f, 0.5f, 0.5f);
    }
}
