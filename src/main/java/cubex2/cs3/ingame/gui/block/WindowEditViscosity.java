package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.block.BlockCSFluid;
import cubex2.cs3.block.attributes.FluidAttributes;
import cubex2.cs3.common.WrappedBlock;
import cubex2.cs3.ingame.gui.common.WindowEditInteger;

public class WindowEditViscosity extends WindowEditInteger
{
    private final WrappedBlock wrappedBlock;

    public WindowEditViscosity(WrappedBlock wrappedBlock)
    {
        super("viscosity", null, 0, 100000, wrappedBlock.container);
        this.wrappedBlock = wrappedBlock;
    }

    @Override
    protected void applyChangedValue()
    {
        ((BlockCSFluid) wrappedBlock.block).getFluid().setViscosity(((FluidAttributes) wrappedBlock.container).viscosity);
    }
}
