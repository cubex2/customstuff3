package cubex2.cs3.ingame.gui.block;

import cubex2.cs3.block.BlockCSFluid;
import cubex2.cs3.block.attributes.FluidAttributes;
import cubex2.cs3.common.WrappedBlock;
import cubex2.cs3.ingame.gui.common.WindowEditBoolean;

public class WindowEditGaseous extends WindowEditBoolean
{
    private final WrappedBlock wrappedBlock;

    public WindowEditGaseous(WrappedBlock wrappedBlock)
    {
        super("gaseous", "Is gaseous", wrappedBlock.container);
        this.wrappedBlock = wrappedBlock;
    }

    @Override
    protected void applyChangedValue()
    {
        ((BlockCSFluid) wrappedBlock.block).getFluid().setGaseous(((FluidAttributes) wrappedBlock.container).gaseous);
    }
}
