package cubex2.cs3.ingame.gui.control;

import cubex2.cs3.lib.Textures;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.ResourceLocation;

public class ButtonUpDown extends Control
{
    protected boolean hover;
    private boolean isUp = false;

    public ButtonUpDown(boolean isUp, Control parent)
    {
        this(isUp, null, 0, 0, parent);
    }

    public ButtonUpDown(boolean isUp, Anchor anchor, int offsetX, int offsetY, Control parent)
    {
        super(9, 9, anchor, offsetX, offsetY, parent);
        this.isUp = isUp;
    }

    @Override
    public void mouseDown(int mouseX, int mouseY, int button)
    {
        if (button == 0)
        {
            mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
        }
    }

    protected int getHoverState(boolean hover)
    {
        byte b0 = 1;

        if (!isEnabled())
        {
            b0 = 0;
        } else if (hover)
        {
            b0 = 2;
        }

        return b0;
    }

    @Override
    public void draw(int mouseX, int mouseY, float renderTick)
    {
        mc.renderEngine.bindTexture(Textures.CONTROLS);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        hover = isMouseOverControl(mouseX, mouseY);

        int k = getHoverState(hover);
        int u = isUp ? 200 : 209;
        drawTexturedModalRect(bounds.getX(), bounds.getY(), u, 18 + k * 9, 9, 9);
    }
}

