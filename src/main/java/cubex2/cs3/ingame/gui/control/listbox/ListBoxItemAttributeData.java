package cubex2.cs3.ingame.gui.control.listbox;

import cubex2.cs3.common.attribute.AttributeData;
import cubex2.cs3.ingame.gui.control.Anchor;
import cubex2.cs3.ingame.gui.control.Control;
import cubex2.cs3.ingame.gui.control.PictureBox;
import cubex2.cs3.lib.Textures;

public class ListBoxItemAttributeData extends ListBoxItemLabel<AttributeData>
{
    private PictureBox favBox;

    public ListBoxItemAttributeData(AttributeData value, int idx, int width, int height, Anchor anchor, int offsetX, int offsetY, Control parent)
    {
        super(value, idx, width, height, anchor, offsetX, offsetY, parent);

        favBox = pictureBox(Textures.CONTROLS, 200, value.isFavourite ? 81 : 104).size(14, 12).right(3).centerVert(1).add();
        favBox.setSrcSize(27, 23);
        favBox.setScale(0.5f);
    }

    @Override
    public boolean handleClick(int mouseX, int mouseY, int button)
    {
        if (favBox.isMouseOverControl(mouseX, mouseY))
        {
            value.switchFavourite();
            favBox.setUV(200, value.isFavourite ? 81 : 104);
            return true;
        }

        return false;
    }

    @Override
    public void draw(int mouseX, int mouseY, float renderTick)
    {
        favBox.setVisible(favBox.getBounds().contains(mouseX, mouseY));
        super.draw(mouseX, mouseY, renderTick);
    }
}
