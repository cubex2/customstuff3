package cubex2.cs3.ingame.gui.control;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class PictureBox extends Control
{
    private ResourceLocation texture;
    private int u;
    private int v;
    private int texWidth;
    private int texHeight;
    private float scale = 1f;

    public PictureBox(ResourceLocation texture, int u, int v, int width, int height, Anchor anchor, int offsetX, int offsetY, Control parent)
    {
        super(width, height, anchor, offsetX, offsetY, parent);
        this.texture = texture;
        this.u = u;
        this.v = v;
        texWidth = width;
        texHeight = height;
    }

    public void setTexture(ResourceLocation texture)
    {
        this.texture = texture;
    }

    public void setUV(int u, int v)
    {
        this.u = u;
        this.v = v;
    }

    public void setSrcSize(int w, int h)
    {
        texWidth = w;
        texHeight = h;
    }

    public void setScale(float scale)
    {
        this.scale = scale;
    }

    @Override
    public void draw(int mouseX, int mouseY, float renderTick)
    {
        int x = bounds.getX();
        int y = bounds.getY();

        float rx = (x / scale) - (int) (x / scale);
        float ry = (y / scale) - (int) (y / scale);

        mc.renderEngine.bindTexture(texture);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.scale(scale, scale, 1f);
        GlStateManager.translate(rx, ry, 0f);
        this.drawTexturedModalRect((int) (x / scale), (int) (y / scale)-1, u, v, texWidth, texHeight);
        GlStateManager.translate(-rx, -ry, 0f);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GlStateManager.scale(1 / scale, 1 / scale, 1f);
    }
}
