package cubex2.cs3.ingame.gui.control;

public interface IBlockDisplayRenderer
{
    void renderBlocks();
}
