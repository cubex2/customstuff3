package cubex2.cs3.ingame.gui.control;

import cubex2.cs3.lib.Color;
import cubex2.cs3.util.GuiHelper;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class PlayerDisplay extends Control
{
    private ItemStack equippedStack = ItemStack.EMPTY;
    private IPlayerDisplayPlayerModifier playerModifier;

    public PlayerDisplay(int width, int height, Anchor anchor, int offsetX, int offsetY, Control parent)
    {
        super(width, height, anchor, offsetX, offsetY, parent);
    }

    public void setPlayerModifier(IPlayerDisplayPlayerModifier value)
    {
        playerModifier = value;
    }

    public void setEquippedStack(ItemStack stack)
    {
        equippedStack = stack;
    }

    @Override
    public void draw(int mouseX, int mouseY, float renderTick)
    {
        GuiHelper.drawRect(getBounds(), Color.BLACK);

        int x = getX() + getWidth() / 2;
        int y = getY() + getHeight() / 2 + 30;

        NonNullList<ItemStack> mainInventory = mc.player.inventory.mainInventory;
        int currentItem = mc.player.inventory.currentItem;

        ItemStack curStack = mainInventory.get(currentItem);

        mainInventory.set(currentItem, equippedStack);
        if (playerModifier != null) playerModifier.preRender(this);
        GlStateManager.color(1f, 1f, 1f, 1f);
        GuiInventory.drawEntityOnScreen(x, y, 30, x - mouseX, y - 45 - mouseY, mc.player);
        if (playerModifier != null) playerModifier.postRender(this);
        mainInventory.set(currentItem, curStack);
    }
}
