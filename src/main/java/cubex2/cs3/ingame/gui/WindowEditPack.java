package cubex2.cs3.ingame.gui;

import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.ingame.gui.control.Button;
import cubex2.cs3.ingame.gui.control.Control;
import cubex2.cs3.ingame.gui.control.listbox.IListBoxItemClickListener;
import cubex2.cs3.ingame.gui.control.listbox.ListBox;
import cubex2.cs3.ingame.gui.control.listbox.ListBoxDescription;
import cubex2.cs3.registry.ContentRegistry;

public class WindowEditPack extends Window implements IListBoxItemClickListener<ContentRegistry>
{
    private BaseContentPack pack;
    private ListBox<ContentRegistry> listBox;

    private Button btnExport;

    public WindowEditPack(BaseContentPack pack)
    {
        super(pack.getName(), BACK, 180, 201);
        this.pack = pack;

        ListBoxDescription<ContentRegistry> desc = new ListBoxDescription<ContentRegistry>(7, 7);
        desc.rows = 12;
        desc.elements = pack.getRegistries();
        desc.canSelect = false;
        desc.sorted = true;
        listBox = listBox(desc).fillWidth(7).top(7).add();

        btnExport = button("Export").left(7).bottom(7).add();
        btnExport.setVisible(false);
    }

    @Override
    protected void controlClicked(Control c, int mouseX, int mouseY)
    {
        if (c == btnExport)
        {
            GuiBase.openWindow(new WindowExportPack(pack));
        } else
        {
            super.controlClicked(c, mouseX, mouseY);
        }
    }

    @Override
    public void itemClicked(ContentRegistry item, ListBox<ContentRegistry> listBox, int button)
    {
        if (button == 0)
        {
            GuiBase.openWindow(item.createListWindow());
        }
    }
}
