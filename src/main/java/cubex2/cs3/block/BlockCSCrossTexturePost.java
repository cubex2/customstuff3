package cubex2.cs3.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.state.IBlockState;

public class BlockCSCrossTexturePost extends BlockCSPost
{
    public BlockCSCrossTexturePost(WrappedBlock block)
    {
        super(block);
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }
}
