package cubex2.cs3.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.state.IBlockState;

public class BlockCSNormal extends BlockCS
{
    public BlockCSNormal(WrappedBlock block)
    {
        super(block);
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return true;
    }
}
