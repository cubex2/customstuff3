package cubex2.cs3.block;

import cubex2.cs3.block.attributes.BlockAttributes;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.EnumPushReaction;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;
import java.util.Random;

public class BlockCS extends Block implements IBlockCS
{
    protected BaseContentPack pack;
    protected WrappedBlock wrappedBlock;
    protected BlockAttributes container;

    public BlockCS(WrappedBlock block)
    {
        super(block.container.material);
        pack = block.getPack();
        wrappedBlock = block;
        container = wrappedBlock.container;
        setSoundType(container.stepSound);
    }

    public int getMetaForFlowerGen(int itemMeta)
    {
        return itemMeta;
    }

    public boolean onBonemeal(World world, BlockPos pos, EntityPlayer player)
    {
        return wrappedBlock.onBonemeal(world, pos, player);
    }

    @Override
    public WrappedBlock getWrappedBlock()
    {
        return wrappedBlock;
    }

    @Override
    public Block setSoundType(SoundType sound)
    {
        this.blockSoundType = sound;
        return this;
    }

    @Override
    public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
    {
        getWrappedBlock().updateTick(worldIn, pos, state, rand);
    }

    @Override
    public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state)
    {
        getWrappedBlock().onBlockAdded(worldIn, pos, state);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return getWrappedBlock() != null && getWrappedBlock().isOpaqueCube(state);
    }

    @Override
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return getWrappedBlock().shouldSideBeRendered(blockState, blockAccess, pos, side);
    }

    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return getWrappedBlock().hasTileEntity(state);
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state)
    {
        return getWrappedBlock().createTileEntity(world, state);
    }

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest)
    {
        boolean removed = world.setBlockToAir(pos);
        getWrappedBlock().removedByPlayer(world, player, pos);
        return removed;
    }

    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos)
    {
        getWrappedBlock().neighborChanged(state, worldIn, pos, blockIn);
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state)
    {
        getWrappedBlock().onBlockBreak(world, pos, state);
    }

    @Override
    public boolean eventReceived(IBlockState state, World worldIn, BlockPos pos, int id, int data)
    {
        return getWrappedBlock().onBlockEventReceived(worldIn, pos, state, id, data);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        return getWrappedBlock().blockActivated(worldIn, pos, state, playerIn, side, hitX, hitY, hitZ);
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
        getWrappedBlock().onBlockPlaced(world, pos, facing, hitX, hitY, hitZ, meta, placer);
        return getStateFromMeta(meta);
    }

    @Override
    public void onBlockClicked(World world, BlockPos pos, EntityPlayer player)
    {
        getWrappedBlock().onBlockClicked(world, pos, player);
    }

    @Override
    public void onEntityCollidedWithBlock(World world, BlockPos pos, IBlockState state, Entity entity)
    {
        getWrappedBlock().onEntityCollidedWithBlock(world, pos, state, entity);
    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase living, ItemStack stack)
    {
        getWrappedBlock().onBlockPlacedBy(world, pos, state, living, stack);
    }

    @Override
    public void onFallenUpon(World world, BlockPos pos, Entity entity, float fallDistance)
    {
        getWrappedBlock().onFallenUpon(world, pos, entity, fallDistance);
    }

    @Override
    public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand)
    {
        getWrappedBlock().randomDisplayTick(stateIn, worldIn, pos, rand);
    }

    @Override
    public boolean isWood(IBlockAccess world, BlockPos pos)
    {
        return getWrappedBlock().isWood(world, pos);
    }

    @Override
    public boolean canSustainLeaves(IBlockState state, IBlockAccess world, BlockPos pos)
    {
        return getWrappedBlock().canSustainLeaves(world, pos);
    }

    @Override
    public boolean canSilkHarvest(World world, BlockPos pos, IBlockState state, EntityPlayer player)
    {
        return getWrappedBlock().canSilkHarvest(world, pos, state, player);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
    {
        return getWrappedBlock().container.hasCollision ? blockState.getBoundingBox(worldIn, pos) : null;
    }

    @Override
    public int tickRate(World world)
    {
        return getWrappedBlock().tickRate(world);
    }

    @Override
    public int getLightValue(IBlockState state, IBlockAccess world, BlockPos pos)
    {
        return getWrappedBlock().getLightValue(world, pos);
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
    {
        return getWrappedBlock().getDrops(world, pos, state, fortune);
    }

    @Override
    public int getExpDrop(IBlockState state, IBlockAccess world, BlockPos pos, int fortune)
    {
        return getWrappedBlock().getExpDrop(world, pos, fortune);
    }

    @Override
    public int getFlammability(IBlockAccess world, BlockPos pos, EnumFacing face)
    {
        return getWrappedBlock().getFlammability(world, pos, face);
    }

    @Override
    public int getFireSpreadSpeed(IBlockAccess world, BlockPos pos, EnumFacing face)
    {
        return getWrappedBlock().getFireSpreadSpeed(world, pos, face);
    }

    @Override
    public boolean canPlaceBlockOnSide(World world, BlockPos pos, EnumFacing side)
    {
        return canPlaceBlockAt(world, pos) && getWrappedBlock().canPlaceBlockOnSide(world, pos, side);
    }

    @Override
    public float getBlockHardness(IBlockState state, World world, BlockPos pos)
    {
        return getWrappedBlock().getHardness(state, world, pos);
    }

    @Override
    public float getExplosionResistance(World world, BlockPos pos, Entity exploder, Explosion explosion)
    {
        return getWrappedBlock().getExplosionResistance(world, pos, exploder, explosion);
    }

    @Override
    public boolean isBurning(IBlockAccess world, BlockPos pos)
    {
        return getWrappedBlock().isBurning(world, pos);
    }

    @Override
    public boolean isBeaconBase(IBlockAccess worldObj, BlockPos pos, BlockPos bpos)
    {
        return getWrappedBlock().isBeaconBase(worldObj, pos, bpos);
    }

    @Override
    public boolean isFireSource(World world, BlockPos pos, EnumFacing side)
    {
        return getWrappedBlock().isFireSource(world, pos, side);
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
    {
        return getWrappedBlock().getPickBlock(target, world, pos);
    }

    @Override
    public EnumPushReaction getMobilityFlag(IBlockState state)
    {
        EnumPushReaction mobility = getWrappedBlock().getMobilityFlag();
        return mobility != null ? mobility : blockMaterial.getMobilityFlag();
    }

    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return getWrappedBlock().getBlockLayer();
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn()
    {
        return getWrappedBlock().getCreativeTabToDisplayOn();
    }
}
