package cubex2.cs3.block;

import cubex2.cs3.block.attributes.TorchAttributes;
import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Iterator;
import java.util.Random;

import static net.minecraft.util.EnumFacing.*;

public class BlockCSTorch extends BlockCS
{
    public static final PropertyDirection FACING = PropertyDirection.create("facing");
    protected static final AxisAlignedBB STANDING_AABB = new AxisAlignedBB(0.4D, 0.0D, 0.4D, 0.6D, 0.6D, 0.6D);
    protected static final AxisAlignedBB HANGING_AABB = new AxisAlignedBB(0.4D, 0.4D, 0.4D, 0.6D, 1D, 0.6D);
    protected static final AxisAlignedBB TORCH_NORTH_AABB = new AxisAlignedBB(0.35D, 0.2D, 0.7D, 0.65D, 0.8D, 1.0D);
    protected static final AxisAlignedBB TORCH_SOUTH_AABB = new AxisAlignedBB(0.35D, 0.2D, 0.0D, 0.65D, 0.8D, 0.3D);
    protected static final AxisAlignedBB TORCH_WEST_AABB = new AxisAlignedBB(0.7D, 0.2D, 0.35D, 1.0D, 0.8D, 0.65D);
    protected static final AxisAlignedBB TORCH_EAST_AABB = new AxisAlignedBB(0.0D, 0.2D, 0.35D, 0.3D, 0.8D, 0.65D);

    private TorchAttributes container;

    public BlockCSTorch(WrappedBlock block)
    {
        super(block);
        setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.UP));
        container = (TorchAttributes) block.container;

        setTickRandomly(true);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
    {
        return NULL_AABB;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        switch (state.getValue(FACING))
        {
            case EAST:
                return TORCH_EAST_AABB;
            case WEST:
                return TORCH_WEST_AABB;
            case SOUTH:
                return TORCH_SOUTH_AABB;
            case NORTH:
                return TORCH_NORTH_AABB;
            case DOWN:
                return HANGING_AABB;
            default:
                return STANDING_AABB;
        }
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    private boolean canPlaceTorchOn(World world, BlockPos pos)
    {
        if (!container.canPlaceOnFloor)
            return false;
        if (world.isSideSolid(pos, UP))
            return true;
        else
        {
            IBlockState state = world.getBlockState(pos);
            Block block = world.getBlockState(pos).getBlock();

            return block.canPlaceTorchOnTop(state, world, pos);
        }
    }

    @Override
    public boolean canPlaceBlockAt(World world, BlockPos pos)
    {
        /*boolean floor = canPlaceTorchOn(world, pos.down()) && container.canPlaceOnFloor;
        boolean wall = (world.isSideSolid(pos.west(), EAST)
                || world.isSideSolid(pos.east(), WEST)
                || world.isSideSolid(pos.north(), SOUTH)
                || world.isSideSolid(pos.south(), NORTH)) && container.canPlaceOnWall;
        boolean ceiling = world.isSideSolid(pos.up(), DOWN) && container.canPlaceOnCeiling;

        return floor || wall || ceiling;*/
        Iterator iterator = FACING.getAllowedValues().iterator();
        EnumFacing facing;

        do
        {
            if (!iterator.hasNext())
            {
                return false;
            }

            facing = (EnumFacing) iterator.next();
        }
        while (!this.canPlaceAt(world, pos, facing));

        return true;
    }


    public boolean canPlaceAt(World world, BlockPos pos, EnumFacing side)
    {
        if (side == UP && !container.canPlaceOnFloor)
            return false;
        if (side == DOWN && !container.canPlaceOnCeiling)
            return false;
        if (side.getAxis().isHorizontal() && !container.canPlaceOnWall)
            return false;

        BlockPos pos1 = pos.offset(side.getOpposite());
        return world.isSideSolid(pos1, side, true) || side == UP && canPlaceTorchOn(world, pos1);
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int metadata, EntityLivingBase placer)
    {
        super.getStateForPlacement(world, pos, facing, hitX, hitY, hitZ, metadata, placer);

        if (canPlaceAt(world, pos, facing))
        {
            return getDefaultState().withProperty(FACING, facing);
        } else
        {
            Iterator iterator = EnumFacing.Plane.HORIZONTAL.iterator();
            EnumFacing facing1;

            do
            {
                if (!iterator.hasNext())
                {
                    return this.getDefaultState();
                }

                facing1 = (EnumFacing) iterator.next();
            }
            while (!world.isSideSolid(pos.offset(facing1.getOpposite()), facing1, true));

            return this.getDefaultState().withProperty(FACING, facing1);
        }

       /* if (container.canPlaceOnCeiling && facing == DOWN && world.isSideSolid(pos.up(), DOWN))
        {
            return getDefaultState().withProperty(FACING, DOWN);
        }

        if (container.canPlaceOnFloor && facing == UP && this.canPlaceTorchOn(world, pos.down()))
        {
            return getDefaultState().withProperty(FACING, UP);
        }

        if (container.canPlaceOnWall)
        {
            if (facing == NORTH && world.isSideSolid(pos.south(), NORTH))
            {
                return getDefaultState().withProperty(FACING, NORTH);
            }

            if (facing == SOUTH && world.isSideSolid(pos.north(), SOUTH))
            {
                return getDefaultState().withProperty(FACING, SOUTH);
            }

            if (facing == WEST && world.isSideSolid(pos.east(), WEST))
            {
                return getDefaultState().withProperty(FACING, WEST);
            }

            if (facing == EAST && world.isSideSolid(pos.west(), EAST))
            {
                return getDefaultState().withProperty(FACING, EAST);
            }
        }
        return getDefaultState();*/
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random random)
    {
        super.updateTick(world, pos, state, random);

        /*if (world.getBlockMetadata(pos) == 0)
        {
            setMetadata(world, pos);
        }*/
    }

    @Override
    public void onBlockAdded(World world, BlockPos pos, IBlockState state)
    {
        super.onBlockAdded(world, pos, state);

        /*if (world.getBlockMetadata(x, y, z) == 0)
        {
            setMetadata(world, x, y, z);
        }*/

        this.dropTorchIfCantStay(world, pos);
    }

    /*private void setMetadata(World world, BlockPos pos)
    {
        if (container.canPlaceOnWall)
            if (world.isSideSolid(x - 1, y, z, EAST))
            {
                world.setBlockMetadataWithNotify(x, y, z, 1, 2);
            } else if (world.isSideSolid(x + 1, y, z, WEST))
            {
                world.setBlockMetadataWithNotify(x, y, z, 2, 2);
            } else if (world.isSideSolid(x, y, z - 1, SOUTH))
            {
                world.setBlockMetadataWithNotify(x, y, z, 3, 2);
            } else if (world.isSideSolid(x, y, z + 1, NORTH))
            {
                world.setBlockMetadataWithNotify(x, y, z, 4, 2);
            }

        if (container.canPlaceOnFloor && this.canPlaceTorchOn(world, x, y - 1, z))
        {
            world.setBlockMetadataWithNotify(x, y, z, 5, 2);
        }

        if (container.canPlaceOnCeiling && world.isSideSolid(x, y + 1, z, DOWN))
        {
            world.setBlockMetadataWithNotify(x, y, z, 6, 2);
        }

        this.dropTorchIfCantStay(world, x, y, z);
    }*/

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block neighborBlock, BlockPos fromPos)
    {
        super.neighborChanged(state, world, pos, neighborBlock, fromPos);

        if (this.dropTorchIfCantStay(world, pos))
        {
            EnumFacing facing = state.getValue(FACING);
            boolean removeTorch = false;

            if ((!world.isSideSolid(pos.west(), EAST) || !container.canPlaceOnWall) && facing == EAST)
            {
                removeTorch = true;
            }

            if ((!world.isSideSolid(pos.east(), WEST) || !container.canPlaceOnWall) && facing == WEST)
            {
                removeTorch = true;
            }

            if ((!world.isSideSolid(pos.north(), SOUTH) || !container.canPlaceOnWall) && facing == SOUTH)
            {
                removeTorch = true;
            }

            if ((!world.isSideSolid(pos.south(), NORTH) || !container.canPlaceOnWall) && facing == NORTH)
            {
                removeTorch = true;
            }

            if ((!this.canPlaceTorchOn(world, pos.down()) || !container.canPlaceOnFloor) && facing == UP)
            {
                removeTorch = true;
            }

            if ((!world.isSideSolid(pos.up(), DOWN) || !container.canPlaceOnCeiling) && facing == DOWN)
            {
                removeTorch = true;
            }

            if (removeTorch)
            {
                this.dropBlockAsItem(world, pos, state, 0);
                world.setBlockToAir(pos);
            }
        }
    }

    private boolean dropTorchIfCantStay(World world, BlockPos pos)
    {
        if (!this.canPlaceBlockAt(world, pos))
        {
            IBlockState state = world.getBlockState(pos);
            if (state.getBlock() == this)
            {
                this.dropBlockAsItem(world, pos, state, 0);
                world.setBlockToAir(pos);
            }

            return false;
        } else
            return true;
    }

    @Override
    public void randomDisplayTick(IBlockState state, World world, BlockPos pos, Random rand)
    {
        if (container.particles)
        {
            EnumFacing facing = state.getValue(FACING);

            double posX = (double) ((float) pos.getX() + 0.5F);
            double posY = (double) ((float) pos.getY() + 0.7F);
            double posY2 = (double) ((float) pos.getY() + 0.4F);
            double posZ = (double) ((float) pos.getZ() + 0.5F);
            double d3 = 0.2199999988079071D;
            double d4 = 0.27000001072883606D;

            if (facing == EAST)
            {
                world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, posX - d4, posY + d3, posZ, 0.0D, 0.0D, 0.0D);
                world.spawnParticle(EnumParticleTypes.FLAME, posX - d4, posY + d3, posZ, 0.0D, 0.0D, 0.0D);
            } else if (facing == WEST)
            {
                world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, posX + d4, posY + d3, posZ, 0.0D, 0.0D, 0.0D);
                world.spawnParticle(EnumParticleTypes.FLAME, posX + d4, posY + d3, posZ, 0.0D, 0.0D, 0.0D);
            } else if (facing == SOUTH)
            {
                world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, posX, posY + d3, posZ - d4, 0.0D, 0.0D, 0.0D);
                world.spawnParticle(EnumParticleTypes.FLAME, posX, posY + d3, posZ - d4, 0.0D, 0.0D, 0.0D);
            } else if (facing == NORTH)
            {
                world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, posX, posY + d3, posZ + d4, 0.0D, 0.0D, 0.0D);
                world.spawnParticle(EnumParticleTypes.FLAME, posX, posY + d3, posZ + d4, 0.0D, 0.0D, 0.0D);
            } else if (facing == UP)
            {
                world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, posX, posY, posZ, 0.0D, 0.0D, 0.0D);
                world.spawnParticle(EnumParticleTypes.FLAME, posX, posY, posZ, 0.0D, 0.0D, 0.0D);
            } else
            {
                world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, posX, posY2, posZ, 0.0D, 0.0D, 0.0D);
                world.spawnParticle(EnumParticleTypes.FLAME, posX, posY2, posZ, 0.0D, 0.0D, 0.0D);
            }
        }
    }

    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public IBlockState getStateFromMeta(int meta)
    {
        return getDefaultState().withProperty(FACING, EnumFacing.getFront(meta - 6));
    }

    @Override
    public int getMetaFromState(IBlockState state)
    {
        EnumFacing facing = state.getValue(FACING);
        return 6 - facing.ordinal();
    }

    @Override
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, FACING);
    }
}
