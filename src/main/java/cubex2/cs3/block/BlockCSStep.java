package cubex2.cs3.block;

import cubex2.cs3.block.attributes.StepAttributes;
import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockCSStep extends BlockCS
{
    public static final PropertyEnum<BlockSlab.EnumBlockHalf> HALF = PropertyEnum.<BlockSlab.EnumBlockHalf>create("half", BlockSlab.EnumBlockHalf.class);
    protected static final AxisAlignedBB AABB_BOTTOM_HALF = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D);
    protected static final AxisAlignedBB AABB_TOP_HALF = new AxisAlignedBB(0.0D, 0.5D, 0.0D, 1.0D, 1.0D, 1.0D);

    private StepAttributes container;

    public BlockCSStep(WrappedBlock block)
    {
        super(block);
        container = (StepAttributes) block.container;
        useNeighborBrightness = true;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return state.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP ? AABB_TOP_HALF : AABB_BOTTOM_HALF;
    }

    public boolean isFullyOpaque(IBlockState state)
    {
        return state.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean doesSideBlockRendering(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing face)
    {
        if (state.isOpaqueCube())
            return true;

        BlockSlab.EnumBlockHalf side = state.getValue(HALF);
        return (side == BlockSlab.EnumBlockHalf.TOP && face == EnumFacing.UP) || (side == BlockSlab.EnumBlockHalf.BOTTOM && face == EnumFacing.DOWN);
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int metadata, EntityLivingBase placer)
    {
        IBlockState iblockstate = super.getStateForPlacement(world, pos, facing, hitX, hitY, hitZ, metadata, placer).withProperty(HALF, BlockSlab.EnumBlockHalf.BOTTOM);
        return facing != EnumFacing.DOWN && (facing == EnumFacing.UP || (double) hitY <= 0.5D) ? iblockstate : iblockstate.withProperty(HALF, BlockSlab.EnumBlockHalf.TOP);
    }


    @Override
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess world, BlockPos pos, EnumFacing side)
    {
        if (side != EnumFacing.UP && side != EnumFacing.DOWN && !wrappedBlock.shouldSideBeRenderedDefault(blockState, world, pos, side))
        {
            return false;
        } else if (false)
        {
            BlockPos pos1 = pos.offset(side.getOpposite());
            IBlockState state = world.getBlockState(pos);
            IBlockState state1 = world.getBlockState(pos1);
            boolean flag = isSlab(state.getBlock()) && state.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP;
            boolean flag1 = isSlab(state1.getBlock()) && state1.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP;
            return flag1 ? (side == EnumFacing.DOWN || (side == EnumFacing.UP && wrappedBlock.shouldSideBeRenderedDefault(blockState, world, pos, side) || (!isSlab(state.getBlock()) || !flag)))
                         : (side == EnumFacing.UP || (side == EnumFacing.DOWN && wrappedBlock.shouldSideBeRenderedDefault(blockState, world, pos, side) || (!isSlab(state.getBlock()) || flag)));
        }
        return super.shouldSideBeRendered(blockState, world, pos, side);
    }

    @SideOnly(Side.CLIENT)
    protected static boolean isSlab(Block blockIn)
    {
        return blockIn == Blocks.STONE_SLAB || blockIn == Blocks.WOODEN_SLAB || blockIn == Blocks.STONE_SLAB2 || blockIn == Blocks.PURPUR_SLAB || blockIn instanceof BlockCSStep;
    }

    @Override
    public int damageDropped(IBlockState state)
    {
        return super.damageDropped(state) & 7;
    }

    public Block getDoubleSlabBlock(int meta)
    {
        return container.doubleSlabBlock;
    }

    public int getDoubleSlabMeta(int meta)
    {
        return container.doubleSlabMeta;
    }

    @Override
    public boolean canCreatureSpawn(IBlockState state, IBlockAccess world, BlockPos pos, EntityLiving.SpawnPlacementType type)
    {
        return state.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP;
    }

    public IBlockState getStateFromMeta(int meta)
    {
        IBlockState iblockstate = this.getDefaultState();

        iblockstate = iblockstate.withProperty(HALF, (meta & 8) == 0 ? BlockSlab.EnumBlockHalf.BOTTOM : BlockSlab.EnumBlockHalf.TOP);


        return iblockstate;
    }

    /**
     * Convert the BlockState into the correct metadata value
     */
    public int getMetaFromState(IBlockState state)
    {
        int i = 0;

        if (state.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP)
        {
            i |= 8;
        }

        return i;
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, HALF);
    }
}
