package cubex2.cs3.block;

import cubex2.cs3.block.attributes.GravityAttributes;
import cubex2.cs3.common.WrappedBlock;
import cubex2.cs3.entity.EntityCSGravityBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Random;

public class BlockCSGravity extends BlockCS
{
    private GravityAttributes container;

    public BlockCSGravity(WrappedBlock block)
    {
        super(block);
        container = (GravityAttributes) block.container;
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return true;
    }

    @Override
    public void onBlockAdded(World world, BlockPos pos, IBlockState state)
    {
        super.onBlockAdded(world, pos, state);
        world.scheduleUpdate(pos, this, tickRate(world));
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block neighborBlock, BlockPos fromPos)
    {
        super.neighborChanged(state, world, pos, neighborBlock, fromPos);
        world.scheduleUpdate(pos, this, tickRate(world));
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand)
    {
        super.updateTick(world, pos, state, rand);
        if (!world.isRemote)
        {
            this.tryToApplyGravity(world, pos, state);
        }
    }

    private void tryToApplyGravity(World world, BlockPos pos, IBlockState state)
    {
        if (canFallAt(world, container.hasAntiGravity ? pos.up() : pos.down()) && pos.getY() >= 0 && pos.getY() <= 255)
        {
            byte b0 = 32;
            if (world.isAreaLoaded(pos.add(-b0, -b0, -b0), pos.add(b0, b0, b0)))
            {
                if (!world.isRemote)
                {
                    EntityCSGravityBlock gravityBlock = new EntityCSGravityBlock(world, pos.getX() + 0.5f, pos.getY() + 0.5f, pos.getZ() + 0.5f, this, state);
                    world.spawnEntity(gravityBlock);
                }
            }
        }
    }

    public static boolean canFallAt(World world, BlockPos pos)
    {
        if (world.isAirBlock(pos)) return true;
        IBlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        Material material = state.getMaterial();
        return block == Blocks.FIRE || material == Material.AIR || material == Material.WATER || material == Material.LAVA;
    }
}
