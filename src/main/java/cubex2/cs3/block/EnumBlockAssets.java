package cubex2.cs3.block;

public enum EnumBlockAssets
{
    BUTTON(EnumBlockType.BUTTON, "button", "ib:button:", "", "pressed", "inventory"),
    CARPET(EnumBlockType.CARPET, "normal", "ib::", "carpet", new String[]{""}),
    //CHEST(EnumBlockType.CHEST, "chest"),
    CROSS_TEXTURE(EnumBlockType.CROSS_TEXTURE, "normal", null, "cross_texture", new String[]{""}),
    CROSS_TEXTURE_POST(EnumBlockType.CROSS_TEXTURE_POST, "post", null),
    DOOR(EnumBlockType.DOOR, "door", null, "bottom", "bottom_rh", "top", "top_rh"),
    FACING(EnumBlockType.FACING, "facing", "ib::"),
    FENCE(EnumBlockType.FENCE, "fence", "ib:fence:", "post", "n", "ne", "ns", "nse", "nsew", "inventory"),
    FENCE_GATE(EnumBlockType.FENCE_GATE, "fence_gate", "ib:fence_gate:", "closed", "open", "wall_closed", "wall_open"),
    FLAT(EnumBlockType.FLAT, "post", "ib::", "flat", new String[]{""}),
    FLUID(EnumBlockType.FLUID, null, null),
    //FURNACE(EnumBlockType.FURNACE, "furnace"),
    GRAVITY(EnumBlockType.GRAVITY, "normal", "ib::", ""),
    LADDER(EnumBlockType.LADDER, "ladder", null, ""),
    NORMAL(EnumBlockType.NORMAL, "normal", "ib::", ""),
    PANE(EnumBlockType.PANE, "pane", null, "n", "ne", "ns", "nse", "nsew"),
    POST(EnumBlockType.POST, "post", "ib::"),
    PRESSURE_PLATE(EnumBlockType.PRESSURE_PLATE, "pressure_plate", "ib::_inventory", "up", "down", "inventory"),
    SLAB(EnumBlockType.SLAB, "slab", "ib::_bottom", "top", "bottom"),
    //SLOPE(EnumBlockType.SLOPE, "slope"),
    STAIRS(EnumBlockType.STAIRS, "stairs", "ib:stairs:", "", "inner", "outer"),
    TORCH(EnumBlockType.TORCH, "torch", null, "", "wall"),
    TRAP_DOOR(EnumBlockType.TRAP_DOOR, "trapdoor", "ib::_bottom", "bottom", "top", "open"),
    WALL(EnumBlockType.WALL, "wall", "ib::_inventory", "post", "n", "ne", "ns", "nse", "nsew", "ns_above", "inventory"),
    WHEAT(EnumBlockType.WHEAT, "normal", "ib::", "wheat", new String[]{""});

    public final EnumBlockType blockType;
    public final String stateName;
    public final String item;
    public final String modelName;
    public final String[] models;

    EnumBlockAssets(EnumBlockType blockType, String stateName, String item, String... models)
    {
        this(blockType, stateName, item, stateName, models);
    }

    EnumBlockAssets(EnumBlockType blockType, String stateName, String item, String modelName, String[] models)
    {
        this.blockType = blockType;
        this.stateName = stateName;
        this.item = item;
        this.modelName = modelName;
        this.models = models;

        if (item != null && !item.matches("ib:.*:.*"))
            throw new IllegalArgumentException("item");
    }

    public static EnumBlockAssets fromBlockType(EnumBlockType type)
    {
        return valueOf(type.name());
    }
}
