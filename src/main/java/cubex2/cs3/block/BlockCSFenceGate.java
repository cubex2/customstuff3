package cubex2.cs3.block;

import cubex2.cs3.block.attributes.FenceGateAttributes;
import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockWall;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockCSFenceGate extends BlockCS
{
    public static final PropertyDirection FACING = PropertyDirection.create("facing", EnumFacing.Plane.HORIZONTAL);
    public static final PropertyBool OPEN = PropertyBool.create("open");
    public static final PropertyBool POWERED = PropertyBool.create("powered");
    public static final PropertyBool IN_WALL = PropertyBool.create("in_wall");
    protected static final AxisAlignedBB AABB_COLLIDE_ZAXIS = new AxisAlignedBB(0.0D, 0.0D, 0.375D, 1.0D, 1.0D, 0.625D);
    protected static final AxisAlignedBB AABB_COLLIDE_XAXIS = new AxisAlignedBB(0.375D, 0.0D, 0.0D, 0.625D, 1.0D, 1.0D);
    protected static final AxisAlignedBB AABB_COLLIDE_ZAXIS_INWALL = new AxisAlignedBB(0.0D, 0.0D, 0.375D, 1.0D, 0.8125D, 0.625D);
    protected static final AxisAlignedBB AABB_COLLIDE_XAXIS_INWALL = new AxisAlignedBB(0.375D, 0.0D, 0.0D, 0.625D, 0.8125D, 1.0D);
    protected static final AxisAlignedBB AABB_CLOSED_SELECTED_ZAXIS = new AxisAlignedBB(0.0D, 0.0D, 0.375D, 1.0D, 1.5D, 0.625D);
    protected static final AxisAlignedBB AABB_CLOSED_SELECTED_XAXIS = new AxisAlignedBB(0.375D, 0.0D, 0.0D, 0.625D, 1.5D, 1.0D);

    private FenceGateAttributes container;

    public BlockCSFenceGate(WrappedBlock block)
    {
        super(block);
        container = (FenceGateAttributes) block.container;
        setDefaultState(this.blockState.getBaseState()
                                       .withProperty(OPEN, false)
                                       .withProperty(POWERED, false)
                                       .withProperty(IN_WALL, false));
    }

    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        state = this.getActualState(state, source, pos);
        return state.getValue(IN_WALL) ? (state.getValue(FACING).getAxis() == EnumFacing.Axis.X ? AABB_COLLIDE_XAXIS_INWALL : AABB_COLLIDE_ZAXIS_INWALL) : (state.getValue(FACING).getAxis() == EnumFacing.Axis.X ? AABB_COLLIDE_XAXIS : AABB_COLLIDE_ZAXIS);
    }

    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos)
    {
        EnumFacing.Axis axis = state.getValue(FACING).getAxis();

        if (axis == EnumFacing.Axis.Z && (isWallAt(worldIn, pos.west()) || isWallAt(worldIn, pos.east())) || axis == EnumFacing.Axis.X && (isWallAt(worldIn, pos.north()) || isWallAt(worldIn, pos.south())))
        {
            state = state.withProperty(IN_WALL, true);
        }

        return state;
    }

    private boolean isWallAt(IBlockAccess world, BlockPos pos)
    {
        return world.getBlockState(pos).getBlock() instanceof BlockCSWall
                || world.getBlockState(pos).getBlock() instanceof BlockWall;
    }

    @Override
    public boolean canPlaceBlockAt(World world, BlockPos pos)
    {
        return world.getBlockState(pos.down()).getMaterial().isSolid() && super.canPlaceBlockAt(world, pos);
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block neighborBlock, BlockPos fromPos)
    {
        super.neighborChanged(state, world, pos, neighborBlock, fromPos);
        if (!world.isRemote)
        {
            boolean flag = world.isBlockPowered(pos);

            if (flag || neighborBlock.getDefaultState().canProvidePower())
            {
                if (flag && !state.getValue(OPEN) && !state.getValue(POWERED))
                {
                    world.setBlockState(pos, state.withProperty(OPEN, true).withProperty(POWERED, true), 2);
                    world.playEvent(null, 1008, pos, 0);
                } else if (!flag && state.getValue(OPEN) && state.getValue(POWERED))
                {
                    world.setBlockState(pos, state.withProperty(OPEN, false).withProperty(POWERED, false), 2);
                    world.playEvent(null, 1014, pos, 0);
                } else if (flag != state.getValue(POWERED))
                {
                    world.setBlockState(pos, state.withProperty(POWERED, flag), 2);
                }
            }
        }
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        super.onBlockActivated(world, pos, state, player, hand, facing, hitX, hitY, hitZ);
        if (container.redstoneOnly)
            return false;
        if (state.getValue(OPEN))
        {
            state = state.withProperty(OPEN, false);
            world.setBlockState(pos, state, 2);
        } else
        {
            EnumFacing enumfacing1 = EnumFacing.fromAngle((double) player.rotationYaw);

            if (state.getValue(FACING) == enumfacing1.getOpposite())
            {
                state = state.withProperty(FACING, enumfacing1);
            }

            state = state.withProperty(OPEN, true);
            world.setBlockState(pos, state, 2);
        }

        world.playEvent(player, state.getValue(OPEN) ? 1008 : 1014, pos, 0);
        return true;
    }

    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
        return this.getDefaultState().withProperty(FACING, placer.getHorizontalFacing()).withProperty(OPEN, false).withProperty(POWERED, false).withProperty(IN_WALL, false);
    }

    @Nullable
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
    {
        return !container.hasCollision || blockState.getValue(OPEN) ? NULL_AABB : (blockState.getValue(FACING).getAxis() == EnumFacing.Axis.Z ? AABB_CLOSED_SELECTED_ZAXIS : AABB_CLOSED_SELECTED_XAXIS);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isPassable(IBlockAccess world, BlockPos pos)
    {
        return world.getBlockState(pos).getValue(OPEN);
    }

    public static boolean isFenceGateOpen(int md)
    {
        return (md & 4) != 0;
    }

    public IBlockState getStateFromMeta(int meta)
    {
        return this.getDefaultState()
                   .withProperty(FACING, EnumFacing.getHorizontal(meta))
                   .withProperty(OPEN, (meta & 4) != 0)
                   .withProperty(POWERED, (meta & 8) != 0);
    }

    /**
     * Convert the BlockState into the correct metadata value
     */
    public int getMetaFromState(IBlockState state)
    {
        byte b0 = 0;
        int i = b0 | state.getValue(FACING).getHorizontalIndex();

        if (state.getValue(POWERED))
        {
            i |= 8;
        }

        if (state.getValue(OPEN))
        {
            i |= 4;
        }

        return i;
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, FACING, OPEN, POWERED, IN_WALL);
    }
}
