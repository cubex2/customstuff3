package cubex2.cs3.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.state.IBlockState;

public class BlockCSCrossTexture extends BlockCS
{
    public BlockCSCrossTexture(WrappedBlock block)
    {
        super(block);
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }
}
