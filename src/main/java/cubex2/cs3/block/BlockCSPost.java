package cubex2.cs3.block;

import cubex2.cs3.block.attributes.PostAttributes;
import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class BlockCSPost extends BlockCSFacing
{
    protected PostAttributes container;

    private final AxisAlignedBB BOUNDS_Y;
    private final AxisAlignedBB BOUNDS_Z;
    private final AxisAlignedBB BOUNDS_X;

    public BlockCSPost(WrappedBlock block)
    {
        super(block);
        container = (PostAttributes) block.container;

        float thickness = container.thickness;
        BOUNDS_Y = new AxisAlignedBB(0.5f - thickness / 2, 0.0f, 0.5f - thickness / 2, 0.5f + thickness / 2, 1.0f, 0.5f + thickness / 2);
        BOUNDS_Z = new AxisAlignedBB(0.5f - thickness / 2, 0.5f - thickness / 2, 0.0f, 0.5f + thickness / 2, 0.5f + thickness / 2, 1.0f);
        BOUNDS_X = new AxisAlignedBB(0.0f, 0.5f - thickness / 2, 0.5f - thickness / 2, 1.0f, 0.5f + thickness / 2, 0.5f + thickness / 2);
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        EnumFacing facing = state.getValue(FACING);

        if (facing.getAxis() == EnumFacing.Axis.Y)
        {
            return BOUNDS_Y;
        } else if (facing.getAxis() == EnumFacing.Axis.Z)
        {
            return BOUNDS_Z;
        } else
        {
            return BOUNDS_X;
        }
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean canPlaceTorchOnTop(IBlockState state, IBlockAccess world, BlockPos pos)
    {
        return state.getValue(FACING).getAxis() == EnumFacing.Axis.Y;
    }

    @Override
    public boolean shouldSideBeRendered(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side)
    {
        return wrappedBlock.shouldSideBeRendered(state, world, pos, side);
    }
}
