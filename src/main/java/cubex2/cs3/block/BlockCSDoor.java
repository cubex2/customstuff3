package cubex2.cs3.block;

import cubex2.cs3.block.attributes.DoorAttributes;
import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.material.EnumPushReaction;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

public class BlockCSDoor extends BlockCS
{
    public static final PropertyDirection FACING = PropertyDirection.create("facing", EnumFacing.Plane.HORIZONTAL);
    public static final PropertyBool OPEN = PropertyBool.create("open");
    public static final PropertyEnum<BlockDoor.EnumHingePosition> HINGE = PropertyEnum.create("hinge", BlockDoor.EnumHingePosition.class);
    public static final PropertyBool POWERED = PropertyBool.create("powered");
    public static final PropertyEnum<BlockDoor.EnumDoorHalf> HALF = PropertyEnum.create("half", BlockDoor.EnumDoorHalf.class);
    protected static final AxisAlignedBB SOUTH_AABB = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.1875D);
    protected static final AxisAlignedBB NORTH_AABB = new AxisAlignedBB(0.0D, 0.0D, 0.8125D, 1.0D, 1.0D, 1.0D);
    protected static final AxisAlignedBB WEST_AABB = new AxisAlignedBB(0.8125D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
    protected static final AxisAlignedBB EAST_AABB = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.1875D, 1.0D, 1.0D);

    private DoorAttributes container;

    public BlockCSDoor(WrappedBlock block)
    {
        super(block);
        container = (DoorAttributes) block.container;
        disableStats();

        setDefaultState(this.blockState.getBaseState()
                                       .withProperty(FACING, EnumFacing.NORTH)
                                       .withProperty(OPEN, false)
                                       .withProperty(HINGE, BlockDoor.EnumHingePosition.LEFT)
                                       .withProperty(POWERED, false)
                                       .withProperty(HALF, BlockDoor.EnumDoorHalf.LOWER));
    }

    public boolean normalBlockShading()
    {
        return container.normalBlockShading;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isPassable(IBlockAccess world, BlockPos pos)
    {
        return isOpen(combineMetadata(world, pos));
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        state = state.getActualState(source, pos);
        EnumFacing enumfacing = state.getValue(FACING);
        boolean flag = !state.getValue(OPEN);
        boolean flag1 = state.getValue(HINGE) == BlockDoor.EnumHingePosition.RIGHT;

        switch (enumfacing)
        {
            case EAST:
            default:
                return flag ? EAST_AABB : (flag1 ? NORTH_AABB : SOUTH_AABB);
            case SOUTH:
                return flag ? SOUTH_AABB : (flag1 ? EAST_AABB : WEST_AABB);
            case WEST:
                return flag ? WEST_AABB : (flag1 ? SOUTH_AABB : NORTH_AABB);
            case NORTH:
                return flag ? NORTH_AABB : (flag1 ? WEST_AABB : EAST_AABB);
        }
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        super.onBlockActivated(world, pos, state, player, hand, facing, hitX, hitY, hitZ);
        if (container.redstoneOnly)
            return false;
        else
        {
            BlockPos blockpos1 = state.getValue(HALF) == BlockDoor.EnumDoorHalf.LOWER ? pos : pos.down();
            IBlockState iblockstate1 = pos.equals(blockpos1) ? state : world.getBlockState(blockpos1);

            if (iblockstate1.getBlock() != this)
            {
                return false;
            } else
            {
                state = iblockstate1.cycleProperty(OPEN);
                world.setBlockState(blockpos1, state, 2);
                world.markBlockRangeForRenderUpdate(blockpos1, pos);
                world.playEvent(player, state.getValue(OPEN) ? 1003 : 1006, pos, 0);
                return true;
            }
        }
    }

    public void toggleDoor(World world, BlockPos pos, boolean open)
    {
        IBlockState iblockstate = world.getBlockState(pos);

        if (iblockstate.getBlock() == this)
        {
            BlockPos blockpos1 = iblockstate.getValue(HALF) == BlockDoor.EnumDoorHalf.LOWER ? pos : pos.down();
            IBlockState iblockstate1 = pos == blockpos1 ? iblockstate : world.getBlockState(blockpos1);

            if (iblockstate1.getBlock() == this && iblockstate1.getValue(OPEN) != open)
            {
                world.setBlockState(blockpos1, iblockstate1.withProperty(OPEN, open), 2);
                world.markBlockRangeForRenderUpdate(blockpos1, pos);
                world.playEvent(null, open ? 1003 : 1006, pos, 0);
            }
        }
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block neighborBlock, BlockPos fromPos)
    {
        if (state.getValue(HALF) == BlockDoor.EnumDoorHalf.UPPER)
        {
            BlockPos blockpos1 = pos.down();
            IBlockState iblockstate1 = world.getBlockState(blockpos1);

            if (iblockstate1.getBlock() != this)
            {
                world.setBlockToAir(pos);
            } else if (neighborBlock != this)
            {
                state.neighborChanged(world, blockpos1, neighborBlock, fromPos);
            }
        } else
        {
            boolean flag1 = false;
            BlockPos blockpos2 = pos.up();
            IBlockState iblockstate2 = world.getBlockState(blockpos2);

            if (iblockstate2.getBlock() != this)
            {
                world.setBlockToAir(pos);
                flag1 = true;
            }

            if (!world.getBlockState(pos.down()).isFullyOpaque())
            {
                world.setBlockToAir(pos);
                flag1 = true;

                if (iblockstate2.getBlock() == this)
                {
                    world.setBlockToAir(blockpos2);
                }
            }

            if (flag1)
            {
                if (!world.isRemote)
                {
                    this.dropBlockAsItem(world, pos, state, 0);
                }
            } else
            {
                boolean flag = world.isBlockPowered(pos) || world.isBlockPowered(blockpos2);

                if ((flag || neighborBlock.getDefaultState().canProvidePower()) && neighborBlock != this && flag != iblockstate2.getValue(POWERED))
                {
                    world.setBlockState(blockpos2, iblockstate2.withProperty(POWERED, flag), 2);

                    if (flag != state.getValue(OPEN))
                    {
                        world.setBlockState(pos, state.withProperty(OPEN, flag), 2);
                        world.markBlockRangeForRenderUpdate(pos, pos);
                        world.playEvent(null, flag ? 1003 : 1006, pos, 0);
                    }
                }
            }
        }
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
    {
        return state.getValue(HALF) == BlockDoor.EnumDoorHalf.UPPER ? new ArrayList<ItemStack>() : super.getDrops(world, pos, state, fortune);
    }

    @Override
    public boolean canPlaceBlockAt(World world, BlockPos pos)
    {
        return pos.getY() < world.getHeight() - 1 && (world.getBlockState(pos.down()).isSideSolid(world, pos.down(), EnumFacing.UP) && super.canPlaceBlockAt(world, pos) && super.canPlaceBlockAt(world, pos.up()));
    }

    @Override
    public EnumPushReaction getMobilityFlag(IBlockState state)
    {
        return EnumPushReaction.DESTROY;
    }

    public static int combineMetadata(IBlockAccess world, BlockPos pos)
    {
        IBlockState state = world.getBlockState(pos);
        int i = state.getBlock().getMetaFromState(state);
        boolean flag = isTop(i);
        IBlockState iblockstate1 = world.getBlockState(pos.down());
        int j = iblockstate1.getBlock().getMetaFromState(iblockstate1);
        int k = flag ? j : i;
        IBlockState iblockstate2 = world.getBlockState(pos.up());
        int l = iblockstate2.getBlock().getMetaFromState(iblockstate2);
        int i1 = flag ? i : l;
        boolean flag1 = (i1 & 1) != 0;
        boolean flag2 = (i1 & 2) != 0;
        return removeHalfBit(k) | (flag ? 8 : 0) | (flag1 ? 16 : 0) | (flag2 ? 32 : 0);
    }

    @Override
    public void onBlockHarvested(World world, BlockPos pos, IBlockState state, EntityPlayer player)
    {
        BlockPos blockpos1 = pos.down();

        if (player.capabilities.isCreativeMode && state.getValue(HALF) == BlockDoor.EnumDoorHalf.UPPER && world.getBlockState(blockpos1).getBlock() == this)
        {
            world.setBlockToAir(blockpos1);
        }
    }

    @SideOnly(Side.CLIENT)
    public Item getItem()
    {
        return Item.REGISTRY.getObject(new ResourceLocation(container.getPack().id, wrappedBlock.getName()));
    }

    @Override
    public void getSubBlocks(Item item, CreativeTabs tab, NonNullList<ItemStack> list)
    {
        if (container.addToCreative)
        {
            list.add(new ItemStack(getItem()));
        }
    }

    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos)
    {
        IBlockState iblockstate1;

        if (state.getValue(HALF) == BlockDoor.EnumDoorHalf.LOWER)
        {
            iblockstate1 = worldIn.getBlockState(pos.up());

            if (iblockstate1.getBlock() == this)
            {
                state = state.withProperty(HINGE, iblockstate1.getValue(HINGE)).withProperty(POWERED, iblockstate1.getValue(POWERED));
            }
        } else
        {
            iblockstate1 = worldIn.getBlockState(pos.down());

            if (iblockstate1.getBlock() == this)
            {
                state = state.withProperty(FACING, iblockstate1.getValue(FACING)).withProperty(OPEN, iblockstate1.getValue(OPEN));
            }
        }

        return state;
    }

    public IBlockState getStateFromMeta(int meta)
    {
        return (meta & 8) > 0 ? this.getDefaultState().withProperty(HALF, BlockDoor.EnumDoorHalf.UPPER).withProperty(HINGE, (meta & 1) > 0 ? BlockDoor.EnumHingePosition.RIGHT : BlockDoor.EnumHingePosition.LEFT).withProperty(POWERED, (meta & 2) > 0) : this.getDefaultState().withProperty(HALF, BlockDoor.EnumDoorHalf.LOWER).withProperty(FACING, EnumFacing.getHorizontal(meta & 3).rotateYCCW()).withProperty(OPEN, (meta & 4) > 0);
    }

    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

    public int getMetaFromState(IBlockState state)
    {
        byte b0 = 0;
        int i;

        if (state.getValue(HALF) == BlockDoor.EnumDoorHalf.UPPER)
        {
            i = b0 | 8;

            if (state.getValue(HINGE) == BlockDoor.EnumHingePosition.RIGHT)
            {
                i |= 1;
            }

            if (state.getValue(POWERED))
            {
                i |= 2;
            }
        } else
        {
            i = b0 | state.getValue(FACING).rotateY().getHorizontalIndex();

            if (state.getValue(OPEN))
            {
                i |= 4;
            }
        }

        return i;
    }

    protected static int removeHalfBit(int meta)
    {
        return meta & 7;
    }

    public static boolean isOpen(IBlockAccess worldIn, BlockPos pos)
    {
        return isOpen(combineMetadata(worldIn, pos));
    }

    public static EnumFacing getFacing(IBlockAccess worldIn, BlockPos pos)
    {
        return getFacing(combineMetadata(worldIn, pos));
    }

    public static EnumFacing getFacing(int combinedMeta)
    {
        return EnumFacing.getHorizontal(combinedMeta & 3).rotateYCCW();
    }

    protected static boolean isOpen(int combinedMeta)
    {
        return (combinedMeta & 4) != 0;
    }

    protected static boolean isTop(int meta)
    {
        return (meta & 8) != 0;
    }

    protected static boolean isHingeLeft(int combinedMeta)
    {
        return (combinedMeta & 16) != 0;
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, HALF, FACING, OPEN, HINGE, POWERED);
    }
}
