package cubex2.cs3.block;

import cubex2.cs3.block.attributes.FacingAttributes;
import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockCSFacing extends BlockCS
{
    public static final PropertyDirection FACING = PropertyDirection.create("facing");

    protected FacingAttributes container;

    public BlockCSFacing(WrappedBlock block)
    {
        super(block);
        container = (FacingAttributes) block.container;
        setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH));
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return true;
    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase living, ItemStack stack)
    {
        super.onBlockPlacedBy(world, pos, state, living, stack);
        if (!container.faceBySide)
        {
            EnumFacing facing = determineOrientation(world, pos, state, (EntityPlayer) living);
            world.setBlockState(pos, state.withProperty(FACING, facing), 3);
        }
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
        super.getStateForPlacement(world, pos, facing, hitX, hitY, hitZ, meta, placer);
        if (container.faceBySide)
        {
            meta = facing.getIndex();
        }
        return getStateFromMeta(meta);
    }

    public EnumFacing determineOrientation(World world, BlockPos pos, IBlockState state, EntityPlayer player)
    {
        if (MathHelper.abs((float) player.posX - pos.getX()) < 2.0F && MathHelper.abs((float) player.posZ - pos.getZ()) < 2.0F)
        {
            double var5 = player.posY + 1.82D - player.getYOffset();

            if (container.canFaceTop && var5 - pos.getY() > 2.0D)
                return EnumFacing.UP;

            if (container.canFaceBottom && pos.getY() - var5 > 0.0D)
                return EnumFacing.DOWN;

            if (!container.canFaceSides)
                return EnumFacing.UP;
        }

        int lookDir = MathHelper.floor(player.rotationYaw * 4.0F / 360.0F + 0.5D) & 3;
        return (lookDir == 0 ? EnumFacing.NORTH : lookDir == 1 ? EnumFacing.EAST : lookDir == 2 ? EnumFacing.SOUTH : lookDir == 3 ? EnumFacing.WEST : EnumFacing.DOWN);
    }

    public boolean rotateSideTextures()
    {
        return container.rotateSideTextures;
    }

    @SideOnly(Side.CLIENT)
    public IBlockState getStateForEntityRender(IBlockState state)
    {
        return this.getDefaultState().withProperty(FACING, EnumFacing.SOUTH);
    }

    @Override
    public IBlockState getStateFromMeta(int meta)
    {
        EnumFacing facing = EnumFacing.getFront(meta);

        return getDefaultState().withProperty(FACING, facing);
    }

    @Override
    public int getMetaFromState(IBlockState state)
    {
        return state.getValue(FACING).getIndex();
    }

    @Override
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, FACING);
    }
}
