package cubex2.cs3.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.Random;

public class BlockCSFlat extends BlockCSFacing
{
    public static final AxisAlignedBB BOUNDS_DOWN = new AxisAlignedBB(0D, 0D, 0D, 1D, 0.05D, 1D);
    public static final AxisAlignedBB BOUNDS_UP = new AxisAlignedBB(0D, 0.95D, 0D, 1D, 1D, 1D);
    public static final AxisAlignedBB BOUNDS_NORTH = new AxisAlignedBB(0D, 0D, 0.95D, 1D, 1D, 1D);
    public static final AxisAlignedBB BOUNDS_SOUTH = new AxisAlignedBB(0D, 0D, 0D, 1D, 1D, 0.05D);
    public static final AxisAlignedBB BOUNDS_WEST = new AxisAlignedBB(0.95D, 0D, 0D, 1D, 1D, 1D);
    public static final AxisAlignedBB BOUNDS_EAST = new AxisAlignedBB(0D, 0D, 0D, 0.05D, 1D, 1D);


    public BlockCSFlat(WrappedBlock block)
    {
        super(block);
        setTickRandomly(true);
    }

    @Override
    public int getMetaForFlowerGen(int itemMeta)
    {
        return 1;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean canPlaceBlockOnSide(World world, BlockPos pos, EnumFacing side)
    {
        return super.canPlaceBlockOnSide(world, pos, side) && world.isSideSolid(pos.offset(side.getOpposite()), side);
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block neighborBlock, BlockPos fromPos)
    {
        super.neighborChanged(state, world, pos, neighborBlock, fromPos);
        this.checkFlowerChange(world, pos, state);
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand)
    {
        super.updateTick(world, pos, state, rand);
        this.checkFlowerChange(world, pos, state);
    }

    protected final void checkFlowerChange(World world, BlockPos pos, IBlockState state)
    {
        if (!this.canBlockStay(world, pos))
        {
            this.dropBlockAsItem(world, pos, state, 0);
            world.setBlockToAir(pos);
        }
    }

    public boolean canBlockStay(World world, BlockPos pos)
    {
        EnumFacing[] aenumfacing = EnumFacing.values();
        int i = aenumfacing.length;

        for (EnumFacing facing : aenumfacing)
        {
            if (world.isSideSolid(pos.offset(facing), facing.getOpposite(), true))
            {
                return true;
            }
        }

        return false;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        EnumFacing facing = state.getValue(FACING);

        switch (facing)
        {
            case DOWN:
                return BOUNDS_DOWN;
            case UP:
                return BOUNDS_UP;
            case NORTH:
                return BOUNDS_NORTH;
            case SOUTH:
                return BOUNDS_SOUTH;
            case WEST:
                return BOUNDS_WEST;
            default:
                return BOUNDS_EAST;
        }
    }
}
