package cubex2.cs3.block;

import com.google.common.collect.Lists;
import cubex2.cs3.block.attributes.PressurePlateAttributes;
import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFence;
import net.minecraft.block.material.EnumPushReaction;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BlockCSPressurePlate extends BlockCS
{
    public static final PropertyBool POWERED = PropertyBool.create("powered");
    /** The bounding box for the pressure plate pressed state */
    protected static final AxisAlignedBB PRESSED_AABB = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.03125D, 0.9375D);
    protected static final AxisAlignedBB UNPRESSED_AABB = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.0625D, 0.9375D);
    /** This bounding box is used to check for entities in a certain area and then determine the pressed state. */
    protected static final AxisAlignedBB PRESSURE_AABB = new AxisAlignedBB(0.125D, 0.0D, 0.125D, 0.875D, 0.25D, 0.875D);

    private PressurePlateAttributes container;

    public BlockCSPressurePlate(WrappedBlock block)
    {
        super(block);
        this.setDefaultState(this.blockState.getBaseState().withProperty(POWERED, false));
        container = (PressurePlateAttributes) block.container;

        setTickRandomly(true);
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        boolean flag = this.getRedstoneStrength(state) > 0;
        return flag ? PRESSED_AABB : UNPRESSED_AABB;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
    {
        return NULL_AABB;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isPassable(IBlockAccess worldIn, BlockPos pos)
    {
        return true;
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
    {
        return this.canBePlacedOn(worldIn, pos.down());
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block neighborBlock, BlockPos fromPos)
    {
        super.neighborChanged(state, world, pos, neighborBlock, fromPos);

        if (!this.canBePlacedOn(world, pos.down()))
        {
            this.dropBlockAsItem(world, pos, state, 0);
            world.setBlockToAir(pos);
        }
    }

    private boolean canBePlacedOn(World worldIn, BlockPos pos)
    {
        return worldIn.getBlockState(pos).isFullyOpaque() || worldIn.getBlockState(pos).getBlock() instanceof BlockFence || worldIn.getBlockState(pos).getBlock() instanceof BlockCSFence;
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand)
    {
        super.updateTick(world, pos, state, rand);

        if (!world.isRemote)
        {
            int i = this.getRedstoneStrength(state);

            if (i > 0)
            {
                this.updateState(world, pos, state, i);
            }
        }
    }

    @Override
    public void onEntityCollidedWithBlock(World world, BlockPos pos, IBlockState state, Entity entity)
    {
        super.onEntityCollidedWithBlock(world, pos, state, entity);

        if (!world.isRemote)
        {
            int i = this.getRedstoneStrength(state);

            if (i == 0)
            {
                this.updateState(world, pos, state, i);
            }
        }
    }

    protected void updateState(World worldIn, BlockPos pos, IBlockState state, int oldRedstoneStrength)
    {
        int j = this.computeRedstoneStrength(worldIn, pos);
        boolean flag = oldRedstoneStrength > 0;
        boolean flag1 = j > 0;

        if (oldRedstoneStrength != j)
        {
            state = this.setRedstoneStrength(state, j);
            worldIn.setBlockState(pos, state, 2);
            this.updateNeighbors(worldIn, pos);
            worldIn.markBlockRangeForRenderUpdate(pos, pos);
        }

        if (!flag1 && flag)
        {
            if (this.blockMaterial == Material.WOOD)
            {
                worldIn.playSound(null, pos, SoundEvents.BLOCK_WOOD_PRESSPLATE_CLICK_OFF, SoundCategory.BLOCKS, 0.3F, 0.7F);
            } else
            {
                worldIn.playSound(null, pos, SoundEvents.BLOCK_STONE_PRESSPLATE_CLICK_OFF, SoundCategory.BLOCKS, 0.3F, 0.5F);
            }
        } else if (flag1 && !flag)
        {
            if (this.blockMaterial == Material.WOOD)
            {
                worldIn.playSound(null, pos, SoundEvents.BLOCK_WOOD_PRESSPLATE_CLICK_ON, SoundCategory.BLOCKS, 0.3F, 0.8F);
            } else
            {
                worldIn.playSound(null, pos, SoundEvents.BLOCK_STONE_PRESSPLATE_CLICK_ON, SoundCategory.BLOCKS, 0.3F, 0.6F);
            }
        }

        if (flag1)
        {
            worldIn.scheduleUpdate(pos, this, this.tickRate(worldIn));
        }
    }

    private Class<? extends Entity> getEntityClass(String s)
    {
        Class<? extends Entity> entityClass;

        if (s.equals("mobs"))
        {
            entityClass = EntityLiving.class;
        } else if (s.equals("players"))
        {
            entityClass = EntityPlayer.class;
        } else if (s.equals("hostiles"))
        {
            entityClass = EntityMob.class;
        } else if (s.equals("animals"))
        {
            entityClass = EntityAnimal.class;
        } else if (s.equals("items"))
        {
            entityClass = EntityItem.class;
        } else if (s.matches("[0-9]+"))
        {
            entityClass = EntityList.getClassFromID(Integer.parseInt(s));
        } else
        {
            entityClass = EntityList.getClass(new ResourceLocation(s));
        }

        return entityClass;
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state)
    {

        if (this.getRedstoneStrength(state) > 0)
        {
            this.updateNeighbors(world, pos);
        }

        super.breakBlock(world, pos, state);
    }

    protected void updateNeighbors(World worldIn, BlockPos pos)
    {
        worldIn.notifyNeighborsOfStateChange(pos, this, false);
        worldIn.notifyNeighborsOfStateChange(pos.down(), this, false);
    }

    @Override
    public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return this.getRedstoneStrength(blockState);
    }

    @Override
    public int getStrongPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return side == EnumFacing.UP ? this.getRedstoneStrength(blockState) : 0;
    }

    @Override
    public boolean canProvidePower(IBlockState state)
    {
        return true;
    }

    @Override
    public EnumPushReaction getMobilityFlag(IBlockState state)
    {
        return EnumPushReaction.DESTROY;
    }

    protected int computeRedstoneStrength(World world, BlockPos pos)
    {
        AxisAlignedBB axisalignedbb = PRESSURE_AABB.offset(pos);
        List<Object> list = Lists.newArrayList();

        if (container.include != null && container.include.length > 0)
        {
            for (String include : container.include)
            {
                if (include.equals("all"))
                {
                    list.addAll(world.getEntitiesWithinAABBExcludingEntity(null, axisalignedbb));
                } else
                {
                    Class<? extends Entity> entityClass = getEntityClass(include);

                    if (entityClass != null)
                    {
                        list.addAll(world.getEntitiesWithinAABB(entityClass, axisalignedbb));
                    }
                }
            }

            if (container.exclude != null && container.exclude.length > 0)
            {
                List<Object> list1 = new ArrayList<Object>(list);
                for (Object o : list1)
                {
                    Entity entity = (Entity) o;
                    for (String exclude : container.exclude)
                    {
                        Class<?> entityClass = getEntityClass(exclude);

                        if (entityClass.isAssignableFrom(entity.getClass()))
                        {
                            list.remove(entity);
                        }
                    }
                }
            }
        }

        if (!list.isEmpty())
        {
            for (Object aList : list)
            {
                Entity entity = (Entity) aList;

                if (!entity.doesEntityNotTriggerPressurePlate())
                {
                    return 15;
                }
            }
        }

        return 0;
    }

    protected int getRedstoneStrength(IBlockState state)
    {
        return state.getValue(POWERED) ? 15 : 0;
    }

    protected IBlockState setRedstoneStrength(IBlockState state, int strength)
    {
        return state.withProperty(POWERED, strength > 0);
    }

    public IBlockState getStateFromMeta(int meta)
    {
        return this.getDefaultState().withProperty(POWERED, meta == 1);
    }

    public int getMetaFromState(IBlockState state)
    {
        return state.getValue(POWERED) ? 1 : 0;
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, POWERED);
    }
}
