package cubex2.cs3.block;

import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class BlockCSWheat extends BlockCS
{
    public static final AxisAlignedBB BOUNDS = new AxisAlignedBB(0F, 0.0F, 0F, 1F, 0.25F, 1F);

    public BlockCSWheat(WrappedBlock block)
    {
        super(block);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return BOUNDS;
    }
}
