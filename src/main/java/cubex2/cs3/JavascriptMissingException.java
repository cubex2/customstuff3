package cubex2.cs3;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiErrorScreen;
import net.minecraftforge.fml.client.CustomModLoadingErrorDisplayException;

public class JavascriptMissingException extends CustomModLoadingErrorDisplayException
{
    public JavascriptMissingException()
    {
        super("JavaScript not available", new RuntimeException());
    }

    @Override
    public void initGui(GuiErrorScreen errorScreen, FontRenderer fontRenderer)
    {

    }

    @Override
    public void drawScreen(GuiErrorScreen errorScreen, FontRenderer fontRenderer, int mouseRelX, int mouseRelY, float tickTime)
    {
        errorScreen.drawCenteredString(fontRenderer, "Custom Stuff 3 requires JavaScript.", errorScreen.width / 2, errorScreen.height / 2-15, 0xFFFFFF);
        errorScreen.drawCenteredString(fontRenderer, "js.jar is missing!", errorScreen.width / 2, errorScreen.height / 2, 0xFFFFFF);
    }
}
