package cubex2.cs3.common;

import cubex2.cs3.util.ItemStackHelper;
import cubex2.cs3.util.RecipeInput;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;

import java.util.Iterator;
import java.util.Random;

public class ShapedRecipe extends BaseContent implements IRecipe
{
    public RecipeInput[] input;
    public Object[] inputArray;
    public int width;
    public int height;
    public ItemStack result;

    private boolean mirrored = true;


    public ShapedRecipe(BaseContentPack pack)
    {
        super(pack);
    }

    public ShapedRecipe(int width, int height, RecipeInput[] input, ItemStack result, BaseContentPack pack)
    {
        super(pack);
        this.width = width;
        this.height = height;
        this.input = input;
        this.result = result;
        this.inputArray = new ShapedOreRecipe(result, createRecipeObjects()).getInput();
    }

    @Override
    public void apply()
    {
        GameRegistry.addRecipe(this);

        super.apply();
    }

    @Override
    public void remove()
    {
        CraftingManager.getInstance().getRecipeList().remove(this);

        super.remove();
    }

    @Override
    public void edit()
    {
        super.edit();
    }

    public Object[] createRecipeObjects()
    {
        Object[] recipeObjects = new Object[height + (width * height) * 2];

        String[] shape = createShape();
        System.arraycopy(shape, 0, recipeObjects, 0, shape.length);

        for (int i = 0, n = 0; i < width * height * 2; i += 2)
        {
            int idx = i + height;
            recipeObjects[idx] = input[n] != null ? (char) (n) : Character.MAX_VALUE;
            recipeObjects[idx + 1] = input[n] != null ? input[n].getInput() : Blocks.AIR;
            n += 1;
        }

        return recipeObjects;
    }

    private String[] createShape()
    {
        String[] shape = new String[height];
        int n = 0;
        for (int i = 0; i < shape.length; i++)
        {
            String s = "";
            for (int j = 0; j < width; j++)
            {
                s += (char) n++;
            }
            shape[i] = s;
        }
        return shape;
    }

    @Override
    public void writeToNBT(NBTTagCompound compound)
    {
        compound.setByte("Width", (byte) width);
        compound.setByte("Height", (byte) height);

        NBTTagList tagList = new NBTTagList();
        for (int i = 0; i < input.length; i++)
        {
            if (input[i] != null)
            {
                NBTTagCompound compound1 = new NBTTagCompound();
                compound1.setByte("Index", (byte) i);

                input[i].writeToNBT(compound1);

                tagList.appendTag(compound1);
            }
        }
        compound.setTag("Input", tagList);

        compound.setTag("Result", ItemStackHelper.writeToNBTNamed(result));
    }

    @Override
    public boolean readFromNBT(NBTTagCompound compound)
    {
        width = compound.getByte("Width");
        height = compound.getByte("Height");

        NBTTagList tagList = compound.getTagList("Input", 10);
        input = new RecipeInput[width * height];
        for (int i = 0; i < tagList.tagCount(); i++)
        {
            NBTTagCompound compound1 = tagList.getCompoundTagAt(i);
            int idx = compound1.getByte("Index");
            input[idx] = RecipeInput.loadFromNBT(compound1);
            if (input[idx] != null && !input[idx].hasValidInput())
            {
                return false;
            }
        }

        inputArray = new ShapedOreRecipe(Blocks.DIRT, createRecipeObjects()).getInput();

        result = ItemStackHelper.readFromNBTNamed(compound.getCompoundTag("Result"));
        return !result.isEmpty();
    }

    @Override
    public boolean matches(InventoryCrafting inv, World worldIn)
    {
        int matrixWidth = inv.getWidth();
        if (matrixWidth <= 0) return false;
        int matrixHeight = inv.getHeight();

        for (int x = 0; x <= matrixWidth - width; x++)
        {
            for (int y = 0; y <= matrixHeight - height; ++y)
            {
                if (checkMatch(inv, x, y, false, matrixWidth, matrixHeight))
                    return true;

                if (mirrored && checkMatch(inv, x, y, true, matrixWidth, matrixHeight))
                    return true;
            }
        }

        return false;
    }

    private boolean checkMatch(InventoryCrafting inv, int startX, int startY, boolean mirror, int matrixWidth, int matrixHeight)
    {
        for (int x = 0; x < matrixWidth; x++)
        {
            for (int y = 0; y < matrixHeight; y++)
            {
                int subX = x - startX;
                int subY = y - startY;
                RecipeInput target = null;

                if (subX >= 0 && subY >= 0 && subX < width && subY < height)
                {
                    if (mirror)
                    {
                        target = input[width - subX - 1 + subY * width];
                    } else
                    {
                        target = input[subX + subY * width];
                    }
                }

                ItemStack slot = inv.getStackInRowAndColumn(x, y);

                if (target != null)
                {
                    boolean matched = false;

                    Iterator<ItemStack> itr = target.getStacks().iterator();
                    while (itr.hasNext() && !matched)
                    {
                        matched = OreDictionary.itemMatches(itr.next(), slot, false);
                    }

                    if (!matched)
                        return false;

                    if (target.damageItem && slot.getItemDamage() + target.damageAmount > slot.getMaxDamage())
                        return false;
                } else if (!slot.isEmpty())
                {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public ItemStack getCraftingResult(InventoryCrafting inv)
    {
        return result.copy();
    }

    @Override
    public int getRecipeSize()
    {
        return input.length;
    }

    @Override
    public ItemStack getRecipeOutput()
    {
        return result;
    }

    @Override
    public NonNullList<ItemStack> getRemainingItems(InventoryCrafting inv)
    {
        NonNullList<ItemStack> stacks = NonNullList.withSize(inv.getSizeInventory(), ItemStack.EMPTY);

        int matrixWidth = inv.getWidth();
        int matrixHeight = inv.getHeight();

        int startX = -1;
        int startY = -1;

        for (int x = 0; x < matrixWidth && startX == -1; x++)
        {
            for (int y = 0; y < matrixHeight; y++)
            {
                if (!inv.getStackInRowAndColumn(x, y).isEmpty())
                {
                    startX = x;
                    break;
                }
            }
        }

        for (int y = 0; y < matrixHeight && startY == -1; y++)
        {
            for (int x = 0; x < matrixWidth; x++)
            {
                if (!inv.getStackInRowAndColumn(x, y).isEmpty())
                {
                    startY = y;
                    break;
                }
            }
        }

        if (checkMatch(inv, startX, startY, false, matrixWidth, matrixHeight)) // no mirror
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int index = x + y * width;
                    doDamaging(index, inv, startX, x, startY, y, stacks);
                }
            }
        } else if (mirrored) // mirror
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int index = (width - x - 1) + y * width;
                    doDamaging(index, inv, startX, x, startY, y, stacks);
                }
            }
        }

        for (int i = 0; i < stacks.size(); i++)
        {
            if (stacks.get(i).isEmpty())
                stacks.set(i, ForgeHooks.getContainerItem(inv.getStackInSlot(i)));
        }

        return stacks;
    }

    private void doDamaging(int index, InventoryCrafting inv, int startX, int x, int startY, int y, NonNullList<ItemStack> stacks)
    {
        if (input[index] != null && input[index].damageItem)
        {
            ItemStack stack = inv.getStackInRowAndColumn(startX + x, startY + y);
            if (!stack.isEmpty() && (stack.isItemStackDamageable() || input[index].damageAmount == 0))
            {
                stack = stack.copy();
                if (input[index].damageAmount != 0 && stack.attemptDamageItem(input[index].damageAmount, new Random()))
                {
                    stack.shrink(1);

                    if (stack.getCount() < 0)
                        stack.setCount(0);
                    stack.setItemDamage(0);
                }
            }

            if (!stack.isEmpty() && stack.getCount() > 0)
                stacks.set(startX + x + (startY + y) * inv.getWidth(), stack.copy());
        }
    }
}
