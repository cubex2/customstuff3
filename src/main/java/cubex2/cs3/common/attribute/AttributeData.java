package cubex2.cs3.common.attribute;

import cubex2.cs3.registry.AttributeContentRegistry;
import cubex2.cs3.util.IPurposeStringProvider;
import cubex2.cs3.util.StringProviderPurpose;

import java.lang.reflect.Field;

public class AttributeData implements Comparable<AttributeData>, IPurposeStringProvider
{
    public final Attribute attribute;
    public final Field field;
    public final String desc;
    public boolean isFavourite;
    public final AttributeContentRegistry registry;

    public AttributeData(Attribute attribute, Field field, AttributeContentRegistry registry)
    {
        this.attribute = attribute;
        this.field = field;
        this.registry = registry;
        this.isFavourite = registry.isFavouriteAttribute(field.getName());
        desc = field.isAnnotationPresent(AttributeDescription.class) ? field.getAnnotation(AttributeDescription.class).value() : null;
    }

    public void switchFavourite()
    {
        isFavourite = registry.switchFavourite(field.getName());
        registry.getPack().save();
    }

    public String getDisplayName()
    {
        return attribute.customName().length() > 0 ? attribute.customName() : field.getName();
    }

    @Override
    public int compareTo(AttributeData o)
    {
        if (isFavourite == o.isFavourite)
            return getDisplayName().compareTo(o.getDisplayName());
        if (isFavourite)
            return -1;
        return 1;
    }

    @Override
    public String getStringForPurpose(StringProviderPurpose purpose)
    {
        return getDisplayName();
    }
}
