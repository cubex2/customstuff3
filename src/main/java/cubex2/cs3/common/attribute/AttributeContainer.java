package cubex2.cs3.common.attribute;

import com.google.common.collect.Maps;
import cubex2.cs3.common.AttributeContent;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.registry.AttributeContentRegistry;
import net.minecraft.nbt.NBTTagCompound;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public abstract class AttributeContainer
{
    protected final BaseContentPack pack;
    private final AttributeContentRegistry registry;
    private final Map<Field, AttributeBridge> bridgeMap = Maps.newHashMap();

    private static final HashMap<Class, Field[]> fieldCache = new HashMap<>();

    public AttributeContainer(BaseContentPack pack, Class<? extends AttributeContent> contentClazz)
    {
        this.pack = pack;
        this.registry = (AttributeContentRegistry) pack.getContentRegistry(contentClazz);
        createBridges();
    }

    public BaseContentPack getPack()
    {
        return pack;
    }

    public void loadFromNBT(NBTTagCompound compound, boolean postInit)
    {
        Field[] fields = Arrays.stream(getFields()).filter(f -> f.getAnnotation(Attribute.class).loadOnPostInit() ^ !postInit).toArray(Field[]::new);

        try
        {
            for (Field field : fields)
            {
                if (compound.hasKey(field.getName()))
                {
                    AttributeBridge bridge = bridgeMap.get(field);
                    NBTTagCompound attributeCompound = compound.getCompoundTag(field.getName());
                    field.set(this, bridge.loadValueFromNBT(attributeCompound));
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void writeToNBT(NBTTagCompound compound)
    {
        Field[] fields = getFields();

        for (Field field : fields)
        {
            try
            {
                AttributeBridge bridge = bridgeMap.get(field);
                NBTTagCompound attributeCompound = new NBTTagCompound();

                Object value = field.get(this);
                bridge.writeValueToNBT(attributeCompound, value);

                compound.setTag(field.getName(), attributeCompound);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private Field[] getFields()
    {
        Class<?> clazz = getClass();

        if (fieldCache.containsKey(clazz))
            return fieldCache.get(clazz);
        else
        {
            Field[] fields = Arrays.stream(clazz.getFields()).filter(f -> f.isAnnotationPresent(Attribute.class)).toArray(Field[]::new);

            fieldCache.put(clazz, fields);

            return fields;
        }
    }

    private void createBridges()
    {
        try
        {
            for (Field field : getFields())
            {
                bridgeMap.put(field, getBridge(field));
            }
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    private AttributeBridge getBridge(Field field) throws Exception
    {
        Class<? extends AttributeBridge> bridgeClass = field.getAnnotation(Attribute.class).bridgeClass();
        if (bridgeClass == NullAttributeBridge.class)
        {
            bridgeClass = DefaultAttributeBridges.getDefaultBridge(field.getType());
            if (bridgeClass == NullAttributeBridge.class)
            {
                throw new RuntimeException("Attribute " + field.getName() + " doesn't have a bridge class!");
            }
        }
        AttributeBridge bridge = bridgeClass.newInstance();
        bridge.additionalInfo = field.getAnnotation(Attribute.class).additionalInfo();
        return bridge;
    }

    /**
     * Gets the data of the attributes that have 'hasOwnWindow' set to true.
     *
     * @param type The type of the block/item/...
     * @return The names of the fields.
     */
    public AttributeData[] getAttributeDatas(String type)
    {
        return Arrays.stream(getFields())
                .filter(f -> f.getAnnotation(Attribute.class).hasOwnWindow())
                .filter(f -> addAttribute(f.getName(), type))
                .map(f -> new AttributeData(f.getAnnotation(Attribute.class), f, registry))
                .toArray(AttributeData[]::new);
    }

    protected boolean addAttribute(String attribute, String type)
    {
        return true;
    }

    public <T> T getAttribute(String attributeName)
    {
        try
        {
            return (T) getClass().getField(attributeName).get(this);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public <T> void setAttribute(String attributeName, T value)
    {
        try
        {
            getClass().getField(attributeName).set(this, value);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public Class<? extends Window> getWindowClass(AttributeData item)
    {
        return item.attribute.windowClass();
    }
}
