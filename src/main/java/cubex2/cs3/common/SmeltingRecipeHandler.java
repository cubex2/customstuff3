package cubex2.cs3.common;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

import javax.annotation.Nonnull;

public class SmeltingRecipeHandler
{
    private final BaseContentPack pack;

    public SmeltingRecipeHandler(BaseContentPack pack)
    {
        this.pack = pack;
    }

    @Nonnull
    public ItemStack getSmeltingResult(ItemStack stack, String recipeList)
    {
        if (recipeList.equals("vanilla"))
            return FurnaceRecipes.instance().getSmeltingResult(stack);
        for (SmeltingRecipe recipe : pack.getContentRegistry(SmeltingRecipe.class).getContentList())
        {
            ItemStack result = recipe.getResult(stack, recipeList);
            if (!result.isEmpty()) return result;
        }
        return ItemStack.EMPTY;
    }

    public float getSmeltingExperience(ItemStack stack, String recipeList)
    {
        if (recipeList.equals("vanilla"))
            return FurnaceRecipes.instance().getSmeltingExperience(stack);
        return 0f;
    }
}
