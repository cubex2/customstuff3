package cubex2.cs3.common;

import com.google.common.collect.Multimap;
import cubex2.cs3.api.scripting.ITriggerData;
import cubex2.cs3.api.scripting.TriggerType;
import cubex2.cs3.common.attribute.AttributeContainer;
import cubex2.cs3.common.scripting.TriggerData;
import cubex2.cs3.item.EnumItemType;
import cubex2.cs3.item.ItemCS;
import cubex2.cs3.item.attributes.ItemAttributes;
import cubex2.cs3.util.GeneralHelper;
import cubex2.cs3.util.ItemStackHelper;
import cubex2.cs3.util.JavaScriptHelper;
import cubex2.cs3.util.ToolClass;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class WrappedItem extends AttributeContent implements Comparable<WrappedItem>
{
    public Item item;
    public ItemAttributes container;

    private EnumItemType type;


    public WrappedItem(String name, EnumItemType type, BaseContentPack pack)
    {
        super(name, pack);
        this.type = type;
    }

    public WrappedItem(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public AttributeContainer getContainer()
    {
        return container;
    }

    @Override
    public String getTypeString()
    {
        return type.name;
    }

    public EnumItemType getType()
    {
        return type;
    }

    private void initItem()
    {
        item.setUnlocalizedName(name);

        for (ToolClass toolClass : container.toolClasses)
        {
            item.setHarvestLevel(toolClass.toolClass, toolClass.harvestLevel);
        }
    }

    @Override
    public void addLangEntries(Map<String, String> langMap)
    {
        if (container != null)
        {
            langMap.put("item." + name + ".name", container.displayName);
        }
    }

    @Override
    public void apply()
    {
        if (item != null)
        {
            initItem();
        }

        super.apply();
    }

    @Override
    public void postInit()
    {
        if (item != null && item instanceof ItemCS)
        {
            ((ItemCS) item).postInit();
        }
    }

    @Override
    public boolean canEdit()
    {
        return item != null;
    }

    @Override
    public void edit()
    {
        super.edit();
    }

    @Override
    public void remove()
    {
        super.remove();
    }

    @Override
    public boolean readFromNBT(final NBTTagCompound compound)
    {
        name = compound.getString("Name");
        type = EnumItemType.get(compound.getString("Type"));

        container = type.createAttributeContainer(this);
        container.loadFromNBT(compound.getCompoundTag("Attributes"), false);
        pack.postponeHandler.addTask(() ->
                                     {
                                         container.loadFromNBT(compound.getCompoundTag("Attributes"), true);
                                         return true;
                                     });

        item = type.createItem(this);

        return true;
    }

    /* Item methods */


    public int getItemStackLimit(ItemStack stack)
    {
        return container.maxStack;
    }


    public boolean isFull3D()
    {
        return container.full3d;
    }

    public boolean hasEffect(ItemStack stack)
    {
        return container.hasEffect || stack.isItemEnchanted();
    }

    public CreativeTabs getCreativeTab()
    {
        return container.creativeTab;
    }

    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        ItemStack stack = player.getHeldItem(hand);

        if (container.onUse != null && container.onUse.script != null)
        {
            ITriggerData data = new TriggerData("onUse", TriggerType.ITEM, world, player, stack).setPosition(pos).setSideAndHit(side, hitX, hitY, hitZ);
            JavaScriptHelper.executeTrigger(container.onUse.script, data, pack);
            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.PASS;
    }


    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand)
    {
        ItemStack stack = player.getHeldItem(hand);

        if (container.onRightClick != null && container.onRightClick.script != null)
        {
            ITriggerData data = new TriggerData("onRightClick", TriggerType.ITEM, world, player, stack);
            JavaScriptHelper.executeTrigger(container.onRightClick.script, data, pack);
            return new ActionResult<>(EnumActionResult.SUCCESS, stack);
        }
        return new ActionResult<>(EnumActionResult.PASS, stack);
    }


    public EnumActionResult onItemUseFirst(EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand)
    {
        ItemStack stack = player.getHeldItem(hand);

        if (container.onUseFirst != null && container.onUseFirst.script != null)
        {
            ITriggerData data = new TriggerData("onUseFirst", TriggerType.ITEM, world, player, stack).setPosition(pos).setSideAndHit(side, hitX, hitY, hitZ);
            JavaScriptHelper.executeTrigger(container.onUseFirst.script, data, pack);
            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.PASS;
    }


    public boolean useItemOnEntity(ItemStack stack, EntityPlayer player, EntityLivingBase target)
    {
        if (target instanceof EntityPlayer && container.onUseOnPlayer != null && container.onUseOnPlayer.script != null)
        {
            ITriggerData data = new TriggerData("onUseOnPlayer", TriggerType.ITEM, player.world, player, stack).setInteractPlayer((EntityPlayer) target);
            JavaScriptHelper.executeTrigger(container.onUseOnPlayer.script, data, pack);
        } else if (container.onUseOnEntity != null && container.onUseOnEntity.script != null)
        {
            ITriggerData data = new TriggerData("onUseOnEntity", TriggerType.ITEM, player.world, player, stack).setLiving(target);
            JavaScriptHelper.executeTrigger(container.onUseOnEntity.script, data, pack);
        }
        return false;
    }


    public boolean onBlockDestroyed(ItemStack stack, World world, IBlockState state, BlockPos pos, EntityLivingBase living)
    {
        if (container.onBlockDestroyed != null && container.onBlockDestroyed.script != null && living instanceof EntityPlayer)
        {
            EntityPlayer player = (EntityPlayer) living;
            ITriggerData data = new TriggerData("onBlockDestroyed", TriggerType.ITEM, world, player, stack).setPosition(pos).setBlockName(GeneralHelper.getBlockName(state.getBlock()));
            JavaScriptHelper.executeTrigger(container.onBlockDestroyed.script, data, pack);
        }
        return false;
    }


    public boolean hitEntity(ItemStack stack, EntityLivingBase living1, EntityLivingBase living2)
    {
        if (container.onHitEntity != null && container.onHitEntity.script != null && living2 instanceof EntityPlayer)
        {
            EntityPlayer player = (EntityPlayer) living2;
            ITriggerData data = new TriggerData("onHitEntity", TriggerType.ITEM, player.world, player, stack).setLiving(living1);
            JavaScriptHelper.executeTrigger(container.onHitEntity.script, data, pack);
        }
        return false;
    }


    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity)
    {
        if (container.onLeftClickLiving != null && container.onLeftClickLiving.script != null && entity instanceof EntityLiving)
        {
            EntityLiving living = (EntityLiving) entity;
            ITriggerData data = new TriggerData("onLeftClickLiving", TriggerType.ITEM, player.world, player, stack).setLiving(living);
            JavaScriptHelper.executeTrigger(container.onLeftClickLiving.script, data, pack);
        } else if (container.onLeftClickPlayer != null && container.onLeftClickPlayer.script != null && entity instanceof EntityPlayer)
        {
            EntityPlayer interactPlayer = (EntityPlayer) entity;
            ITriggerData data = new TriggerData("onLeftClickPlayer", TriggerType.ITEM, player.world, player, stack).setInteractPlayer(interactPlayer);
            JavaScriptHelper.executeTrigger(container.onLeftClickPlayer.script, data, pack);
        }
        return false;
    }


    public void onUpdate(ItemStack stack, World world, Entity entity, int slotId, boolean isCurrentItem)
    {
        if (container.onUpdate != null && container.onUpdate.script != null && entity instanceof EntityPlayer)
        {
            EntityPlayer player = (EntityPlayer) entity;
            ITriggerData data = new TriggerData("onUpdate", TriggerType.ITEM, player.world, player, stack).setSlotId(slotId).setIsCurrentItem(isCurrentItem);
            JavaScriptHelper.executeTrigger(container.onUpdate.script, data, pack);
        }
    }


    public void onCreated(ItemStack stack, World world, EntityPlayer player)
    {
        if (container.onCreated != null && container.onCreated.script != null)
        {
            ITriggerData data = new TriggerData("onCreated", TriggerType.ITEM, player.world, player, stack);
            JavaScriptHelper.executeTrigger(container.onCreated.script, data, pack);
        }
    }


    public void onUsingTick(ItemStack stack, EntityLivingBase living, int tickCount)
    {
        if (container.onUsing != null && container.onUsing.script != null)
        {
            if (living instanceof EntityPlayer) // TODO 1.9
            {
                ITriggerData data = new TriggerData("onUsing", TriggerType.ITEM, living.world, (EntityPlayer) living, stack).setTickCount(getMaxItemUseDuration(stack) - tickCount);
                JavaScriptHelper.executeTrigger(container.onUsing.script, data, pack);
            }
        }
    }


    public void onPlayerStoppedUsing(ItemStack stack, World world, EntityLivingBase living, int tickCount)
    {
        if (container.onStoppedUsing != null && container.onStoppedUsing.script != null)
        {
            if (living instanceof EntityPlayer) // TODO 1.9
            {
                ITriggerData data = new TriggerData("onStoppedUsing", TriggerType.ITEM, living.world, (EntityPlayer) living, stack).setTickCount(getMaxItemUseDuration(stack) - tickCount);
                JavaScriptHelper.executeTrigger(container.onStoppedUsing.script, data, pack);
            }
        }
    }


    public boolean onDroppedByPlayer(ItemStack stack, EntityPlayer player)
    {
        if (container.onDroppedByPlayer != null && container.onDroppedByPlayer.script != null)
        {
            ITriggerData data = new TriggerData("onDroppedByPlayer", TriggerType.ITEM, player.world, player, stack);
            JavaScriptHelper.executeTrigger(container.onDroppedByPlayer.script, data, pack);
        }
        return true;
    }


    public boolean onBlockStartBreak(ItemStack stack, BlockPos pos, EntityPlayer player)
    {
        if (container.onBlockStartBreak != null && container.onBlockStartBreak.script != null)
        {
            ITriggerData data = new TriggerData("onBlockStartBreak", TriggerType.ITEM, player.world, player, stack).setPosition(pos);
            JavaScriptHelper.executeTrigger(container.onBlockStartBreak.script, data, pack);
        }
        return false;
    }


    public ItemStack onItemUseFinish(ItemStack stack, World world, EntityLivingBase living)
    {
        if (container.onEaten != null && container.onEaten.script != null)
        {
            if (living instanceof EntityPlayer)
            {
                ITriggerData data = new TriggerData("onEaten", TriggerType.ITEM, living.world, (EntityPlayer) living, stack);
                JavaScriptHelper.executeTrigger(container.onEaten.script, data, pack);
            }
        }
        return stack;
    }

    public float getStrVsBlock(ItemStack stack, IBlockState state)
    {
        if (container.toolClasses.length == 1 && container.toolClasses[0].toolClass.equals("noHarvest"))
            return 0.0f;
        for (ToolClass toolClass : container.toolClasses)
        {
            if (state.getBlock().isToolEffective(toolClass.toolClass, state))
                return container.efficiency;
        }
        /* TODO
        if (container.effectiveBlocks.length == 0)
            return 1.0F;
        for (ItemStack is : container.effectiveBlocks)
            if (ItemStackHelper.itemStackEqual(is, new ItemStack(block, 1, meta)))
                return container.efficiency;*/
        return 1.0F;
    }

    public boolean canHarvestBlock(IBlockState state)
    {
        /* TODO
        for (Block block1 : container.harvestBlocks)
        {
            if (block1 == block)
            {
                return true;
            }
        }*/
        return false;
    }

    public int getMaxItemUseDuration(ItemStack stack)
    {
        return container.maxUsingDuration;
    }

    public int getItemEnchantability()
    {
        return container.enchantability;
    }

    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean advanced)
    {
        if (container.information == null) return;

        String[] split = container.information.split("\n");
        Collections.addAll(list, split);
    }

    public EnumAction getItemUseAction(ItemStack stack)
    {
        return container.usingAction;
    }

    public boolean isDamageable()
    {
        return container.maxDamage > 0;
    }

    public int getMaxDamage()
    {
        return container.maxDamage;
    }

    public Multimap<String, AttributeModifier> getAttributeModifiers(Multimap<String, AttributeModifier> multimap, UUID uuid)
    {
        if (container.damage >= 1)
        {
            multimap.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), new AttributeModifier(uuid, "Weapon modifier", (double) container.damage, 0));
        }
        return multimap;
    }

    public boolean hasContainerItem(ItemStack stack)
    {
        return container.containerItem != null;
    }

    public ItemStack getContainerItem(ItemStack stack)
    {
        return container.containerItem != null ? container.containerItem.copy() : null;
    }

    public boolean getIsRepairable(ItemStack stack, ItemStack material)
    {
        return container.anvilMaterial != null && ItemStackHelper.itemStackEqual(container.anvilMaterial, material);
    }

    @Override
    public int compareTo(WrappedItem o)
    {
        return name.compareTo(o.name);
    }
}
