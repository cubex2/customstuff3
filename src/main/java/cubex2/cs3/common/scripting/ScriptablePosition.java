package cubex2.cs3.common.scripting;

public class ScriptablePosition implements Cloneable
{
    private static final long serialVersionUID = -7583400857543055489L;

    public double x;
    public double y;
    public double z;

    public double jsGet_x()
    {
        return x;
    }

    public double jsGet_y()
    {
        return y;
    }

    public double jsGet_z()
    {
        return z;
    }

    @SuppressWarnings("unused")
    public void jsSet_x(double x)
    {
        this.x = x;
    }

    @SuppressWarnings("unused")
    public void jsSet_y(double y)
    {
        this.y = y;
    }

    @SuppressWarnings("unused")
    public void jsSet_z(double z)
    {
        this.z = z;
    }

    public ScriptablePosition(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @SuppressWarnings("unused")
    public ScriptablePosition()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    public String getClassName()
    {
        return "Position";
    }

    @Override
    public Object clone()
    {
        return new ScriptablePosition(x, y, z);
    }

}
