package cubex2.cs3.common.scripting;

import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.WrappedGui;
import cubex2.cs3.common.inventory.Inventory;
import cubex2.cs3.common.inventory.WrappedInventory;
import cubex2.cs3.gui.ContainerBasic;
import cubex2.cs3.gui.EnumGuiType;
import cubex2.cs3.gui.InventoryItemStack;
import cubex2.cs3.gui.WindowNormal;
import cubex2.cs3.ingame.gui.GuiBase;
import cubex2.cs3.network.PacketOpenCustomGuiServer;
import cubex2.cs3.network.PacketOpenUserContainerGuiClient;
import cubex2.cs3.registry.GuiRegistry;
import cubex2.cs3.tileentity.TileEntityInventory;
import cubex2.cs3.util.GeneralHelper;
import cubex2.cs3.util.JavaScriptHelper;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSnow;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.FoodStats;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

public class ScriptableEntityPlayer extends ScriptableEntityLiving
{

    private EntityPlayer player;
    private Inventory inventory;

    public ScriptableEntityPlayer(EntityPlayer player)
    {
        super(player);
        this.player = player;
        inventory = new Inventory(player.inventory);
    }

    /**
     * Opens a client-only gui for the player
     */
    public void openGui(String guiName)
    {
        BaseContentPack pack = JavaScriptHelper.executingPack;
        WrappedGui gui = ((GuiRegistry) pack.getContentRegistry(WrappedGui.class)).getGui(guiName);
        while (GuiBase.INSTANCE.window instanceof WindowNormal)
            GuiBase.openPrevWindow();

        if (gui.getType() == EnumGuiType.NORMAL && player.world.isRemote)
        {
            FMLClientHandler.instance().showGuiScreen(GuiBase.INSTANCE);
            GuiBase.openWindow(gui.getType().createWindow(gui));
        }
    }

    /**
     * Opens an item inventory gui for the player.
     *
     * @param slotId The id of the slot of the itemstack
     */
    public void openItemGui(String guiName, int slotId)
    {
        BaseContentPack pack = JavaScriptHelper.executingPack;
        WrappedGui gui = ((GuiRegistry) pack.getContentRegistry(WrappedGui.class)).getGui(guiName);
        while (GuiBase.INSTANCE.window instanceof WindowNormal)
            GuiBase.openPrevWindow();

        if (gui.getType() == EnumGuiType.NORMAL && player.world.isRemote)
        {
            FMLClientHandler.instance().showGuiScreen(GuiBase.INSTANCE);
            GuiBase.openWindow(gui.getType().createWindow(gui));
        } else if (gui.getType() == EnumGuiType.CONTAINER && !player.world.isRemote)
        {
            PacketOpenCustomGuiServer.openGuiOnServer((EntityPlayerMP) player, new PacketOpenUserContainerGuiClient(gui, slotId),
                                                      new ContainerBasic(gui, player, new InventoryItemStack(gui, player, slotId)));
        }
    }

    /**
     * Opens a block inventory gui for the player.
     *
     * @param position The position of the block to open a gui for.
     */
    public void openBlockGui(String guiName, ScriptablePosition position)
    {
        openBlockGui(guiName, (int) position.x, (int) position.y, (int) position.z);
    }

    /**
     * Opens a block inventory gui for the player.
     *
     * @param guiName The name of the gui used for the name-attribute in the gui-file
     * @param x       The x-position of the block
     * @param y       The y-position of the block
     * @param z       The z-position of the block
     */
    public void openBlockGui(String guiName, int x, int y, int z)
    {
        BaseContentPack pack = JavaScriptHelper.executingPack;
        WrappedGui gui = ((GuiRegistry) pack.getContentRegistry(WrappedGui.class)).getGui(guiName);
        while (GuiBase.INSTANCE.window instanceof WindowNormal)
            GuiBase.openPrevWindow();

        if (gui.getType() == EnumGuiType.NORMAL && player.world.isRemote)
        {
            FMLClientHandler.instance().showGuiScreen(GuiBase.INSTANCE);
            GuiBase.openWindow(gui.getType().createWindow(gui));
        } else if (gui.getType() == EnumGuiType.CONTAINER && !player.world.isRemote)
        {
            TileEntity te = player.world.getTileEntity(new BlockPos(x, y, z));
            if (te != null && te instanceof TileEntityInventory)
            {
                PacketOpenCustomGuiServer.openGuiOnServer((EntityPlayerMP) player, new PacketOpenUserContainerGuiClient(gui, x, y, z),
                                                          new ContainerBasic(gui, player, (IInventory) te));
            } else
            {
                player.sendMessage(new TextComponentString("ERROR: Block has no tile entity of type 'inventory'"));
            }
        }
    }

    /**
     * Sets the player's hunger
     *
     * @param hunger The hunger to set
     */
    public void setHunger(int hunger)
    {
        ReflectionHelper.setPrivateValue(FoodStats.class, player.getFoodStats(), hunger, 0);
    }

    /**
     * Gets the player's hunger
     *
     * @return The player's hunger
     */
    public int getHunger()
    {
        return player.getFoodStats().getFoodLevel();
    }

    /**
     * Sets the player's saturation
     *
     * @param saturation The saturation to set
     */
    public void setSaturation(double saturation)
    {
        player.getFoodStats().setFoodSaturationLevel((float) saturation);
    }

    /**
     * Gets the player's saturation
     *
     * @return The player's saturation
     */
    public double getSaturation()
    {
        return player.getFoodStats().getSaturationLevel();
    }

    /**
     * Sets how high this entity can step up when running into a block to try to get over it
     *
     * @param f The step height
     */
    public void setStepHeight(double f)
    {
        player.stepHeight = (float) f;
    }

    /**
     * Gets the step height
     *
     * @return The player's step height
     */
    public double getStepHeight()
    {
        return player.stepHeight;
    }

    /**
     * Displays a message for the player. Doesn't work at the moment.
     *
     * @param message The message to display
     */
    public void sendMessage(String message)
    {
        if (!player.world.isRemote)
        {
            player.sendMessage(new TextComponentString(message));
        }
    }

    /**
     * Makes the player send a chat message as if the message was sent with the chat console. Commands will work this way.
     *
     * @param chat The chat message
     */
    public void sendChat(String chat)
    {
        Minecraft mc = FMLClientHandler.instance().getClient();
        if (mc != null && !player.world.isRemote)
        {
            mc.player.sendChatMessage(chat);
        }
    }

    /**
     * Sets the current item in use, so onUsing is called.
     */
    public void setItemInUse()
    {
        player.setActiveHand(EnumHand.MAIN_HAND);
    }

    /**
     * Gets the id of the slot the player is currently using
     *
     * @return The id of the slot
     */
    public int getCurrentSlot()
    {
        return player.inventory.currentItem;
    }

    /**
     * Places a block
     *
     * @param position         The position
     * @param side             On what side at position the block should be added
     * @param blockName        The block's name
     * @param metadata         The block's metadata
     * @param useFromInventory If true the function will uses blocks from the inventory and won't place any block if the required block isn't in the
     *                         player's inventory
     * @return True if the block has been placed, false otherwise
     */
    public boolean placeBlock(ScriptablePosition position, int side, String blockName, int metadata, boolean useFromInventory)
    {
        return placeBlock((int) position.x, (int) position.y, (int) position.z, side, blockName, metadata, useFromInventory);
    }

    /**
     * Places a block
     *
     * @param x                The x coordinate
     * @param y                The y coordinate
     * @param z                The z coordinate
     * @param side             On what side at position the block should be added
     * @param blockName        The block's name
     * @param metadata         The block's metadata
     * @param useFromInventory If true the function will uses blocks from the inventory and won't place any block if the required block isn't in the
     *                         player's inventory
     * @return True if the block has been placed, false otherwise
     */
    public boolean placeBlock(int x, int y, int z, int side, String blockName, int metadata, boolean useFromInventory)
    {
        World world = player.world;
        BlockPos pos = new BlockPos(x, y, z);
        IBlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        Block toPlace = GeneralHelper.getBlock(blockName);

        EnumFacing facing = EnumFacing.getFront(side);

        if (block == Blocks.SNOW_LAYER && state.getValue(BlockSnow.LAYERS) < 1)
        {
            facing = EnumFacing.UP;
        } else if (!block.isReplaceable(world, pos))
        {
            pos = pos.offset(facing);
        }


        ItemStack stack;
        if (useFromInventory)
        {
            stack = inventory.findItem(Item.getItemFromBlock(toPlace), metadata);
            if (stack.isEmpty())
                return false;
        } else
        {
            stack = new ItemStack(toPlace, 1, metadata);
        }

        if (stack.getCount() == 0)
        {
            return false;
        } else if (!player.canPlayerEdit(pos, facing, stack))
        {
            return false;
        } else if (pos.getY() == 255 && toPlace.getDefaultState().getMaterial().isSolid())
        {
            return false;
        } else if (world.mayPlace(toPlace, pos, false, facing, null))
        {
            int i = stack.getItem().getMetadata(stack.getMetadata());
            IBlockState iblockstate1 = toPlace.getStateForPlacement(world, pos, facing, 0, 0, 0, i, player);

            if (placeBlockAt(toPlace, stack, player, world, pos, facing, 0, 0, 0, iblockstate1))
            {
                SoundType soundtype = block.getSoundType();
                world.playSound(player, pos, soundtype.getPlaceSound(), SoundCategory.BLOCKS, (soundtype.getVolume() + 1.0F) / 2.0F, soundtype.getPitch() * 0.8F);
                stack.shrink(1);
            }

            return true;
        } else
        {
            return false;
        }
    }

    private boolean placeBlockAt(Block toPlace, ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, IBlockState newState)
    {
        if (!world.setBlockState(pos, newState, 3)) return false;

        IBlockState state = world.getBlockState(pos);
        if (state.getBlock() == toPlace)
        {
            ItemBlock.setTileEntityNBT(world, player, pos, stack);
            toPlace.onBlockPlacedBy(world, pos, state, player, stack);
        }

        return true;
    }

    private RayTraceResult getMop()
    {
        Vec3d vec1 = new Vec3d(player.posX, player.posY + player.getEyeHeight(), player.posZ);
        Vec3d vec2 = player.getLookVec();
        Vec3d vec3 = vec1.addVector(vec2.xCoord * 1000, vec2.yCoord * 1000, vec2.zCoord * 1000);
        RayTraceResult mop = player.world.rayTraceBlocks(vec1, vec3);
        if (mop != null && mop.typeOfHit == RayTraceResult.Type.BLOCK)
            return mop;
        return null;
    }

    /**
     * Gets the x-coordinate of the block that the player is looking on.
     *
     * @return The x-coordinate or -1 if the player isn't looking on any block
     */
    public int getLookX()
    {
        RayTraceResult mop = getMop();
        return mop != null ? mop.getBlockPos().getX() : -1;
    }

    /**
     * Gets the y-coordinate of the block that the player is looking on.
     *
     * @return The y-coordinate or -1 if the player isn't looking on any block
     */
    public int getLookY()
    {
        RayTraceResult mop = getMop();
        return mop != null ? mop.getBlockPos().getY() : -1;
    }

    /**
     * Gets the z-coordinate of the block that the player is looking on.
     *
     * @return The z-coordinate or -1 if the player isn't looking on any block
     */
    public int getLookZ()
    {
        RayTraceResult mop = getMop();
        return mop != null ? mop.getBlockPos().getZ() : -1;
    }

    /**
     * Adds experience to the player
     *
     * @param amount The amount of xp
     */
    public void addExperience(int amount)
    {
        player.addExperience(amount);
    }

    /**
     * Removes experience from the player
     *
     * @param amount The amount of xp to remove
     */
    public void removeExperience(int amount)
    {
        player.experienceTotal -= amount;
        if (player.experienceTotal < 0)
        {
            player.experienceTotal = 0;
        }
    }

    /**
     * Sets the player's experience
     *
     * @param amount The new xp of the player
     */
    public void setExperience(int amount)
    {
        player.experienceTotal = amount;
        if (player.experienceTotal < 0)
        {
            player.experienceTotal = 0;
        }
    }

    /**
     * Adds level to the player
     *
     * @param amount The amount of levels
     */
    public void addExperienceLevel(int amount)
    {
        player.addExperienceLevel(amount);
    }

    /**
     * Removes level from the player
     *
     * @param amount The amount of levels
     */
    public void removeExperienceLevel(int amount)
    {
        player.addExperienceLevel(-amount);
    }

    /**
     * Sets the player's level
     *
     * @param level The level
     */
    public void setExperienceLevel(int level)
    {
        player.experienceLevel = level;
    }

    /**
     * Gets the player's experience
     *
     * @return
     */
    public int getExperience()
    {
        return player.experienceTotal;
    }

    /**
     * Gets the player's level
     *
     * @return
     */
    public int getExperienceLevel()
    {
        return player.experienceLevel;
    }

    /**
     * Gets the player's horizontal look angle
     *
     * @return
     */
    public float getHorizontalAngle()
    {
        return MathHelper.wrapDegrees(player.rotationYaw);
    }

    /**
     * Gets the player's vertical look angle
     *
     * @return
     */
    public float getVerticalAngle()
    {
        return MathHelper.wrapDegrees(player.rotationPitch);
    }

    /**
     * Gets the player's username
     *
     * @return
     */
    public String getUsername()
    {
        return player.getName();
    }

    /**
     * Check if the player is sneaking
     *
     * @return
     */
    public boolean isSneaking()
    {
        return player.isSneaking();
    }

    /**
     * Checks if the player is sprinting
     *
     * @return
     */
    public boolean isSprinting()
    {
        return player.isSprinting();
    }

    /**
     * Sets the player's max health
     *
     * @param value The new max health
     */
    public void setMaxHealth(int value)
    {
        value = value < 1 ? 1 : value;
        player.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(value);
    }


    /**
     * Swings the item the player is holding.
     */
    public void swingItem()
    {
        player.swingArm(EnumHand.MAIN_HAND);
    }

    /**
     * Get if the player is in creative mode
     *
     * @return true if the player is in creative mode false otherwise
     */
    public boolean isInCreative()
    {
        return player.capabilities.isCreativeMode;
    }

    /**
     * Damages the item currently hold by the player.
     *
     * @param amount The amount of damage to add to the item
     */
    public void damageCurrentItem(int amount)
    {
        getInventory().damageItem(getCurrentSlot(), amount);
    }

    /**
     * Gets the players inventory.
     *
     * @return
     */
    public ScriptableInventory getInventory()
    {
        return new ScriptableInventory(player.inventory);
    }

    public void openEnderChest(String customName)
    {
        if (!player.world.isRemote)
        {
            WrappedInventory inv = new WrappedInventory(player.getInventoryEnderChest());
            inv.customName = customName;
            player.displayGUIChest(inv);
        }
    }

    public void openEnderChest()
    {
        openEnderChest(null);
    }
}
