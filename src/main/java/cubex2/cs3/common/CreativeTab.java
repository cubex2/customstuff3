package cubex2.cs3.common;


import cubex2.cs3.util.CreativeTabCS3;
import cubex2.cs3.util.ItemStackHelper;
import cubex2.cs3.util.StackLabelItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.Map;

public class CreativeTab extends BaseContent implements StackLabelItem
{
    public String name;
    public String label;
    public ItemStack icon;

    private CreativeTabCS3 tab;

    public CreativeTab(BaseContentPack pack)
    {
        super(pack);
    }

    public CreativeTab(String name, String label, ItemStack icon, BaseContentPack pack)
    {
        super(pack);
        this.name = name;
        this.label = label;
        this.icon = icon;
    }

    @Override
    public void apply()
    {
        tab = new CreativeTabCS3(name, icon);

        super.apply();
    }

    @Override
    public void edit()
    {
        tab.icon = icon;

        super.edit();
    }

    @Override
    public void addLangEntries(Map<String, String> langMap)
    {
        langMap.put("itemGroup." + name, label);
    }

    @Override
    public void writeToNBT(NBTTagCompound compound)
    {
        compound.setString("Name", name);
        compound.setString("Label", label);
        compound.setTag("Icon", ItemStackHelper.writeToNBTNamed(icon));
    }

    @Override
    public boolean readFromNBT(NBTTagCompound compound)
    {
        name = compound.getString("Name");
        label = compound.getString("Label");
        icon = ItemStackHelper.readFromNBTNamed(compound.getCompoundTag("Icon"));

        return icon != null;
    }

    @Override
    public ItemStack getStack()
    {
        return icon;
    }

    @Override
    public String getLabel()
    {
        return name;
    }
}
