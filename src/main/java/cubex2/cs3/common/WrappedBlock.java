package cubex2.cs3.common;

import cubex2.cs3.api.scripting.ITriggerData;
import cubex2.cs3.api.scripting.TriggerType;
import cubex2.cs3.block.EnumBlockType;
import cubex2.cs3.block.attributes.BlockAttributes;
import cubex2.cs3.common.attribute.AttributeContainer;
import cubex2.cs3.common.scripting.TriggerData;
import cubex2.cs3.util.JavaScriptHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.EnumPushReaction;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameData;

import java.util.*;

public class WrappedBlock extends AttributeContent implements Comparable<WrappedBlock>
{
    public Block block;
    public Item blockItem;
    public BlockAttributes container;

    private EnumBlockType type;

    private Random random = new Random();

    public WrappedBlock(String name, EnumBlockType type, BaseContentPack pack)
    {
        super(name, pack);
        this.type = type;
    }

    public WrappedBlock(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public AttributeContainer getContainer()
    {
        return container;
    }

    @Override
    public String getTypeString()
    {
        return type.name;
    }

    public EnumBlockType getType()
    {
        return type;
    }

    private void initBlock()
    {
        block.setUnlocalizedName(name);
        block.setLightOpacity(container.opacity);
        blockItem.setMaxStackSize(container.maxStack);
        block.slipperiness = container.slipperiness;

        if (container.toolClass != null)
        {
            block.setHarvestLevel(container.toolClass, container.harvestLevel);
        }
    }

    @Override
    public void addLangEntries(Map<String, String> langMap)
    {
        if (container != null)
        {
            langMap.put("tile." + getName() + ".name", container.displayName);
        }
    }

    @Override
    public boolean canEdit()
    {
        return block != null;
    }

    @Override
    public void apply()
    {
        if (block != null)
        {
            initBlock();
        }

        super.apply();
    }

    @Override
    public boolean readFromNBT(final NBTTagCompound compound)
    {
        name = compound.getString("Name");
        type = EnumBlockType.get(compound.getString("Type"));

        container = type.createAttributeContainer(this);
        container.loadFromNBT(compound.getCompoundTag("Attributes"), false);
        pack.postponeHandler.addTask(() ->
                                     {
                                         container.loadFromNBT(compound.getCompoundTag("Attributes"), true);
                                         return true;
                                     });

        block = type.createBlock(this);
        blockItem = GameData.getItemRegistry().getObject(new ResourceLocation(pack.id + ":" + getName()));

        container.postCreateBlock(block);

        return true;
    }

    /* Block methods */
    public boolean isOpaqueCube(IBlockState state)
    {
        return !container.transparent && !container.semiTransparent;
    }

    public int getRenderBlockPass()
    {
        return container.semiTransparent ? 1 : 0;
    }

    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess world, BlockPos pos, EnumFacing side)
    {
        if (!container.tileTransparent)
        {
            Block block = world.getBlockState(pos.offset(side)).getBlock();
            return block != this.block && shouldSideBeRenderedDefault(blockState, world, pos, side);
        }
        return shouldSideBeRenderedDefault(blockState, world, pos, side);
    }

    public boolean shouldSideBeRenderedDefault(IBlockState blockState, IBlockAccess worldIn, BlockPos pos, EnumFacing side)
    {
        AxisAlignedBB axisalignedbb = blockState.getBoundingBox(worldIn, pos);

        switch (side)
        {
            case DOWN:

                if (axisalignedbb.minY > 0.0D)
                {
                    return true;
                }

                break;
            case UP:

                if (axisalignedbb.maxY < 1.0D)
                {
                    return true;
                }

                break;
            case NORTH:

                if (axisalignedbb.minZ > 0.0D)
                {
                    return true;
                }

                break;
            case SOUTH:

                if (axisalignedbb.maxZ < 1.0D)
                {
                    return true;
                }

                break;
            case WEST:

                if (axisalignedbb.minX > 0.0D)
                {
                    return true;
                }

                break;
            case EAST:

                if (axisalignedbb.maxX < 1.0D)
                {
                    return true;
                }
        }

        return !worldIn.getBlockState(pos.offset(side)).doesSideBlockRendering(worldIn, pos.offset(side), side.getOpposite());
    }

    public boolean hasTileEntity(IBlockState state)
    {
        return container.tileEntity != null;
    }

    public TileEntity createTileEntity(World world, IBlockState state)
    {
        return container.tileEntity.createTileEntity();
    }

    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand)
    {
        if (container.onUpdate != null && container.onUpdate.script != null)
        {
            ITriggerData data = new TriggerData("onUpdate", TriggerType.BLOCK, world, pos);
            JavaScriptHelper.executeTrigger(container.onUpdate.script, data, pack);
            world.updateBlockTick(pos, block, block.tickRate(world), 0);
        }
    }

    public void removedByPlayer(World world, EntityPlayer player, BlockPos pos)
    {
        if (container.onDestroyedByPlayer != null && container.onDestroyedByPlayer.script != null)
        {
            ITriggerData data = new TriggerData("onDestroyedByPlayer", TriggerType.BLOCK, world, pos).setPlayer(player);
            JavaScriptHelper.executeTrigger(container.onDestroyedByPlayer.script, data, pack);
        }
    }

    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block blockIn)
    {
        if (container.onNeighborChange != null && container.onNeighborChange.script != null)
        {
            ITriggerData data = new TriggerData("onNeighborChange", TriggerType.BLOCK, world, pos);
            JavaScriptHelper.executeTrigger(container.onNeighborChange.script, data, pack);
        }
        if (container.onRedstoneSignal != null && container.onRedstoneSignal.script != null)
        {
            TriggerData data = new TriggerData("onRedstoneSignal", TriggerType.BLOCK, world, pos);
            if (world.isBlockIndirectlyGettingPowered(pos) > 0)
            {
                data.setRedstoneSignal(true);
            } else
            {
                data.setRedstoneSignal(false);
            }
            JavaScriptHelper.executeTrigger(container.onRedstoneSignal.script, data, pack);
        }
    }

    public void onBlockAdded(World world, BlockPos pos, IBlockState state)
    {
        /*if (container.hasTileEntity)
        {
            world.setTileEntity(pos, block.createTileEntity(world, meta));
        }*/

        if (container.onAdded != null && container.onAdded.script != null)
        {
            ITriggerData data = new TriggerData("onAdded", TriggerType.BLOCK, world, pos);
            JavaScriptHelper.executeTrigger(container.onAdded.script, data, pack);
        }
        if (container.onUpdate != null && container.onUpdate.script != null)
        {
            world.updateBlockTick(pos, block, block.tickRate(world), 0);
        }
        if (container.onRedstoneSignal != null && container.onRedstoneSignal.script != null)
        {
            TriggerData data = new TriggerData("onRedstoneSignal", TriggerType.BLOCK, world, pos);
            if (world.isBlockIndirectlyGettingPowered(pos) > 0)
            {
                data.setRedstoneSignal(true);
            } else
            {
                data.setRedstoneSignal(false);
            }
            JavaScriptHelper.executeTrigger(container.onRedstoneSignal.script, data, pack);
        }
    }

    public void onBlockBreak(World world, BlockPos pos, IBlockState state)
    {
        if (container.onBreak != null && container.onBreak.script != null)
        {
            ITriggerData data = new TriggerData("onBreak", TriggerType.BLOCK, world, pos);
            JavaScriptHelper.executeTrigger(container.onBreak.script, data, pack);
        }
        if (container.tileEntity != null)
        {
            TileEntity te = world.getTileEntity(pos);
            if (te instanceof IInventory)
            {
                InventoryHelper.dropInventoryItems(world, pos, (IInventory) te);
            }
            world.removeTileEntity(pos);
        }
    }

    public boolean blockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        if (container.onActivated != null && container.onActivated.script != null)
        {
            ITriggerData data = new TriggerData("onActivated", TriggerType.BLOCK, world, pos).setPlayer(player).setSideAndHit(facing, hitX, hitY, hitZ);
            return JavaScriptHelper.executeTrigger(container.onActivated.script, data, pack, false);
        }
        return false;
    }

    public void onEntityWalking(World world, BlockPos pos, Entity entity)
    {
        if (container.onWalking != null && container.onWalking.script != null)
        {
            ITriggerData data = new TriggerData("onWalking", TriggerType.BLOCK, world, pos).setEntity(entity);
            JavaScriptHelper.executeTrigger(container.onWalking.script, data, pack);
        }
    }

    public void onBlockPlaced(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase living)
    {
        // TODO add facing, hit, living?
        if (container.onPlaced != null && container.onPlaced.script != null)
        {
            ITriggerData data = new TriggerData("onPlaced", TriggerType.BLOCK, world, pos);
            JavaScriptHelper.executeTrigger(container.onPlaced.script, data, pack);
        }
    }

    public void onBlockClicked(World world, BlockPos pos, EntityPlayer player)
    {
        if (container.onClicked != null && container.onClicked.script != null)
        {
            ITriggerData data = new TriggerData("onClicked", TriggerType.BLOCK, world, pos).setPlayer(player);
            JavaScriptHelper.executeTrigger(container.onClicked.script, data, pack);
        }
    }

    public void onEntityCollidedWithBlock(World world, BlockPos pos, IBlockState state, Entity entity)
    {
        if (container.onCollided != null && container.onCollided.script != null)
        {
            ITriggerData data = new TriggerData("onCollided", TriggerType.BLOCK, world, pos).setEntity(entity);
            JavaScriptHelper.executeTrigger(container.onCollided.script, data, pack);
        }
    }

    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase living, ItemStack stack)
    {
        if (living instanceof EntityPlayer && container.onPlacedByPlayer != null && container.onPlacedByPlayer.script != null)
        {
            ITriggerData data = new TriggerData("onPlacedByPlayer", TriggerType.BLOCK, world, pos).setPlayer((EntityPlayer) living);
            JavaScriptHelper.executeTrigger(container.onPlacedByPlayer.script, data, pack);
        } else if (container.onPlacedBy != null && container.onPlacedBy.script != null)
        {
            ITriggerData data = new TriggerData("onPlacedBy", TriggerType.BLOCK, world, pos).setLiving(living);
            JavaScriptHelper.executeTrigger(container.onPlacedBy.script, data, pack);
        }
    }

    public void onFallenUpon(World world, BlockPos pos, Entity entity, float fallDistance)
    {
        if (container.onFallenUpon != null && container.onFallenUpon.script != null)
        {
            ITriggerData data = new TriggerData("onFallenUpon", TriggerType.BLOCK, world, pos).setEntity(entity);
            JavaScriptHelper.executeTrigger(container.onFallenUpon.script, data, pack);
        }
    }

    public boolean onBonemeal(World world, BlockPos pos, EntityPlayer player)
    {
        if (container.onBonemeal != null && container.onBonemeal.script != null)
        {
            ITriggerData data = new TriggerData("onBonemeal", TriggerType.BLOCK, world, pos).setPlayer(player);
            JavaScriptHelper.executeTrigger(container.onBonemeal.script, data, pack);
        }
        return false;
    }

    public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand)
    {
        if (container.onRandomDisplayTick != null && container.onRandomDisplayTick.script != null)
        {
            ITriggerData data = new TriggerData("onRandomDisplayTick", TriggerType.BLOCK, worldIn, pos);
            JavaScriptHelper.executeTrigger(container.onRandomDisplayTick.script, data, pack);
        }
    }

    public boolean isWood(IBlockAccess world, BlockPos pos)
    {
        return container.isWood;
    }

    public boolean canSustainLeaves(IBlockAccess world, BlockPos pos)
    {
        return container.isWood;
    }

    public boolean canSilkHarvest(World world, BlockPos pos, IBlockState state, EntityPlayer player)
    {
        return container.canSilkHarvest;
    }

    public int tickRate(World world)
    {
        return container.tickrate;
    }

    public float getHardness(IBlockState state, World world, BlockPos pos)
    {
        return container.hardness;
    }

    public float getExplosionResistance(World world, BlockPos pos, Entity exploder, Explosion explosion)
    {
        return container.resistance / 5.0f;
    }

    public int getLightValue(IBlockAccess world, BlockPos pos)
    {
        return container.light;
    }

    public int getFlammability(IBlockAccess world, BlockPos pos, EnumFacing face)
    {
        return container.flammability;
    }

    public int getFireSpreadSpeed(IBlockAccess world, BlockPos pos, EnumFacing face)
    {
        return container.fireSpreadSpeed;
    }


    /* Methods from block item */

    public boolean doesContainerItemLeaveCraftingGrid(ItemStack stack)
    {
        return !container.leaveContainerItem;
    }

    public ItemStack getContainerItem(ItemStack stack)
    {
        return container.containerItem.copy();
    }

   /* public void registerBlockIcons(IIconRegister iconRegister)
    {
        Field[] fields = container.getAttributeFields(new Predicate<Field>()
        {
            @Override
            public boolean apply(Field input)
            {
                return input.getType() == IconWrapper.class && input.isAnnotationPresent(Attribute.class);
            }
        });

        try
        {
            for (Field f : fields)
            {
                ((IconWrapper) f.get(container)).setIcon(iconRegister);
            }
        } catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }*/

    public CreativeTabs getCreativeTabToDisplayOn()
    {
        return container.creativeTab;
    }

    public boolean canPlaceBlockOnSide(World world, BlockPos pos, EnumFacing side)
    {
        if (side == EnumFacing.DOWN && !container.canPlaceOnCeiling)
            return false;
        if (side == EnumFacing.UP && !container.canPlaceOnFloor)
            return false;

        return !(side.ordinal() > 1 && !container.canPlaceOnWall);
    }

    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean advanced)
    {
        if (container.information == null) return;

        String[] split = container.information.split("\n");
        Collections.addAll(list, split);
    }

    public int getExpDrop(IBlockAccess world, BlockPos pos, int fortune)
    {
        int min = container.expDropMin;
        int max = container.expDropMax;

        return random.nextInt(max + 1 - min) + min;
    }

    public ArrayList<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
    {
        return container.drop.getDrops(container.useFortune ? fortune + container.fortuneModifier : 0, random);
    }

    public EnumPushReaction getMobilityFlag()
    {
        return container.blocksPiston ? EnumPushReaction.BLOCK : null;
    }

    public boolean isBurning(IBlockAccess world, BlockPos pos)
    {
        return container.isBurning;
    }

    public boolean isBeaconBase(IBlockAccess worldObj, BlockPos pos, BlockPos beaconPos)
    {
        return container.isBeaconBase;
    }

    public boolean isFireSource(World world, BlockPos pos, EnumFacing side)
    {
        return container.isFireSource;
    }

    public ItemStack getPickBlock(RayTraceResult target, World world, BlockPos pos)
    {
        return container.pick;
    }

    public boolean onBlockEventReceived(World world, BlockPos pos, IBlockState state, int id, int data)
    {
        if (container.tileEntity != null)
        {
            TileEntity tileentity = world.getTileEntity(pos);
            return tileentity != null && tileentity.receiveClientEvent(id, data);
        }

        return false;
    }


    public BlockRenderLayer getBlockLayer()
    {
        return container.semiTransparent ? BlockRenderLayer.TRANSLUCENT : container.transparent ? BlockRenderLayer.CUTOUT : BlockRenderLayer.SOLID;
    }

    @Override
    public int compareTo(WrappedBlock o)
    {
        return name.compareTo(o.name);
    }
}
