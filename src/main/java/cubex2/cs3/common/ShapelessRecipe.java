package cubex2.cs3.common;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import cubex2.cs3.util.ItemStackHelper;
import cubex2.cs3.util.RecipeInput;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapelessOreRecipe;

import java.util.*;

public class ShapelessRecipe extends BaseContent implements IRecipe
{
    public RecipeInput[] input;
    public NonNullList<Object> inputArray;
    public ItemStack result;

    private ShapelessOreRecipe recipe;

    public ShapelessRecipe(BaseContentPack pack)
    {
        super(pack);
    }

    public ShapelessRecipe(RecipeInput[] input, ItemStack result, BaseContentPack pack)
    {
        super(pack);
        this.input = input;
        this.result = result;
        this.inputArray = new ShapelessOreRecipe(result, createRecipeObjects()).getInput();
    }

    @Override
    public void apply()
    {
        GameRegistry.addRecipe(this);

        super.apply();
    }

    @Override
    public void remove()
    {
        CraftingManager.getInstance().getRecipeList().remove(this);

        super.remove();
    }

    @Override
    public void edit()
    {
        super.edit();
    }

    public Object[] createRecipeObjects()
    {
        Object[] recipeObjects = new Object[input.length];
        for (int i = 0; i < input.length; i++)
        {
            recipeObjects[i] = input[i].getInput();
        }

        return recipeObjects;
    }

    @Override
    public void writeToNBT(NBTTagCompound compound)
    {
        NBTTagList tagList = new NBTTagList();
        for (int i = 0; i < input.length; i++)
        {
            NBTTagCompound compound1 = new NBTTagCompound();
            input[i].writeToNBT(compound1);
            tagList.appendTag(compound1);
        }
        compound.setTag("Input", tagList);

        compound.setTag("Result", ItemStackHelper.writeToNBTNamed(result));
    }

    @Override
    public boolean readFromNBT(NBTTagCompound compound)
    {
        NBTTagList tagList = compound.getTagList("Input", 10);
        input = new RecipeInput[tagList.tagCount()];
        for (int i = 0; i < input.length; i++)
        {
            NBTTagCompound compound1 = tagList.getCompoundTagAt(i);
            input[i] = RecipeInput.loadFromNBT(compound1);
            if (input[i] == null || !input[i].hasValidInput())
            {
                return false;
            }
        }

        this.inputArray = new ShapelessOreRecipe(Blocks.DIRT, createRecipeObjects()).getInput();

        result = ItemStackHelper.readFromNBTNamed(compound.getCompoundTag("Result"));
        return !result.isEmpty();
    }

    @Override
    public boolean matches(InventoryCrafting inv, World worldIn)
    {
        ArrayList<RecipeInput> required = Lists.newArrayList(input);

        for (int x = 0; x < inv.getSizeInventory(); x++)
        {
            ItemStack slot = inv.getStackInSlot(x);

            if (!slot.isEmpty())
            {
                boolean inRecipe = false;

                for (RecipeInput next : required)
                {
                    boolean match = false;

                    Iterator<ItemStack> itr = next.getStacks().iterator();
                    while (itr.hasNext() && !match)
                    {
                        match = OreDictionary.itemMatches(itr.next(), slot, false) &&
                                (!next.damageItem || slot.getItemDamage() + next.damageAmount <= slot.getMaxDamage());
                    }

                    if (match)
                    {
                        inRecipe = true;
                        required.remove(next);
                        break;
                    }
                }

                if (!inRecipe)
                {
                    return false;
                }
            }
        }

        return required.isEmpty();
    }

    @Override
    public ItemStack getCraftingResult(InventoryCrafting inv)
    {
        return result.copy();
    }

    @Override
    public int getRecipeSize()
    {
        return input.length;
    }

    @Override
    public ItemStack getRecipeOutput()
    {
        return result;
    }

    @Override
    public NonNullList<ItemStack> getRemainingItems(InventoryCrafting inv)
    {
        NonNullList<ItemStack> stacks = NonNullList.withSize(inv.getSizeInventory(), ItemStack.EMPTY);

        Set<Integer> used = Sets.newHashSet();
        for (RecipeInput anInput : input)
        {
            if (!anInput.damageItem) continue;


            List<ItemStack> inputItems = anInput.getStacks();
            outer:
            for (int j = 0; j < inv.getSizeInventory(); j++)
            {
                if ((!stacks.get(j).isEmpty()) || used.contains(j)) continue;
                ItemStack stack1 = inv.getStackInSlot(j);
                for (ItemStack stack : inputItems)
                {
                    if (OreDictionary.itemMatches(stack, stack1, false))
                    {
                        stack1 = stack1.copy();
                        if (anInput.damageAmount != 0 && stack1.attemptDamageItem(anInput.damageAmount, new Random()))
                        {
                            stack1.shrink(1);

                            if (stack1.getCount() < 0)
                                stack1.setCount(0);
                            stack1.setItemDamage(0);
                        }

                        if (stack1.getCount() > 0)
                            stacks.set(j, stack1.copy());

                        used.add(j);
                        break outer;
                    }
                }
            }


        }

        for (int i = 0; i < stacks.size(); i++)
        {
            if (stacks.get(i).isEmpty())
                stacks.set(i, ForgeHooks.getContainerItem(inv.getStackInSlot(i)));
        }

        return stacks;
    }
}
