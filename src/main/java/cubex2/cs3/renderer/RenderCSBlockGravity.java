package cubex2.cs3.renderer;

import cubex2.cs3.entity.EntityCSGravityBlock;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderCSBlockGravity extends Render
{

    public RenderCSBlockGravity(RenderManager renderManager)
    {
        super(renderManager);
        shadowSize = 0.5F;
    }

    public void doRenderGravityBlock(EntityCSGravityBlock gravityBlock, double x, double y, double z, float f, float partialTicks)
    {
        if (gravityBlock.getBlock() != null)
        {
            bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
            Block block = gravityBlock.getBlock();
            IBlockState state = block.getStateFromMeta(gravityBlock.meta);
            BlockPos pos = new BlockPos(gravityBlock);
            World world = gravityBlock.getWorld();

            if (state != world.getBlockState(pos) && state.getRenderType() != EnumBlockRenderType.INVISIBLE)
            {
                if (state.getRenderType() == EnumBlockRenderType.MODEL)
                {
                    GlStateManager.pushMatrix();
                    GlStateManager.translate((float) x, (float) y, (float) z);
                    GlStateManager.disableLighting();
                    Tessellator tessellator = Tessellator.getInstance();
                    VertexBuffer worldrenderer = tessellator.getBuffer();
                    worldrenderer.begin(7, DefaultVertexFormats.BLOCK);
                    int i = pos.getX();
                    int j = pos.getY();
                    int k = pos.getZ();
                    worldrenderer.setTranslation((double) ((float) (-i) - 0.5F), (double) (-j), (double) ((float) (-k) - 0.5F));
                    BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();
                    IBakedModel ibakedmodel = blockrendererdispatcher.getModelForState(state);
                    blockrendererdispatcher.getBlockModelRenderer().renderModel(world, ibakedmodel, state, pos, worldrenderer, false);
                    worldrenderer.setTranslation(0.0D, 0.0D, 0.0D);
                    tessellator.draw();
                    GlStateManager.enableLighting();
                    GlStateManager.popMatrix();
                    super.doRender(gravityBlock, x, y, z, f, partialTicks);
                }
            }

        }
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity entity)
    {
        return TextureMap.LOCATION_BLOCKS_TEXTURE;
    }

    @Override
    public void doRender(Entity entity, double x, double y, double z, float var8, float partialTicks)
    {
        this.doRenderGravityBlock((EntityCSGravityBlock) entity, x, y, z, var8, partialTicks);
    }
}
