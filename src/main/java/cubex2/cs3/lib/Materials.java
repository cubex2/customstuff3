package cubex2.cs3.lib;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import net.minecraft.block.material.Material;

public class Materials
{
    private static BiMap<String, Material> materialMap = HashBiMap.create();

    public static Material getMaterial(String name)
    {
        if (name == null)
            return null;

        Material material = null;

        if (materialMap.containsKey(name))
        {
            material = materialMap.get(name);
        }

        return material;
    }

    public static String getMaterialName(Material material)
    {
        if (material == null)
            return null;

        String name = null;

        if (materialMap.inverse().containsKey(material))
        {
            name = materialMap.inverse().get(material);
        }

        return name;
    }

    public static Material[] getAllMaterials()
    {
        return materialMap.values().toArray(new Material[materialMap.values().size()]);
    }

    static
    {
        materialMap.put("cactus", Material.CACTUS);
        materialMap.put("circuits", Material.CIRCUITS);
        materialMap.put("clay", Material.CLAY);
        materialMap.put("cloth", Material.CLOTH);
        materialMap.put("craftedSnow", Material.CRAFTED_SNOW);
        materialMap.put("fire", Material.FIRE);
        materialMap.put("glass", Material.GLASS);
        materialMap.put("grass", Material.GRASS);
        materialMap.put("ground", Material.GROUND);
        materialMap.put("ice", Material.ICE);
        materialMap.put("iron", Material.IRON);
        materialMap.put("lava", Material.LAVA);
        materialMap.put("leaves", Material.LEAVES);
        materialMap.put("plants", Material.PLANTS);
        materialMap.put("pumpkin", Material.GOURD);
        materialMap.put("redstoneLight", Material.REDSTONE_LIGHT);
        materialMap.put("rock", Material.ROCK);
        materialMap.put("sand", Material.SAND);
        materialMap.put("snow", Material.SNOW);
        materialMap.put("sponge", Material.SPONGE);
        materialMap.put("tnt", Material.TNT);
        materialMap.put("vine", Material.VINE);
        materialMap.put("water", Material.WATER);
        materialMap.put("wood", Material.WOOD);
    }
}
