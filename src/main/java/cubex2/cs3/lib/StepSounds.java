package cubex2.cs3.lib;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import net.minecraft.block.SoundType;

public class StepSounds
{
    private static BiMap<String, SoundType> stepSoundMap = HashBiMap.create();

    public static SoundType getStepSound(String name)
    {
        if (name == null)
            return null;

        SoundType stepSound = null;

        if (stepSoundMap.containsKey(name))
        {
            stepSound = stepSoundMap.get(name);
        }

        return stepSound;
    }

    public static String getStepSoundName(SoundType stepSound)
    {
        if (stepSound == null)
            return null;

        String name = null;

        if (stepSoundMap.inverse().containsKey(stepSound))
        {
            name = stepSoundMap.inverse().get(stepSound);
        }

        return name;
    }

    public static SoundType[] getAllSounds()
    {
        return stepSoundMap.values().toArray(new SoundType[stepSoundMap.values().size()]);
    }

    static
    {
        stepSoundMap.put("anvil", SoundType.ANVIL);
        stepSoundMap.put("cloth", SoundType.CLOTH);
        stepSoundMap.put("glass", SoundType.GLASS);
        stepSoundMap.put("grass", SoundType.PLANT);
        stepSoundMap.put("gravel", SoundType.GROUND);
        stepSoundMap.put("ladder", SoundType.LADDER);
        stepSoundMap.put("metal", SoundType.METAL);
        stepSoundMap.put("sand", SoundType.SAND);
        stepSoundMap.put("slime", SoundType.SLIME);
        stepSoundMap.put("snow", SoundType.SNOW);
        stepSoundMap.put("stone", SoundType.STONE);
        stepSoundMap.put("wood", SoundType.WOOD);
    }
}
