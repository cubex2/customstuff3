package cubex2.cs3.lib;

import com.google.common.base.Predicate;
import cubex2.cs3.ingame.gui.control.ItemDisplay;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

public class Validators
{
    public static final Predicate<ItemDisplay> ITEM_DISPLAY_NOT_NULL = input -> input != null && input.getItemStack() != ItemStack.EMPTY;

    public static final Predicate<ItemDisplay> ITEM_DISPLAY_SMELTING_INPUT = input -> input != null && input.getItemStack() != ItemStack.EMPTY && FurnaceRecipes.instance().getSmeltingResult(input.getItemStack()) == null;
}
