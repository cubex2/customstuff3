package cubex2.cs3.lib;

public class ModInfo
{
    public static final String ID = "customstuff3";
    public static final String NAME = "Custom Stuff 3";
    public static final String VERSION = "0.10.23";
    public static final String DEPENDENCIES = "after:JEI@[3.13.4,)";
}
