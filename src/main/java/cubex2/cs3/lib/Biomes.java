package cubex2.cs3.lib;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Biomes
{
    public static Biome getBiome(String name)
    {
        if (name == null)
            return null;

        return Biome.REGISTRY.getObject(new ResourceLocation(name));
    }

    public static Set<String> getBiomeNames()
    {
        Set<String> res = Sets.newHashSet();
        for (Biome b : Biome.REGISTRY)
        {
            if (b != null)
            {
                res.add(b.getBiomeName());
            }
        }
        return res;
    }

    public static List<Biome> getBiomes()
    {
        List<Biome> res = Lists.newArrayList();
        for (Biome biome : Biome.REGISTRY)
        {
            res.add(biome);
        }
        return res;
    }

    public static final Comparator<Biome> COMPARATOR = new Comparator<Biome>()
    {
        @Override
        public int compare(Biome o1, Biome o2)
        {
            return o1.getBiomeName().compareTo(o2.getBiomeName());
        }
    };
}
