package cubex2.cs3.network;

import cubex2.cs3.CustomStuff3;
import cubex2.cs3.util.Util;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public abstract class PacketOpenCustomGuiServer implements IMessage
{
    public PacketOpenCustomGuiServer()
    {
    }

    public static abstract class Handler<T extends PacketOpenCustomGuiServer> implements IMessageHandler<T, IMessage>
    {
        @Override
        public IMessage onMessage(T message, MessageContext ctx)
        {
            EntityPlayerMP player = ctx.getServerHandler().playerEntity;
            if (Util.checkThreadAndEnqueue(message, this, ctx, player.getServer()))
                return null;

            player.getNextWindowId();
            player.closeContainer();
            int windowId = player.currentWindowId;

            CustomStuff3.packetPipeline.sendTo(getPacketForClient(message, windowId, player), player);

            player.openContainer = getServerContainer(message, player);
            player.openContainer.windowId = windowId;
            player.openContainer.addListener(player);
            return null;
        }

        public void handleMessage(T message, EntityPlayerMP player)
        {
            player.getNextWindowId();
            player.closeContainer();
            int windowId = player.currentWindowId;

            CustomStuff3.packetPipeline.sendTo(getPacketForClient(message, windowId, player), player);

            player.openContainer = getServerContainer(message, player);
            player.openContainer.windowId = windowId;
            player.openContainer.addListener(player);
        }

        protected abstract IMessage getPacketForClient(T message, int windowId, EntityPlayerMP player);

        protected abstract Container getServerContainer(T message, EntityPlayerMP player);
    }

    public static void openGuiOnServer(EntityPlayerMP player, PacketOpenCustomGuiClient clientPacket, Container container)
    {
        player.getNextWindowId();
        player.closeContainer();
        int windowId = player.currentWindowId;
        clientPacket.setWindowId(windowId);

        CustomStuff3.packetPipeline.sendTo(clientPacket, player);

        player.openContainer = container;
        player.openContainer.windowId = windowId;
        player.openContainer.addListener(player);
    }
}