package cubex2.cs3.network;

import cubex2.cs3.util.ClientHelper;
import cubex2.cs3.util.Util;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public abstract class PacketOpenCustomGuiClient implements IMessage
{
    public int windowId;

    public PacketOpenCustomGuiClient()
    {
    }

    public PacketOpenCustomGuiClient(int windowId)
    {
        this.windowId = windowId;
    }

    public void setWindowId(int windowId)
    {
        this.windowId = windowId;
    }

    @Override
    public void fromBytes(ByteBuf buffer)
    {
        windowId = buffer.readInt();
    }

    @Override
    public void toBytes(ByteBuf buffer)
    {
        buffer.writeInt(windowId);
    }

    public static abstract class Handler<T extends PacketOpenCustomGuiClient> implements IMessageHandler<T, IMessage>
    {
        @Override
        public IMessage onMessage(T message, MessageContext ctx)
        {
            if (Util.checkThreadAndEnqueue(message, this, ctx, FMLClientHandler.instance().getClient()))
                return null;

            EntityPlayer player = ClientHelper.getPlayer();
            FMLCommonHandler.instance().showGuiScreen(getClientGuiElement(message, player));
            player.openContainer.windowId = message.windowId;
            return null;
        }

        protected abstract Object getClientGuiElement(T message, EntityPlayer player);
    }
}