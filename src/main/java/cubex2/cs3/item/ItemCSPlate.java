package cubex2.cs3.item;

import cubex2.cs3.common.WrappedItem;
import net.minecraft.inventory.EntityEquipmentSlot;

public class ItemCSPlate extends ItemCSArmor
{
    public ItemCSPlate(WrappedItem item)
    {
        super(item, EntityEquipmentSlot.CHEST);
    }
}
