package cubex2.cs3.item;

import com.google.common.collect.Multimap;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.WrappedItem;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.List;

public class ItemCS extends Item
{
    protected BaseContentPack pack;
    protected WrappedItem wrappedItem;

    public ItemCS(WrappedItem item)
    {
        super();
        this.pack = item.getPack();
        this.wrappedItem = item;
    }

    public void postInit()
    {

    }

    @Override
    public void setHarvestLevel(String toolClass, int level)
    {
        if (!toolClass.equals("noHarvest") && !toolClass.equals("all"))
        {
            super.setHarvestLevel(toolClass, level);
        }
    }

    @Override
    public int getItemStackLimit(ItemStack stack)
    {
        return wrappedItem.getItemStackLimit(stack);
    }

    @Override
    public boolean isFull3D()
    {
        return wrappedItem.isFull3D();
    }

    @Override
    public boolean hasEffect(ItemStack stack)
    {
        return wrappedItem.hasEffect(stack);
    }

    @Override
    public CreativeTabs getCreativeTab()
    {
        return wrappedItem.getCreativeTab();
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand)
    {
        return wrappedItem.onItemRightClick(world, player, hand);
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        return wrappedItem.onItemUse(player, world, pos, hand, side, hitX, hitY, hitZ);
    }

    @Override
    public EnumActionResult onItemUseFirst(EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand)
    {
        return wrappedItem.onItemUseFirst(player, world, pos, side, hitX, hitY, hitZ, hand);
    }

    @Override
    public boolean itemInteractionForEntity(ItemStack stack, EntityPlayer player, EntityLivingBase target, EnumHand hand)
    {
        wrappedItem.useItemOnEntity(stack, player, target);
        return false;
    }

    @Override
    public boolean onBlockDestroyed(ItemStack stack, World world, IBlockState state, BlockPos pos, EntityLivingBase living)
    {
        return wrappedItem.onBlockDestroyed(stack, world, state, pos, living);
    }

    @Override
    public boolean hitEntity(ItemStack stack, EntityLivingBase living1, EntityLivingBase living2)
    {
        return wrappedItem.hitEntity(stack, living1, living2);
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity)
    {
        return wrappedItem.onLeftClickEntity(stack, player, entity);
    }

    @Override
    public void onUpdate(ItemStack stack, World world, Entity entity, int slotId, boolean isCurrentItem)
    {
        wrappedItem.onUpdate(stack, world, entity, slotId, isCurrentItem);
    }

    @Override
    public void onCreated(ItemStack stack, World world, EntityPlayer player)
    {
        wrappedItem.onCreated(stack, world, player);
    }

    @Override
    public void onUsingTick(ItemStack stack, EntityLivingBase living, int tickCount)
    {
        wrappedItem.onUsingTick(stack, living, tickCount);
    }

    @Override
    public void onPlayerStoppedUsing(ItemStack stack, World world, EntityLivingBase living, int tickCount)
    {
        wrappedItem.onPlayerStoppedUsing(stack, world, living, tickCount);
    }

    @Override
    public boolean onDroppedByPlayer(ItemStack stack, EntityPlayer player)
    {
        return wrappedItem.onDroppedByPlayer(stack, player);
    }

    @Override
    public boolean onBlockStartBreak(ItemStack stack, BlockPos pos, EntityPlayer player)
    {
        return wrappedItem.onBlockStartBreak(stack, pos, player);
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World world, EntityLivingBase living)
    {
        return wrappedItem.onItemUseFinish(stack, world, living);
    }

    @Override
    public float getStrVsBlock(ItemStack stack, IBlockState state)
    {
        return wrappedItem.getStrVsBlock(stack, state);
    }

    @Override
    public boolean canHarvestBlock(IBlockState state, ItemStack stack)
    {
        return wrappedItem.canHarvestBlock(state);
    }

    @Override
    public int getMaxItemUseDuration(ItemStack stack)
    {
        return wrappedItem.getMaxItemUseDuration(stack);
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean advanced)
    {
        wrappedItem.addInformation(stack, player, list, advanced);
    }

    @Override
    public EnumAction getItemUseAction(ItemStack stack)
    {
        return wrappedItem.getItemUseAction(stack);
    }

    @Override
    public int getMaxDamage()
    {
        return wrappedItem.getMaxDamage();
    }

    @Override
    public boolean isDamageable()
    {
        return wrappedItem.isDamageable();
    }

    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EntityEquipmentSlot slot, ItemStack stack)
    {
        return wrappedItem.getAttributeModifiers(super.getAttributeModifiers(slot, stack), ATTACK_DAMAGE_MODIFIER);
    }

    @Override
    public boolean hasContainerItem(ItemStack stack)
    {
        return wrappedItem.hasContainerItem(stack);
    }

    @Override
    public ItemStack getContainerItem(ItemStack stack)
    {
        return wrappedItem.getContainerItem(stack);
    }


    @Override
    public int getItemEnchantability()
    {
        return wrappedItem.getItemEnchantability();
    }

    @Override
    public boolean getIsRepairable(ItemStack stack, ItemStack material)
    {
        return wrappedItem.getIsRepairable(stack, material);
    }


}
