package cubex2.cs3.item;

import cubex2.cs3.block.BlockCSDoor;
import cubex2.cs3.block.attributes.DoorAttributes;
import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.List;

public class ItemCSDoor extends Item
{
    private WrappedBlock wrappedBlock;
    private DoorAttributes container;


    public ItemCSDoor(WrappedBlock wrappedBlock)
    {
        super();
        this.wrappedBlock = wrappedBlock;
        container = (DoorAttributes) wrappedBlock.container;
        setMaxStackSize(wrappedBlock.container.maxStack);
    }

    @Override
    public int getMetadata(int i)
    {
        return 0;
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if (side != EnumFacing.UP)
        {
            return EnumActionResult.FAIL;
        } else
        {
            IBlockState iblockstate = world.getBlockState(pos);
            Block block = iblockstate.getBlock();

            if (!block.isReplaceable(world, pos))
            {
                pos = pos.offset(side);
            }

            if (!player.canPlayerEdit(pos, side, player.getHeldItem(hand)))
            {
                return EnumActionResult.FAIL;
            } else if (!this.wrappedBlock.block.canPlaceBlockAt(world, pos))
            {
                return EnumActionResult.FAIL;
            } else
            {
                placeDoor(world, pos, EnumFacing.fromAngle((double) player.rotationYaw), this.wrappedBlock.block);
                player.getHeldItem(hand).shrink(1);
                return EnumActionResult.SUCCESS;
            }
        }
    }

    public static void placeDoor(World world, BlockPos pos, EnumFacing facing, Block doorBlock)
    {
        BlockPos blockpos1 = pos.offset(facing.rotateY());
        BlockPos blockpos2 = pos.offset(facing.rotateYCCW());
        int i = (world.getBlockState(blockpos2).isNormalCube() ? 1 : 0) + (world.getBlockState(blockpos2.up()).isNormalCube() ? 1 : 0);
        int j = (world.getBlockState(blockpos1).isNormalCube() ? 1 : 0) + (world.getBlockState(blockpos1.up()).isNormalCube() ? 1 : 0);
        boolean flag = world.getBlockState(blockpos2).getBlock() == doorBlock || world.getBlockState(blockpos2.up()).getBlock() == doorBlock;
        boolean flag1 = world.getBlockState(blockpos1).getBlock() == doorBlock || world.getBlockState(blockpos1.up()).getBlock() == doorBlock;
        boolean flag2 = false;

        if (flag && !flag1 || j > i)
        {
            flag2 = true;
        }

        BlockPos blockpos3 = pos.up();
        IBlockState iblockstate = doorBlock.getDefaultState().withProperty(BlockCSDoor.FACING, facing).withProperty(BlockCSDoor.HINGE, flag2 ? BlockDoor.EnumHingePosition.RIGHT : BlockDoor.EnumHingePosition.LEFT);
        world.setBlockState(pos, iblockstate.withProperty(BlockCSDoor.HALF, BlockDoor.EnumDoorHalf.LOWER), 2);
        world.setBlockState(blockpos3, iblockstate.withProperty(BlockCSDoor.HALF, BlockDoor.EnumDoorHalf.UPPER), 2);
        world.notifyNeighborsOfStateChange(pos, doorBlock, false);
        world.notifyNeighborsOfStateChange(blockpos3, doorBlock, false);
    }

    @Override
    public CreativeTabs getCreativeTab()
    {
        return wrappedBlock.block.getCreativeTabToDisplayOn();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void getSubItems(Item item, CreativeTabs creativeTabs, NonNullList<ItemStack> list)
    {
        wrappedBlock.block.getSubBlocks(item, creativeTabs, list);
    }

    @Override
    public String getUnlocalizedName(ItemStack itemstack)
    {
        return wrappedBlock.block.getUnlocalizedName();
    }

    @Override
    public String getUnlocalizedName()
    {
        return wrappedBlock.block.getUnlocalizedName();
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean flag)
    {
        wrappedBlock.addInformation(stack, player, list, flag);
    }
}
