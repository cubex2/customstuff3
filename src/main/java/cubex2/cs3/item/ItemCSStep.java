package cubex2.cs3.item;

import cubex2.cs3.block.BlockCSStep;
import cubex2.cs3.common.WrappedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemCSStep extends ItemCSBlock
{
    private BlockCSStep block;

    public ItemCSStep(Block block, WrappedBlock wrappedBlock)
    {
        super(block, wrappedBlock);
        this.block = (BlockCSStep) block;
    }

    @Override
    public int getMetadata(int metadata)
    {
        return metadata;
    }

    @Override
    public String getUnlocalizedName(ItemStack itemstack)
    {
        return block.getUnlocalizedName();
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        ItemStack stack = player.getHeldItem(hand);

        if (stack.getCount() == 0)
        {
            return EnumActionResult.FAIL;
        } else if (!player.canPlayerEdit(pos, side, stack))
        {
            return EnumActionResult.FAIL;
        } else
        {
            IBlockState state = world.getBlockState(pos);
            Block block1 = state.getBlock();
            if (block1 == block)
            {
                boolean isTop = state.getValue(BlockCSStep.HALF) == BlockSlab.EnumBlockHalf.TOP;
                Block doubleSlabBlock = block.getDoubleSlabBlock(stack.getItemDamage());

                if ((side == EnumFacing.UP && !isTop || side == EnumFacing.DOWN && isTop))
                {
                    if (doubleSlabBlock != null && world.checkNoEntityCollision(doubleSlabBlock.getCollisionBoundingBox(state, world, pos)) && world.setBlockState(pos, doubleSlabBlock.getStateFromMeta(block.getDoubleSlabMeta(stack.getItemDamage())), 3))
                    {
                        SoundType soundtype = doubleSlabBlock.getSoundType();
                        world.playSound(player, pos, soundtype.getPlaceSound(), SoundCategory.BLOCKS, (soundtype.getVolume() + 1.0F) / 2.0F, soundtype.getPitch() * 0.8F);
                        stack.shrink(1);
                    }

                    return EnumActionResult.SUCCESS;
                }
            }

            return tryPlace(block, stack, player, world, pos.offset(side), side) ? EnumActionResult.SUCCESS : super.onItemUse(player, world, pos, hand, side, hitX, hitY, hitZ);
        }
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean canPlaceBlockOnSide(World world, BlockPos pos, EnumFacing side, EntityPlayer player, ItemStack stack)
    {
        BlockPos pos1 = pos;
        IBlockState state = world.getBlockState(pos);

        if (state.getBlock() == this.block)
        {
            boolean flag = state.getValue(BlockSlab.HALF) == BlockSlab.EnumBlockHalf.TOP;

            if ((side == EnumFacing.UP && !flag || side == EnumFacing.DOWN && flag))
            {
                return true;
            }
        }

        pos = pos.offset(side);
        IBlockState state1 = world.getBlockState(pos);
        return state1.getBlock() == this.block || super.canPlaceBlockOnSide(world, pos1, side, player, stack);
    }

    private static boolean tryPlace(BlockCSStep block, ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side)
    {
        IBlockState state = world.getBlockState(pos);

        Block doubleSlabBlock = block.getDoubleSlabBlock(stack.getItemDamage());
        if (doubleSlabBlock != null && state.getBlock() == block)
        {
            if (world.checkNoEntityCollision(doubleSlabBlock.getCollisionBoundingBox(state, world, pos)) && world.setBlockState(pos, doubleSlabBlock.getStateFromMeta(block.getDoubleSlabMeta(stack.getItemDamage())), 3))
            {
                SoundType soundtype = doubleSlabBlock.getSoundType();
                world.playSound(player, pos, soundtype.getPlaceSound(), SoundCategory.BLOCKS, (soundtype.getVolume() + 1.0F) / 2.0F, soundtype.getPitch() * 0.8F);
                stack.shrink(1);
            }

            return true;
        } else
            return false;
    }
}
