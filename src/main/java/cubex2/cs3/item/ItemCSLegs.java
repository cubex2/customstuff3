package cubex2.cs3.item;

import cubex2.cs3.common.WrappedItem;
import net.minecraft.inventory.EntityEquipmentSlot;

public class ItemCSLegs extends ItemCSArmor
{
    public ItemCSLegs(WrappedItem item)
    {
        super(item, EntityEquipmentSlot.LEGS);
    }
}
