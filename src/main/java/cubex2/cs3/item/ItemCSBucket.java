package cubex2.cs3.item;

import cubex2.cs3.common.WrappedItem;
import cubex2.cs3.item.attributes.BucketAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.event.ForgeEventFactory;

public class ItemCSBucket extends ItemCS
{
    private BucketAttributes container;

    public ItemCSBucket(WrappedItem item)
    {
        super(item);

        container = (BucketAttributes) item.container;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand)
    {
        super.onItemRightClick(world, player, hand);

        ItemStack stack = player.getHeldItem(hand);

        RayTraceResult pos = this.rayTrace(world, player, false);
        ActionResult<ItemStack> ret = ForgeEventFactory.onBucketUse(player, world, stack, pos);
        if (ret != null) return ret;

        if (pos == null)
        {
            return new ActionResult<>(EnumActionResult.PASS, stack);
        } else if (pos.typeOfHit != RayTraceResult.Type.BLOCK)
        {
            return new ActionResult<>(EnumActionResult.PASS, stack);
        } else
        {
            BlockPos bpos = pos.getBlockPos();

            if (!world.isBlockModifiable(player, bpos))
            {
                return new ActionResult<>(EnumActionResult.FAIL, stack);
            } else
            {
                boolean flag1 = world.getBlockState(bpos).getBlock().isReplaceable(world, bpos);
                BlockPos bpos1 = flag1 && pos.sideHit == EnumFacing.UP ? bpos : bpos.offset(pos.sideHit);

                if (!player.canPlayerEdit(bpos1, pos.sideHit, stack))
                {
                    return new ActionResult<>(EnumActionResult.FAIL, stack);
                } else if (placeFluid(world, bpos1))
                {
                    player.addStat(StatList.getObjectUseStats(this));
                    return !player.capabilities.isCreativeMode ? new ActionResult<>(EnumActionResult.SUCCESS, new ItemStack(Items.BUCKET))
                                                               : new ActionResult<>(EnumActionResult.SUCCESS, stack);
                } else
                {
                    return new ActionResult<>(EnumActionResult.FAIL, stack);
                }
            }
        }

    }

    public boolean placeFluid(World world, BlockPos pos)
    {
        if (!world.isAirBlock(pos) && world.getBlockState(pos).getMaterial().isSolid())
            return false;
        else
        {
            if (world.provider.doesWaterVaporize() /*
                                            * this.isFull == Block.waterMoving.blockID
                                            *//* TODO allow in nether */)
            {
                world.playSound(null, pos, SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.5F, 2.6F + (world.rand.nextFloat() - world.rand.nextFloat()) * 0.8F);

                for (int var11 = 0; var11 < 8; ++var11)
                {
                    world.spawnParticle(EnumParticleTypes.SMOKE_LARGE, pos.getX() + Math.random(), pos.getY() + Math.random(), pos.getZ() + Math.random(), 0.0D, 0.0D, 0.0D);
                }
            } else
            {
                world.setBlockState(pos, container.fluid.getDefaultState(), 3);
            }

            return true;
        }
    }
}
