package cubex2.cs3.item;

import cubex2.cs3.common.WrappedItem;
import cubex2.cs3.item.attributes.FoodAttributes;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;

public class ItemCSFood extends ItemCS
{
    private FoodAttributes container;

    public ItemCSFood(WrappedItem item)
    {
        super(item);
        container = (FoodAttributes) item.container;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand)
    {
        if (player.canEat(container.alwaysEdible))
        {
            player.setActiveHand(hand);
        }

        return super.onItemRightClick(world, player, hand);
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World world, EntityLivingBase living)
    {
        stack.shrink(1);
        if (living instanceof EntityPlayer)
        {
            EntityPlayer player = (EntityPlayer) living;
            player.getFoodStats().addStats(container.hunger, container.saturation);
            world.playSound((EntityPlayer) null, player.posX, player.posY, player.posZ, SoundEvents.ENTITY_PLAYER_BURP, SoundCategory.PLAYERS, 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
            this.onFoodEaten(stack, world, player);
        }

        return super.onItemUseFinish(stack, world, living);
    }

    protected void onFoodEaten(ItemStack stack, World world, EntityPlayer player)
    {
        if (container.potion != null && !world.isRemote && world.rand.nextFloat() < container.potionProbability)
        {
            player.addPotionEffect(new PotionEffect(container.potion, container.potionDuration * 20, container.potionAmplifier));
        }
    }
}
