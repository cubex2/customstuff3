package cubex2.cs3.item;

import cubex2.cs3.common.WrappedItem;
import net.minecraft.inventory.EntityEquipmentSlot;

public class ItemCSBoots extends ItemCSArmor
{
    public ItemCSBoots(WrappedItem item)
    {
        super(item, EntityEquipmentSlot.FEET);
    }
}
