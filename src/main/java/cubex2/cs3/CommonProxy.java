package cubex2.cs3;

import cubex2.cs3.common.WrappedBlock;
import cubex2.cs3.common.WrappedItem;
import cubex2.cs3.entity.EntityCSGravityBlock;
import cubex2.cs3.handler.event.BonemealHandler;
import cubex2.cs3.handler.event.FillBucketHandler;
import cubex2.cs3.lib.ModInfo;
import cubex2.cs3.tileentity.TileEntityCS;
import cubex2.cs3.tileentity.TileEntityInventory;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy
{
    public void registerKeyBindings()
    {
        // do nothing
    }

    public void registerEventHandlers()
    {
        MinecraftForge.EVENT_BUS.register(new BonemealHandler());
        MinecraftForge.EVENT_BUS.register(new FillBucketHandler());
    }

    public void registerEntities()
    {
        EntityRegistry.registerModEntity(new ResourceLocation(ModInfo.ID, "cs_gravityblock"), EntityCSGravityBlock.class, "cs_gravityblock", 1, CustomStuff3.instance, 80, 1, true);
    }

    public void registerTileEntities()
    {
        GameRegistry.registerTileEntity(TileEntityCS.class, "cs3_normal");
        GameRegistry.registerTileEntity(TileEntityInventory.class, "cs3_inventory");
    }

    public void initRendering()
    {
        // do nothing
    }

    public void registerModels(WrappedBlock wrappedBlock, Block block)
    {
        // do nothing
    }

    public void registerModels(WrappedItem wrappedItem, Item item)
    {
        // do nothing
    }
}
