package cubex2.cs3.util;

import com.google.common.collect.Lists;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameData;
import net.minecraftforge.oredict.OreDictionary;

import javax.annotation.Nonnull;
import java.util.List;

public class ItemStackHelper
{
    public static void damageItem(ItemStack stack, int amount)
    {
        if (stack.isItemStackDamageable())
        {
            stack.setItemDamage(stack.getItemDamage() + amount);

            if (stack.getItemDamage() > stack.getMaxDamage())
            {
                stack.shrink(1);

                if (stack.getCount() < 0)
                {
                    stack.setCount(0);
                }

                stack.setItemDamage(0);
            }
        }
    }

    public static boolean hasEnchantment(ItemStack stack, int id, int level)
    {
        if (!stack.isItemEnchanted())
            return false;
        NBTTagCompound tag = stack.getTagCompound();
        NBTTagList tagList = (NBTTagList) tag.getTag("ench");
        for (int i = 0; i < tagList.tagCount(); i++)
        {
            NBTTagCompound tag1 = tagList.getCompoundTagAt(i);
            boolean idMatch = id == -1 || tag1.getShort("id") == id;
            boolean lvlMatch = level == -1 || tag1.getShort("lvl") == level;
            if (idMatch && lvlMatch)
                return true;
        }
        return false;
    }

    public static void clearEnchantments(ItemStack stack)
    {
        if (!stack.isItemEnchanted())
            return;
        NBTTagCompound tag = stack.getTagCompound();
        tag.removeTag("ench");
    }

    public static void addEnchantment(ItemStack stack, int id, int level)
    {
        Enchantment enchantment = Enchantment.getEnchantmentByID(id);
        if (enchantment != null)
        {
            stack.addEnchantment(enchantment, level);
        }
    }

    public static void removeEnchantment(ItemStack stack, int id)
    {
        if (hasEnchantment(stack, id, -1))
        {
            NBTTagCompound tag = stack.getTagCompound();
            NBTTagList tagList = (NBTTagList) tag.getTag("ench");
            for (int i = 0; i < tagList.tagCount(); i++)
            {
                NBTTagCompound tag1 = tagList.getCompoundTagAt(i);
                if (tag1.getShort("id") == id)
                {
                    tagList.removeTag(i);
                    break;
                }
            }
            if (tagList.tagCount() == 0)
            {
                tag.removeTag("ench");
            }
        }
    }

    public static int getIntData(ItemStack stack, String name)
    {
        return NBTHelper.getCSIntData(stack.getTagCompound(), name);
    }

    public static float getFloatData(ItemStack stack, String name)
    {
        return NBTHelper.getCSFloatData(stack.getTagCompound(), name);
    }

    public static String getStringData(ItemStack stack, String name)
    {
        return NBTHelper.getCSStringData(stack.getTagCompound(), name);
    }

    public static void setIntData(ItemStack stack, String name, int data)
    {
        initNBTTag(stack);
        NBTHelper.setCSIntData(stack.getTagCompound(), name, data);
    }

    public static void setFloatData(ItemStack stack, String name, float data)
    {
        initNBTTag(stack);
        NBTHelper.setCSFloatData(stack.getTagCompound(), name, data);
    }

    public static void setStringData(ItemStack stack, String name, String data)
    {
        initNBTTag(stack);
        NBTHelper.setCSStringData(stack.getTagCompound(), name, data);
    }

    public static void initNBTTag(ItemStack stack)
    {
        if (stack.getTagCompound() == null)
        {
            stack.setTagCompound(new NBTTagCompound());
        }
    }

    public static NonNullList<ItemStack> getSubTypes(Item item)
    {
        NonNullList<ItemStack> stacks = NonNullList.create();
        item.getSubItems(item, null, stacks);
        return stacks;
    }

    public static NonNullList<ItemStack> getAllItemStacks()
    {
        return getAllItemStacks(true);
    }

    public static NonNullList<ItemStack> getAllItemStacks(boolean wildCardStacks)
    {
        List<ItemStack> stacks = Lists.newArrayList();

        for (ResourceLocation location : GameData.getItemRegistry().getKeys())
        {
            Item item = GameData.getItemRegistry().getObject(location);

            List<ItemStack> itemStacks = Lists.newArrayList();
            if (item.getHasSubtypes())
            {
                itemStacks.addAll(ItemStackHelper.getSubTypes(item));
                if (wildCardStacks && itemStacks.size() > 1)
                {
                    try
                    {
                        itemStacks.add(new ItemStack(item, 1, OreDictionary.WILDCARD_VALUE));
                    } catch (Exception ignored)
                    {
                        // Some mod items cause a crash
                    }
                }
            } else if (item.isDamageable())
            {
                itemStacks.add(new ItemStack(item, 1, 0));
                if (wildCardStacks)
                {
                    try
                    {
                        itemStacks.add(new ItemStack(item, 1, OreDictionary.WILDCARD_VALUE));
                    } catch (Exception ignored)
                    {
                        // Some mod items cause a crash
                    }
                }
            } else
            {
                itemStacks.add(new ItemStack(item, 1, 0));
            }
            stacks.addAll(itemStacks);
        }
        return toNonNullList(stacks);
    }

    public static NonNullList<ItemStack> getBlockStacks(boolean wildCardStacks)
    {
        NonNullList<ItemStack> stacks = NonNullList.create();

        for (ResourceLocation location : GameData.getBlockRegistry().getKeys())
        {
            Block block = GameData.getBlockRegistry().getObject(location);
            Item item = Item.getItemFromBlock(block);
            block.getSubBlocks(Item.getItemFromBlock(block), CreativeTabs.SEARCH, stacks);
            if (item != null && wildCardStacks && item.getHasSubtypes())
            {
                try
                {
                    stacks.add(new ItemStack(item, 1, OreDictionary.WILDCARD_VALUE));
                } catch (Exception ignored)
                {
                    // Some mod items cause a crash
                }
            }
        }

        for (int i = 0; i < stacks.size(); i++)
        {
            if (stacks.get(i).getItem() == null)
            {
                stacks.remove(i--);
            }
        }

        return stacks;
    }

    public static NonNullList<ItemStack> getBlockStacks()
    {
        List<ItemStack> stacks = Lists.newArrayList();

        for (ResourceLocation location : GameData.getBlockRegistry().getKeys())
        {
            Block block = GameData.getBlockRegistry().getObject(location);
            Item item = Item.getItemFromBlock(block);
            if (item != null && item instanceof ItemBlock)
            {
                if (item.getHasSubtypes())
                {
                    NonNullList<ItemStack> list = NonNullList.create();
                    item.getSubItems(item, CreativeTabs.SEARCH, list);
                    stacks.addAll(list);
                } else
                {
                    stacks.add(new ItemStack(item, 1, 0));
                }
            }
        }

        return toNonNullList(stacks);
    }

    public static NonNullList<ItemStack> toNonNullList(List<ItemStack> stacks)
    {
        NonNullList<ItemStack> ret = NonNullList.withSize(stacks.size(), ItemStack.EMPTY);
        int index = 0;
        for (ItemStack stack : stacks)
        {
            ret.set(index, stack != null ? stack : ItemStack.EMPTY);
            index++;
        }
        return ret;
    }

    public static List<ItemStack> getOreItemStacks(String oreName)
    {
        List<ItemStack> stacks = Lists.newArrayList();
        List<ItemStack> oreStacks = OreDictionary.getOres(oreName);

        for (int i = 0; i < oreStacks.size(); i++)
        {
            ItemStack stack = oreStacks.get(i);
            Item item = stack.getItem();
            if (stack.getItemDamage() == OreDictionary.WILDCARD_VALUE && item != null)
            {
                if (item.getHasSubtypes())
                {
                    stacks.addAll(ItemStackHelper.getSubTypes(item));
                } else
                {
                    stacks.add(new ItemStack(item, 1, 0));
                }
            } else
            {
                stacks.add(stack.copy());
            }
        }
        return stacks;
    }

    public static boolean itemStackEqual(ItemStack is1, ItemStack is2)
    {
        if (is1 == is2)
            return true;
        if (is1 == null ^ is2 == null)
            return false;
        if (is1.isEmpty() && is1.isEmpty())
            return true;
        if (is1.isEmpty() ^ is2.isEmpty())
            return false;
        if (is1.getItem() == null && is2.getItem() == null)
            return true;
        if (is1.getItem() == null ^ is2.getItem() == null)
            return false;

        boolean itemEqual = is1.getItem() == is2.getItem();

        boolean damageEqual;
        if (is1.getItemDamage() == OreDictionary.WILDCARD_VALUE || is2.getItemDamage() == OreDictionary.WILDCARD_VALUE)
        {
            damageEqual = true;
        } else
        {
            damageEqual = is1.getItemDamage() == is2.getItemDamage();
        }

        return itemEqual && damageEqual;
    }

    public static void writeToNBTNamed(ItemStack stack, NBTTagCompound nbt)
    {
        ResourceLocation location = Item.REGISTRY.getNameForObject(stack.getItem());
        nbt.setString("ItemName", location.toString());
        nbt.setByte("Count", (byte) stack.getCount());
        nbt.setShort("Damage", (short) stack.getItemDamage());

        if (stack.getTagCompound() != null)
        {
            nbt.setTag("tag", stack.getTagCompound());
        }
    }

    public static NBTTagCompound writeToNBTNamed(ItemStack stack)
    {
        NBTTagCompound nbt = new NBTTagCompound();
        writeToNBTNamed(stack, nbt);
        return nbt;
    }

    @Nonnull
    public static ItemStack readFromNBTNamed(NBTTagCompound compound)
    {
        if (!compound.hasKey("ItemName"))
            return ItemStack.EMPTY;


        Item item = GameData.getItemRegistry().getObject(new ResourceLocation(compound.getString("ItemName")));
        if (item == null)
            return ItemStack.EMPTY;

        ItemStack stack = new ItemStack(item);
        stack.setCount(compound.getByte("Count"));
        stack.setItemDamage(compound.getShort("Damage"));

        if (compound.hasKey("tag", 10))
        {
            stack.setTagCompound(compound.getCompoundTag("tag"));
        }

        return stack;
    }
}
