package cubex2.cs3.util;

import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.Biome;

public class SimulatedWorld implements IBlockAccess
{
    private IBlockState[] blocks;
    public final int minX, minY, minZ;
    public final int maxX, maxY, maxZ;
    private final int xSize, ySize, zSize;

    public SimulatedWorld(int minX, int minY, int minZ, int maxX, int maxY, int maxZ)
    {
        this.minX = minX;
        this.minY = minY;
        this.minZ = minZ;
        this.maxX = maxX;
        this.maxY = maxY;
        this.maxZ = maxZ;

        xSize = maxX - minX + 1;
        ySize = maxY - minY + 1;
        zSize = maxZ - minZ + 1;

        blocks = new IBlockState[xSize * ySize * zSize];
    }

    @Override
    public IBlockState getBlockState(BlockPos pos)
    {
        int x = pos.getX();
        int y = pos.getY();
        int z = pos.getZ();

        if (x < minX || y < minY || z < minZ || x > maxX || y > maxY || z > maxZ)
            return Blocks.AIR.getDefaultState();

        x += Math.abs(Math.min(0, minX));
        y += Math.abs(Math.min(0, minY));
        z += Math.abs(Math.min(0, minZ));

        IBlockState state = blocks[x + y * xSize + z * ySize * xSize];
        return state == null ? Blocks.AIR.getDefaultState() : state;
    }

    public void setBlock(BlockPos pos, IBlockState state)
    {
        int x = pos.getX();
        int y = pos.getY();
        int z = pos.getZ();

        if (x < minX || y < minY || z < minZ || x > maxX || y > maxY || z > maxZ)
            return;

        x += Math.abs(Math.min(0, minX));
        y += Math.abs(Math.min(0, minY));
        z += Math.abs(Math.min(0, minZ));

        blocks[x + y * xSize + z * ySize * xSize] = state;
    }

    public void setMetadata(BlockPos pos, int meta)
    {
        setBlock(pos, getBlockState(pos).getBlock().getStateFromMeta(meta));
    }

    public int getMetadata(BlockPos pos)
    {
        IBlockState state = getBlockState(pos);
        return state.getBlock().getMetaFromState(state);
    }

    @Override
    public TileEntity getTileEntity(BlockPos pos)
    {
        return null;
    }

    @Override
    public int getCombinedLight(BlockPos pos, int side)
    {
        return 15;
    }

    @Override
    public int getStrongPower(BlockPos pos, EnumFacing side)
    {
        return 0;
    }

    @Override
    public WorldType getWorldType()
    {
        return WorldType.DEFAULT;
    }

    @Override
    public boolean isAirBlock(BlockPos pos)
    {
        return getBlockState(pos).getBlock() == Blocks.AIR;
    }

    @Override
    public Biome getBiome(BlockPos pos)
    {
        return Biomes.PLAINS;
    }

    @Override
    public boolean isSideSolid(BlockPos pos, EnumFacing side, boolean _default)
    {
        if (pos.getX() < minX || pos.getY() < minY || pos.getZ() < minZ
            || pos.getX() > maxX || pos.getY() > maxY || pos.getZ() > maxZ)
            return _default;

        IBlockState blockState = getBlockState(pos);
        return blockState.getBlock().isSideSolid(blockState, this, pos, side);
    }

    public void clear()
    {
        for (int i = 0; i < blocks.length; i++)
        {
            blocks[i] = null;
        }
    }
}
