package cubex2.cs3.util;

import com.google.common.collect.Lists;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.oredict.OreDictionary;

import java.util.List;

public class RecipeInput
{
    private ItemStack stack = ItemStack.EMPTY;
    private String oreClass;
    public boolean damageItem;
    public int damageAmount;

    public RecipeInput(ItemStack stack)
    {
        this.stack = stack;
    }

    public RecipeInput(String oreClass)
    {
        this.oreClass = oreClass;
    }

    public boolean isOreClass()
    {
        return oreClass != null;
    }

    public Object getInput()
    {
        return isOreClass() ? oreClass : stack;
    }

    public boolean hasValidInput()
    {
        return isOreClass() ? oreClass != null : !stack.isEmpty();
    }

    public List<ItemStack> getStacks()
    {
        List<ItemStack> stacks = Lists.newArrayList();
        if (!stack.isEmpty())
        {
            stacks.add(stack.copy());
        } else
        {
            stacks.addAll(OreDictionary.getOres(oreClass));
        }
        return stacks;
    }

    public void writeToNBT(NBTTagCompound compound)
    {
        compound.setBoolean("IsOre", oreClass != null);
        if (oreClass != null)
        {
            compound.setString("OreClass", oreClass);
        } else
        {
            compound.setTag("Stack", ItemStackHelper.writeToNBTNamed(stack));
        }
        compound.setBoolean("DamageItem", damageItem);
        if (damageItem)
            compound.setInteger("DamageAmount", damageAmount);
    }

    public static RecipeInput loadFromNBT(NBTTagCompound compound)
    {
        if (!compound.hasKey("IsOre"))
        {
            return null;
        }

        RecipeInput result;
        boolean isOre = compound.getBoolean("IsOre");
        if (isOre)
        {
            result = new RecipeInput(compound.getString("OreClass"));
        } else
        {
            result = new RecipeInput(ItemStackHelper.readFromNBTNamed(compound.getCompoundTag("Stack")));
        }

        result.damageItem = compound.getBoolean("DamageItem");
        if (result.damageItem)
            result.damageAmount = compound.getInteger("DamageAmount");

        return result;
    }
}
