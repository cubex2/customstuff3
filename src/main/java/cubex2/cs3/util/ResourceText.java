package cubex2.cs3.util;

import com.google.common.collect.Maps;

import java.util.Map;

public class ResourceText
{
    private static final Map<String, String> texts = Maps.newHashMap();

    public static String fromPath(String path)
    {
        if (texts.containsKey(path))
            return texts.get(path);

        texts.put(path, ClientHelper.loadResFile(path));

        return texts.get(path);
    }
}
