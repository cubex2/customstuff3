package cubex2.cs3.util;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.lang3.StringUtils;

public interface Filter<T>
{
    boolean matches(T obj, String searchText);

    Filter<ItemStack> ITEM_STACK = (obj, searchText) -> obj != null && StringUtils.containsIgnoreCase(obj.getDisplayName(), searchText);

    Filter<OreDictionaryClass> ORE_CLASS = (obj, searchText) -> obj != null && StringUtils.containsIgnoreCase(obj.oreClass, searchText);

    Filter<ResourceLocation> RESOURCE_LOCATION = (obj, searchText) -> obj != null && StringUtils.containsIgnoreCase(obj.toString(), searchText);

}
