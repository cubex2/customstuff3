package cubex2.cs3.util;

import cubex2.cs3.lib.Biomes;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.IThreadListener;
import net.minecraft.util.NonNullList;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.util.Collection;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class Util
{
    public static <T extends IMessage> boolean checkThreadAndEnqueue(final T message, final IMessageHandler<T, IMessage> handler, final MessageContext ctx, IThreadListener listener)
    {
        if (!listener.isCallingFromMinecraftThread())
        {
            listener.addScheduledTask(() -> handler.onMessage(message, ctx));
            return true;
        }

        return false;
    }

    public static void writeStacksToNBT(String name, NonNullList<ItemStack> stacks, NBTTagCompound nbt)
    {
        NBTTagList tagList = new NBTTagList();

        for (int i = 0; i < stacks.size(); i++)
        {
            if (!stacks.get(i).isEmpty())
            {
                NBTTagCompound compound = new NBTTagCompound();
                compound.setByte("Slot", (byte) i);
                stacks.get(i).writeToNBT(compound);
                tagList.appendTag(compound);
            }
        }

        nbt.setTag(name, tagList);
    }

    public static void readStacksFromNBT(String name, NonNullList<ItemStack> stacks, NBTTagCompound nbt)
    {
        NBTTagList tagList = nbt.getTagList(name, 10);

        for (int i = 0; i < tagList.tagCount(); i++)
        {
            NBTTagCompound compound = tagList.getCompoundTagAt(i);
            int j = compound.getByte("Slot") & 255;

            if (j >= 0 && j < stacks.size())
            {
                stacks.set(j, new ItemStack(compound));
            }
        }
    }

    public static <T extends NBTData> void writeListToNBT(String name, Collection<T> list, NBTTagCompound nbt)
    {
        NBTTagList tagList = new NBTTagList();
        for (NBTData data : list)
        {
            NBTTagCompound compound = new NBTTagCompound();
            data.writeToNBT(compound);
            tagList.appendTag(compound);
        }
        nbt.setTag(name, tagList);
    }

    public static <T extends NBTData> void readListFromNBT(String name, Collection<T> list, NBTTagCompound nbt, Class<T> clazz)
    {
        NBTTagList tagList = nbt.getTagList(name, Constants.NBT.TAG_COMPOUND);
        for (int i = 0; i < tagList.tagCount(); i++)
        {
            try
            {
                NBTTagCompound compound = tagList.getCompoundTagAt(i);
                T data = clazz.newInstance();
                data.readFromNBT(compound);
                list.add(data);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public static <T> void writeListToNBT(String listName, List<T> list, BiConsumer<T, NBTTagCompound> writer, NBTTagCompound nbt)
    {
        NBTTagList tagList = new NBTTagList();
        for (T t : list)
        {
            NBTTagCompound compound = new NBTTagCompound();
            writer.accept(t, compound);
            tagList.appendTag(compound);
        }

        nbt.setTag(listName, tagList);
    }

    public static <T> void readListFromNBT(String listName, List<T> list, Function<NBTTagCompound, T> reader, NBTTagCompound nbt)
    {
        if (!nbt.hasKey(listName))
            return;

        NBTTagList tagList = nbt.getTagList(listName, Constants.NBT.TAG_COMPOUND);
        for (int i = 0; i < tagList.tagCount(); i++)
        {
            NBTTagCompound compound = tagList.getCompoundTagAt(i);
            list.add(reader.apply(compound));
        }
    }

    public static final BiConsumer<ItemStack, NBTTagCompound> NBT_ITEMSTACK_WRITER = (itemStack, nbtTagCompound) ->
    {
        if (itemStack != null)
        {
            itemStack.writeToNBT(nbtTagCompound);
        }
    };

    public static final Function<NBTTagCompound, ItemStack> NBT_ITEMSTACK_READER = ItemStack::new;

    public static final BiConsumer<String, NBTTagCompound> NBT_STRING_WRITER = (string, nbtTagCompound) ->
    {
        if (string != null && string.length() > 0)
        {
            nbtTagCompound.setString("string", string);
        }
    };

    public static final Function<NBTTagCompound, String> NBT_STRING_READER = nbtTagCompound -> nbtTagCompound.getString("string");

    public static final BiConsumer<Integer, NBTTagCompound> NBT_INTEGER_WRITER = (i, nbtTagCompound) ->
    {
        if (i != null)
        {
            nbtTagCompound.setInteger("int", i);
        }
    };

    public static final Function<NBTTagCompound, Integer> NBT_INTEGER_READER = nbtTagCompound -> nbtTagCompound.getInteger("int");

    public static final BiConsumer<Biome, NBTTagCompound> NBT_BIOME_WRITER = (biome, nbtTagCompound) ->
    {
        if (biome != null && biome.getRegistryName() != null)
        {
            nbtTagCompound.setString("Value", biome.getRegistryName().toString());
        }
    };

    public static final Function<NBTTagCompound, Biome> NBT_BIOME_READER = nbtTagCompound -> Biomes.getBiome(nbtTagCompound.getString("Value"));
}
