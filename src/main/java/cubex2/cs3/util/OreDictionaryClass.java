package cubex2.cs3.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraftforge.oredict.OreDictionary;

import java.util.List;
import java.util.Map;

public final class OreDictionaryClass implements Comparable<OreDictionaryClass>
{
    private static final Map<String, OreDictionaryClass> instances = Maps.newHashMap();

    private static OreDictionaryClass getInstance(String oreClass)
    {
        if (!instances.containsKey(oreClass))
            instances.put(oreClass, new OreDictionaryClass(oreClass));

        return instances.get(oreClass);
    }

    public static List<OreDictionaryClass> getAllClasses()
    {
        List<OreDictionaryClass> list = Lists.newArrayList();
        for (String s : OreDictionary.getOreNames())
        {
            OreDictionaryClass instance = getInstance(s);
            if (instance != null)
            {
                list.add(instance);
            }
        }
        return list;
    }

    public final String oreClass;

    private OreDictionaryClass(String oreClass)
    {
        this.oreClass = oreClass == null ? "null" : oreClass;
    }

    @Override
    public int compareTo(OreDictionaryClass o)
    {
        return oreClass.compareTo(o.oreClass);
    }
}
