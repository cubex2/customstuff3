package cubex2.cs3.util;

import cubex2.cs3.api.scripting.IScriptableObjectProvider;
import cubex2.cs3.api.scripting.ITriggerData;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.scripting.*;
import net.minecraft.item.ItemStack;

import javax.script.*;
import java.util.List;

public class JavaScriptHelper
{
    public static BaseContentPack executingPack = null;

    private static Compilable compiler;
    private static ScriptEngine engine;

    static
    {
        engine = new ScriptEngineManager().getEngineByName("nashorn");
        compiler = (Compilable) engine;
    }

    public static CompiledScript createScript(String source, String sourceName)
    {
        try
        {
            source = "var Position = Java.type('cubex2.cs3.common.scripting.ScriptablePosition');" + source;
            return compiler.compile(source);
        } catch (ScriptException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static void executeTrigger(CompiledScript script, ITriggerData data, BaseContentPack pack)
    {
        try
        {
            executeTrigger(script, data, pack, null);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static <T> T executeTrigger(CompiledScript script, ITriggerData data, BaseContentPack pack, T defaultResult)
    {
        T result = defaultResult;

        try
        {
            Bindings bindings = new SimpleBindings();

            for (IScriptableObjectProvider provider : ScriptObjectManager.providers)
            {
                List<Object> scriptObjects = provider.getGlobalScriptObjects(pack);
                if (scriptObjects != null)
                {
                    for (int i = 0; i < scriptObjects.size(); i += 2)
                    {
                        bindings.put(scriptObjects.get(i).toString(), scriptObjects.get(i + 1));
                    }
                }
            }


            if (result != null)
            {
                bindings.put("result", result);
            }
            if (data.getWorld() != null)
            {
                ScriptableWorld scriptableWorld = new ScriptableWorld(data.getWorld());
                scriptableWorld.setMod(pack);
                bindings.put("world", scriptableWorld);
            }
            if (data.getPosition() != null)
            {
                bindings.put("position", new ScriptablePosition(data.getPosition().getX(),
                                                                data.getPosition().getY(),
                                                                data.getPosition().getZ()));
            }
            if (data.getPlayer() != null)
            {
                ScriptableEntityPlayer player = new ScriptableEntityPlayer(data.getPlayer());
                //player.setMod(pack);
                bindings.put("player", player);
            }
            if (data.getInteractPlayer() != null)
            {
                ScriptableEntityPlayer player = new ScriptableEntityPlayer(data.getInteractPlayer());
                //player.setMod(mod);
                bindings.put("interactPlayer", player);
            }
            if (data.getLiving() != null)
            {
                bindings.put("living", new ScriptableEntityLiving(data.getLiving()));
            }
            if (data.getItem() != null)
            {
                bindings.put("item", new ScriptableEntityItem(data.getItem()));
            }
            if (data.getEntity() != null)
            {
                bindings.put("entity", new ScriptableEntity(data.getEntity()));
            }
            if (data.getItemStack() != ItemStack.EMPTY)
            {
                bindings.put("itemstack", new ScriptableItemStack(data.getItemStack()));
            }
            if (data.getHitX() != null)
            {
                bindings.put("hitX", data.getHitX());
                bindings.put("hitY", data.getHitY());
            }
            if (data.getSide() != null)
            {
                bindings.put("side", data.getSide());
            }
            if (data.getBlockName() != null)
            {
                bindings.put("blockName", data.getBlockName());
            }
            if (data.getSlotId() != null)
            {
                bindings.put("slotId", data.getSlotId());
            }
            if (data.getTickCount() != null)
            {
                bindings.put("tickCount", data.getTickCount());
            }
            if (data.getIsCurrentItem() != null)
            {
                bindings.put("isCurrentItem", data.getIsCurrentItem());
            }
            if (data.getRedstoneSignal() != null)
            {
                bindings.put("redstoneSignal", data.getRedstoneSignal());
            }
            if (data.getGuiX() != null)
            {
                bindings.put("guiX", data.getGuiX());
                bindings.put("guiY", data.getGuiY());
            }
            if (data.getMouseX() != null)
            {
                bindings.put("mouseX", data.getMouseX());
                bindings.put("mouseY", data.getMouseY());
            }
            if (data.getWidth() != null)
            {
                bindings.put("width", data.getWidth());
                bindings.put("height", data.getHeight());
            }
            if (data.getCraftMatrix() != null)
            {
                bindings.put("craftMatrix", new ScriptableInventory(data.getCraftMatrix()));
            }

            for (IScriptableObjectProvider provider : ScriptObjectManager.providers)
            {
                List<Object> scriptObjects = provider.getGlobalScriptObjects(pack);
                if (scriptObjects != null)
                {
                    for (int i = 0; i < scriptObjects.size(); i += 2)
                    {
                        bindings.put(scriptObjects.get(i).toString(), scriptObjects.get(i + 1));
                    }
                }
            }

            executingPack = pack;
            //script.exec(cx, scope);
            script.eval(bindings);

            if (result != null)
            {
                result = (T) bindings.get("result");
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
//            Context.exit();
            executingPack = null;
        }

        return result;
    }
}
