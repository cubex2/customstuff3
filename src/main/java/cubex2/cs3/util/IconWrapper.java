package cubex2.cs3.util;

import cubex2.cs3.common.BaseContentPack;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.ResourceLocation;

public class IconWrapper
{
    public String iconString;

    public IconWrapper(String iconString)
    {
        this.iconString = iconString;
    }

    public String getTextForGui(BaseContentPack pack)
    {
        String res = iconString;
        if (res.contains(":") && res.split(":")[0].equals(pack.id.toLowerCase()) && !res.endsWith(":"))
            res = res.split(":")[1];
        return res;
    }

    public String getIconForModel(String folder)
    {
        return iconString == null || iconString.length() == 0 ? TextureMap.LOCATION_MISSING_TEXTURE.toString() : iconString.replace(":", ":" + folder + "/");
    }

    public ResourceLocation getResourceLocationForFluid()
    {
        return new ResourceLocation(getIconForModel("blocks"));
    }

    @Override
    public String toString()
    {
        return "icon(" + iconString + ")";
    }
}
