package cubex2.cs3.util;

import com.google.common.collect.Lists;

import java.io.*;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipHelper
{
    public static String readEntryContent(File zipFile, String entryName) throws IOException
    {
        FileInputStream fin = new FileInputStream(zipFile);
        ZipInputStream zin = new ZipInputStream(fin);
        ZipEntry ze;
        String ret = null;
        while ((ze = zin.getNextEntry()) != null)
        {
            if (ze.getName().equals(entryName))
            {
                StringWriter sw = new StringWriter();
                for (int c = zin.read(); c != -1; c = zin.read())
                {
                    sw.write(c);
                }
                ret = sw.getBuffer().toString();
                zin.closeEntry();
                sw.close();
                break;
            }
            zin.closeEntry();
        }
        zin.close();
        fin.close();
        return ret == null ? "" : ret;
    }

    public static List<String> listEntries(File zipFile) throws IOException
    {
        FileInputStream fin = new FileInputStream(zipFile);
        ZipInputStream zin = new ZipInputStream(fin);
        ZipEntry ze;
        List<String> entries = Lists.newArrayList();
        while ((ze = zin.getNextEntry()) != null)
        {
            entries.add(ze.getName());
            zin.closeEntry();
        }
        zin.close();
        fin.close();
        return entries;
    }

    public static void jar(File source, File base, JarOutputStream target) throws IOException
    {
        BufferedInputStream in = null;
        try
        {
            if (source.isDirectory())
            {
                String name = source.getPath().replace("\\", "/");
                name = name.substring(base.getAbsolutePath().replace("\\", "/").length());
                if (!name.isEmpty())
                {
                    if (!name.endsWith("/"))
                        name += "/";
                    if (name.startsWith("/"))
                        name = name.substring(1);

                    JarEntry entry = new JarEntry(name);
                    entry.setTime(source.lastModified());
                    target.putNextEntry(entry);
                    target.closeEntry();
                }
                for (File nestedFile : source.listFiles())
                    jar(nestedFile, base, target);
                return;
            }

            JarEntry entry = new JarEntry(source.getPath().replace("\\", "/").substring(base.getAbsolutePath().replace("\\", "/").length() + 1));
            entry.setTime(source.lastModified());
            target.putNextEntry(entry);
            in = new BufferedInputStream(new FileInputStream(source));

            byte[] buffer = new byte[1024];
            while (true)
            {
                int count = in.read(buffer);
                if (count == -1)
                    break;
                target.write(buffer, 0, count);
            }
            target.closeEntry();
        } finally
        {
            if (in != null)
                in.close();
        }
    }
}
