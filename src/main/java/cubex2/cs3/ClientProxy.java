package cubex2.cs3;

import cubex2.cs3.block.BlockCSDoor;
import cubex2.cs3.block.BlockCSFenceGate;
import cubex2.cs3.block.BlockCSFluid;
import cubex2.cs3.block.EnumBlockType;
import cubex2.cs3.common.WrappedBlock;
import cubex2.cs3.common.WrappedItem;
import cubex2.cs3.entity.EntityCSGravityBlock;
import cubex2.cs3.handler.KeyBindingHandler;
import cubex2.cs3.lib.ModInfo;
import cubex2.cs3.renderer.RenderCSBlockGravity;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockModelShapes;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMap;
import net.minecraft.client.resources.IResourcePack;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy
{
    public static IResourcePack resPack;

    @Override
    public void registerKeyBindings()
    {
        ClientRegistry.registerKeyBinding(KeyBindingHandler.openGuiKey);
        MinecraftForge.EVENT_BUS.register(KeyBindingHandler.INSTANCE);
    }

    @Override
    public void registerEventHandlers()
    {
        super.registerEventHandlers();
    }

    @Override
    public void initRendering()
    {
        resPack = FMLClientHandler.instance().getResourcePackFor(ModInfo.ID);

        RenderingRegistry.registerEntityRenderingHandler(EntityCSGravityBlock.class, new RenderCSBlockGravity(FMLClientHandler.instance().getClient().getRenderManager()));
    }

    @Override
    public void registerModels(final WrappedBlock wrappedBlock, Block block)
    {
        Item item = Item.REGISTRY.getObject(new ResourceLocation(wrappedBlock.getPack().id, wrappedBlock.getName()));
        ModelBakery.registerItemVariants(item, new ResourceLocation(wrappedBlock.getPack().id + ":block_" + wrappedBlock.getName()));

        final ModelResourceLocation l = new ModelResourceLocation(wrappedBlock.getPack().id + ":block_" + wrappedBlock.getName(), "inventory");
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, l);

        BlockModelShapes shapes = FMLClientHandler.instance().getClient().getBlockRendererDispatcher().getBlockModelShapes();

        if (wrappedBlock.getType() == EnumBlockType.FENCE_GATE)
        {
            shapes.registerBlockWithStateMapper(block, new StateMap.Builder().ignore(BlockCSFenceGate.POWERED).build());
        } else if (wrappedBlock.getType() == EnumBlockType.DOOR)
        {
            shapes.registerBlockWithStateMapper(block, new StateMap.Builder().ignore(BlockCSDoor.POWERED).build());
        } else if (wrappedBlock.getType() == EnumBlockType.FLUID)
        {
            shapes.registerBlockWithStateMapper(block, new StateMap.Builder().ignore(BlockCSFluid.LEVEL).build());
        }
    }

    @Override
    public void registerModels(WrappedItem wrappedItem, Item item)
    {
        ModelBakery.registerItemVariants(item, new ResourceLocation(wrappedItem.getPack().id + ":" + wrappedItem.getName()));

        ModelResourceLocation l = new ModelResourceLocation(wrappedItem.getPack().id + ":" + wrappedItem.getName(), "inventory");
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, l);
    }
}
