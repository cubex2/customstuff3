package cubex2.cs3.api.scripting;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public interface ITriggerData
{
    String getTriggerName();

    String getBlockName();

    TriggerType getTriggerType();

    World getWorld();

    BlockPos getPosition();

    EntityPlayer getPlayer();

    EntityPlayer getInteractPlayer();

    EntityLivingBase getLiving();

    EntityItem getItem();

    Entity getEntity();

    ItemStack getItemStack();

    Float getHitX();

    Float getHitY();

    EnumFacing getSide();

    Integer getSlotId();

    Integer getTickCount();

    Boolean getIsCurrentItem();

    Boolean getRedstoneSignal();

    Integer getMouseX();

    Integer getMouseY();

    Integer getGuiX();

    Integer getGuiY();

    Integer getWidth();

    Integer getHeight();

    IInventory getCraftMatrix();
}
