package cubex2.cs3.asm.export;

import cubex2.cs3.common.BaseContentPack;
import org.objectweb.asm.commons.Remapper;

public class ModRemapper extends Remapper
{
    private static final String OLD_PACKAGE = "cubex2/cs3/asm/export/templates";

    private final String modId;
    private final String modName;
    private final String modVersion;
    private final String newPackage;

    private String typeOld;
    private String typeNew;

    public ModRemapper(BaseContentPack pack, String version)
    {
        modId = pack.id;
        modName = pack.name;
        modVersion = version;
        newPackage = "_customstuff/" + pack.id.toLowerCase();
    }

    public void setTypeReplacement(String old, String _new)
    {
        typeOld = old;
        typeNew = _new;
    }

    @Override
    public String mapDesc(String desc)
    {
        //if (desc.startsWith("cubex2"))
        //  System.out.println("mapDesc: " + desc);
        desc = desc.replace(OLD_PACKAGE, newPackage);
        return super.mapDesc(desc);
    }

    @Override
    public String map(String typeName)
    {
        // if (typeName.startsWith("cubex2"))
        //   System.out.println("map: " + typeName);

        typeName = typeName.replace(OLD_PACKAGE, newPackage);

        if (typeOld != null && typeName.contains(newPackage))
            typeName = typeName.replace(typeOld, typeNew);
        return super.map(typeName);
    }

    @Override
    public Object mapValue(Object value)
    {
        // System.out.println("mapValue: " + value);
        if (value != null && value instanceof String)
        {
            String s = (String) value;
            if (s.contains("%MOD_ID%"))
                return super.mapValue(s.replace("%MOD_ID%", modId));
            if (s.contains("%MOD_NAME%"))
                return super.mapValue(s.replace("%MOD_NAME%", modName));
            if (s.contains("%MOD_VERSION%"))
                return super.mapValue(s.replace("%MOD_VERSION%", modVersion));
        }

        return super.mapValue(value);
    }
}
