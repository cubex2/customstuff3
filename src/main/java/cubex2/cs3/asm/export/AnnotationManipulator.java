package cubex2.cs3.asm.export;

import java.util.List;

public class AnnotationManipulator
{

    /**
     * Edit value only.
     *
     * @param value [0] is desc, [1] is value
     */
    public void editEnum(final String name, String[] value)
    {

    }

    public void editArray(final String name, List<?> values)
    {

    }

    /**
     * This is not called for enums, arrays or annotations
     */
    public Object editValue(final String name, Object value)
    {
        return value;
    }

    public static AnnotationManipulator IDENTITY = new AnnotationManipulator();
}
