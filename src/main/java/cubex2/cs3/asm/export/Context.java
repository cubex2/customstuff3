package cubex2.cs3.asm.export;


import com.google.common.collect.Lists;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.InsnList;

import java.util.List;

public class Context
{
    public final InsnList preInit = new InsnList();
    public final InsnList init = new InsnList();
    public final InsnList postInit = new InsnList();
    public final List<FieldNode> fields = Lists.newLinkedList();

    public void addField(int access, String name, Class<?> clazz)
    {
        fields.add(new FieldNode(access, name, Type.getDescriptor(clazz), null, null));
    }

    public void addField(int access, String name, String type)
    {
        if (!type.startsWith("L"))
            type = "L" + type + ";";
        fields.add(new FieldNode(access, name, type, null, null));
    }
}
