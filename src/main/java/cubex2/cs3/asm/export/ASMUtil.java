package cubex2.cs3.asm.export;

import cubex2.cs3.asm.export.templates.Util;
import cubex2.cs3.util.GeneralHelper;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.Remapper;
import org.objectweb.asm.commons.RemappingClassAdapter;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.util.Printer;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class ASMUtil implements Opcodes
{
    public static ClassWriter copyClass(Class<?> clazz, Remapper remapper, ClassNode visitor)
    {
        if (!clazz.getName().startsWith("cubex2.cs3.asm"))
            throw new IllegalArgumentException("Wrong class location");

        try
        {
            InputStream is = clazz.getResourceAsStream("/" + clazz.getName().replace('.', '/')
                                                               + ".class");
            ClassReader cr = new ClassReader(is);
            is.close();

            cr.accept(visitor, ClassReader.SKIP_FRAMES);

            ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
            visitor.accept(new RemappingClassAdapter(cw, remapper));

            return cw;
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static InsnList getInstructions(Class<?> clazz, String method, String desc)
    {
        try
        {
            ClassNode classNode = new ClassNode();
            ClassReader reader = new ClassReader(clazz.getName());
            reader.accept(classNode, 0);

            MethodNode m = findMethod(classNode, method, desc);
            return m.instructions;
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return new InsnList();
    }

    public static MethodNode findMethod(ClassNode node, String name, String desc)
    {
        for (MethodNode m : node.methods)
        {
            if (desc != null && !desc.equals(m.desc))
                continue;

            if (m.name.equals(name))
                return m;
        }

        return null;
    }

    public static void writeClass(ClassWriter cw, File file)
    {
        if (!file.getParentFile().exists())
        {
            file.getParentFile().mkdirs();
        }

        try
        {
            FileOutputStream fs = new FileOutputStream(file);
            fs.write(cw.toByteArray());
            fs.close();
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static InsnList createPrintln(String s)
    {
        InsnList list = new InsnList();
        list.add(new FieldInsnNode(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"));
        list.add(new LdcInsnNode(s));
        list.add(new MethodInsnNode(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false));
        return list;
    }

    public static String insnToString(AbstractInsnNode insn)
    {
        insn.accept(mp);
        StringWriter sw = new StringWriter();
        printer.print(new PrintWriter(sw));
        printer.getText().clear();
        return sw.toString();
    }

    public static void pushStack(InsnList list, ItemStack stack)
    {
        list.add(new LdcInsnNode(GeneralHelper.getItemName(stack.getItem())));
        pushInt(list, stack.getItemDamage());
        pushInt(list, stack.getCount());
        invokeStatic(list, Util.class, "getStack", String.class, int.class, int.class);
    }

    public static void pushBlock(InsnList list, Block block)
    {
        pushConst(list, GeneralHelper.getBlockName(block));
        invokeStatic(list, Util.class, "getBlock", String.class);
    }

    public static void pushConst(InsnList list, Object o)
    {
        list.add(new LdcInsnNode(o));
    }

    public static void pushInt(InsnList list, int value)
    {
        if (value == 0)
            list.add(new InsnNode(ICONST_0));
        else if (value == 1)
            list.add(new InsnNode(ICONST_1));
        else if (value == 2)
            list.add(new InsnNode(ICONST_2));
        else if (value == 3)
            list.add(new InsnNode(ICONST_3));
        else if (value == 4)
            list.add(new InsnNode(ICONST_4));
        else if (value == 5)
            list.add(new InsnNode(ICONST_5));
        else if (value <= Byte.MAX_VALUE)
            list.add(new IntInsnNode(BIPUSH, (byte) value));
        else if (value <= Short.MAX_VALUE)
            list.add(new IntInsnNode(SIPUSH, (short) value));
        else
            list.add(new LdcInsnNode(value));
    }

    public static void pushInteger(InsnList list, int value)
    {
        pushInt(list, value);
        invokeStatic(list, Integer.class, "valueOf", int.class);
    }

    public static void pushFloat(InsnList list, float value)
    {
        if (value == 0.0f)
            list.add(new InsnNode(FCONST_0));
        else if (value == 1.0f)
            list.add(new InsnNode(FCONST_1));
        else if (value == 2.0f)
            list.add(new InsnNode(FCONST_2));
        else
            list.add(new LdcInsnNode(value));
    }

    public static void pushBoolean(InsnList list, boolean value)
    {
        if (value)
            list.add(new InsnNode(ICONST_1));
        else
            list.add(new InsnNode(ICONST_0));
    }

    public static void pushCharacter(InsnList list, char value)
    {
        pushInt(list, value);
        invokeStatic(list, Character.class, "valueOf", char.class);
    }

    public static void pushObjectArray(InsnList list, Object[] array)
    {
        pushInt(list, array.length);
        list.add(new TypeInsnNode(ANEWARRAY, "java/lang/Object"));
        list.add(new InsnNode(DUP));

        for (int i = 0; i < array.length; i++)
        {
            pushInt(list, i);

            push(list, array[i]);
            list.add(new InsnNode(AASTORE));

            if (i < array.length - 1)
                list.add(new InsnNode(DUP));
        }
    }

    public static void pushIntArray(InsnList list, int[] array)
    {
        pushInt(list, array.length);
        list.add(new IntInsnNode(NEWARRAY, T_INT));
        list.add(new InsnNode(DUP));

        for (int i = 0; i < array.length; i++)
        {
            pushInt(list, i);

            pushInt(list, array[i]);
            list.add(new InsnNode(IASTORE));

            if (i < array.length - 1)
                list.add(new InsnNode(DUP));
        }
    }

    public static void pushStringArray(InsnList list, String[] array)
    {
        pushInt(list, array.length);
        list.add(new TypeInsnNode(ANEWARRAY, "java/lang/String"));
        list.add(new InsnNode(DUP));

        for (int i = 0; i < array.length; i++)
        {
            pushInt(list, i);

            pushConst(list, array[i]);
            list.add(new InsnNode(AASTORE));

            if (i < array.length - 1)
                list.add(new InsnNode(DUP));
        }
    }

    public static void push(InsnList list, Object o)
    {
        if (o instanceof ItemStack)
            pushStack(list, (ItemStack) o);
        else if (o instanceof Block)
            pushBlock(list, (Block) o);
        else if (o instanceof Integer)
            pushInteger(list, (Integer) o);
        else if (o instanceof Character)
            pushCharacter(list, (Character) o);
        else
            pushConst(list, o);
    }

    public static void pushNewInstance(InsnList list, Class<?> clazz)
    {
        list.add(new TypeInsnNode(NEW, Type.getInternalName(clazz)));
    }

    public static void pushNewInstance(InsnList list, String type)
    {
        list.add(new TypeInsnNode(NEW, type));
    }

    public static void putStatic(InsnList list, Class<?> owner, String name, Class<?> type)
    {
        list.add(new FieldInsnNode(PUTSTATIC, Type.getInternalName(owner), name, Type.getDescriptor(type)));
    }

    public static void putStatic(InsnList list, Class<?> owner, String name, String typeDesc)
    {
        list.add(new FieldInsnNode(PUTSTATIC, Type.getInternalName(owner), name, typeDesc));
    }

    public static void putStatic(InsnList list, String owner, String name, Class<?> type)
    {
        list.add(new FieldInsnNode(PUTSTATIC, owner, name, Type.getDescriptor(type)));
    }

    public static void putStatic(InsnList list, String owner, String name, String typeDesc)
    {
        list.add(new FieldInsnNode(PUTSTATIC, owner, name, typeDesc));
    }

    public static void putField(InsnList list, Class<?> owner, String name, Class<?> type)
    {
        list.add(new FieldInsnNode(PUTFIELD, Type.getInternalName(owner), name, Type.getDescriptor(type)));
    }

    public static void putField(InsnList list, String owner, String name, Class<?> type)
    {
        list.add(new FieldInsnNode(PUTFIELD, owner, name, Type.getDescriptor(type)));
    }

    public static void invokeStatic(InsnList list, Class<?> owner, String name, Class<?>... params)
    {
        list.add(new MethodInsnNode(INVOKESTATIC, Type.getInternalName(owner), name, Type.getMethodDescriptor(getMethod(owner, name, params)), false));
    }

    public static void invokeVirtual(InsnList list, Class<?> owner, String name, Class<?>... params)
    {
        list.add(new MethodInsnNode(INVOKEVIRTUAL, Type.getInternalName(owner), name, Type.getMethodDescriptor(getMethod(owner, name, params)), false));
    }

    public static void invokeSpecial(InsnList list, Class<?> owner, Class<?>... params)
    {
        list.add(new MethodInsnNode(INVOKESPECIAL, Type.getInternalName(owner), "<init>", Type.getConstructorDescriptor(getConstructor(owner, params)), false));
    }

    public static void invokeSpecial(InsnList list, String owner, Class<?> template, Class<?>... params)
    {
        list.add(new MethodInsnNode(INVOKESPECIAL, owner, "<init>", Type.getConstructorDescriptor(getConstructor(template, params)), false));
    }

    public static Method getMethod(Class<?> owner, String name, Class<?>... params)
    {
        try
        {
            return owner.getMethod(name, params);
        } catch (NoSuchMethodException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static Constructor<?> getConstructor(Class<?> owner, Class<?>... params)
    {
        try
        {
            return owner.getConstructor(params);
        } catch (NoSuchMethodException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    private static Printer printer = new Textifier();
    private static TraceMethodVisitor mp = new TraceMethodVisitor(printer);
}
