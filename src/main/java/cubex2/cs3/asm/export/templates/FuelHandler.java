package cubex2.cs3.asm.export.templates;

import com.google.common.collect.Lists;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.IFuelHandler;

import java.util.List;

public class FuelHandler implements IFuelHandler
{
    private static List<Fuel> fuels = Lists.newLinkedList();

    public static void addFuel(ItemStack stack, int duration)
    {
        fuels.add(new Fuel(stack, duration));
    }

    @Override
    public int getBurnTime(ItemStack stack)
    {
        for (Fuel fuel : fuels)
        {
            if (fuel.isRepresentingStack(stack))
                return fuel.duration;
        }
        return 0;
    }
}
