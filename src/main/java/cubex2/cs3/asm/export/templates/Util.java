package cubex2.cs3.asm.export.templates;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.DungeonHooks;
import net.minecraftforge.fml.common.registry.GameData;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

import java.util.ArrayList;
import java.util.List;

public class Util
{
    public static ItemStack getStack(String item, int damage, int stacksize)
    {
        return new ItemStack(GameData.getItemRegistry().getObject(new ResourceLocation(item)), stacksize, damage);
    }

    public static Block getBlock(String value)
    {
        return GameData.getBlockRegistry().getObject(new ResourceLocation(value));
    }

    public static Item getItem(String value)
    {
        return GameData.getItemRegistry().getObject(new ResourceLocation(value));
    }

    public static void addFlowerEntry(ItemStack stack, int weight)
    {
        Block block = Block.getBlockFromItem(stack.getItem());
        int meta = stack.getItemDamage();

        Biome.FlowerEntry entry = new Biome.FlowerEntry(block.getStateFromMeta(meta), weight);

        for (Biome bgb : Biome.REGISTRY)
        {
            if (bgb != null)
            {
                List<Biome.FlowerEntry> entries = ReflectionHelper.getPrivateValue(Biome.class, bgb, "flowers");
                entries.add(entry);
            }
        }
    }

    public static void addDungeonMob(String mob, int rarity)
    {
        ArrayList<DungeonHooks.DungeonMob> dungeonMobs = ReflectionHelper.getPrivateValue(DungeonHooks.class, null, "dungeonMobs");
        dungeonMobs.add(new DungeonHooks.DungeonMob(rarity, new ResourceLocation(mob)));
    }

    public static void addMobSpawn(String mob, int rate, int min, int max, int type, String[] biomes)
    {
        Class<? extends Entity> entityClazz = EntityList.getClass(new ResourceLocation(mob));

        if (EntityLiving.class.isAssignableFrom(entityClazz))
        {
            Class<? extends EntityLiving> clazz = (Class<? extends EntityLiving>) entityClazz;

            for (String biomeName : biomes)
            {
                Biome biome = getBiome(biomeName);
                List<Biome.SpawnListEntry> spawns = biome.getSpawnableList(EnumCreatureType.values()[type]);

                Biome.SpawnListEntry entry = new Biome.SpawnListEntry(clazz, rate, min, max);
                spawns.add(entry);
            }
        }
    }

    public static Biome getBiome(String name)
    {
        return Biome.REGISTRY.getObject(new ResourceLocation(name));
    }
}
