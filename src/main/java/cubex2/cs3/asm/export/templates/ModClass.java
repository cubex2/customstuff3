package cubex2.cs3.asm.export.templates;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModClass
{
    @Mod.Instance("%MOD_ID%")
    public static ModClass instance;

    @Mod.EventHandler
    public void preInitFixed(FMLPreInitializationEvent event)
    {
        GameRegistry.registerFuelHandler(new FuelHandler());
        MinecraftForge.EVENT_BUS.register(new MobDropHandler());
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {

    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
        //GameRegistry.addRecipe(new ShapelessOreRecipe(Util.getStack("minecraft:bla", 7, 9), "sssss", Util.getStack("minecraft:bla", 7, 9)));
    }
}
