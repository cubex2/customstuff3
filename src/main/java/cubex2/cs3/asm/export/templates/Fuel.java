package cubex2.cs3.asm.export.templates;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class Fuel
{
    public ItemStack stack;
    public int duration;

    public Fuel(ItemStack stack, int duration)
    {
        this.stack = stack;
        this.duration = duration;
    }

    public boolean isRepresentingStack(ItemStack stack)
    {
        return this.stack.getItem() == stack.getItem() &&
                (this.stack.getItemDamage() == stack.getItemDamage() || this.stack.getItemDamage() == OreDictionary.WILDCARD_VALUE);
    }
}
