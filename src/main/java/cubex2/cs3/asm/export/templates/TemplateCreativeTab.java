package cubex2.cs3.asm.export.templates;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class TemplateCreativeTab extends CreativeTabs
{
    private ItemStack stack = ItemStack.EMPTY;

    public TemplateCreativeTab(String label)
    {
        super(label);
    }

    private void init()
    {

    }

    @Override
    public ItemStack getTabIconItem()
    {
        if (stack.isEmpty())
            init();

        return stack;
    }
}
