package cubex2.cs3.asm.export.templates;

import com.google.common.collect.Lists;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.List;

public class MobDropHandler
{
    private static List<MobDrop> drops = Lists.newLinkedList();

    public static void addDrop(String mob, ItemStack stack, float chance, boolean playerKillOnly)
    {
        drops.add(new MobDrop(mob, stack, chance, playerKillOnly));
    }

    @SubscribeEvent
    public void onLivingDrops(LivingDropsEvent event)
    {
        for (MobDrop drop : drops)
        {
            Entity source = event.getSource().getEntity();
            Class<? extends Entity> eClass = drop.getEntityClass();
            if (eClass != null && eClass == event.getEntity().getClass() &&
                    (!drop.playerKillOnly || (source != null && source instanceof EntityPlayer)))
            {
                if (event.getEntity().world.rand.nextFloat() <= drop.chance)
                {
                    event.getEntity().entityDropItem(drop.stack.copy(), 0.0f);
                }
            }
        }
    }
}
