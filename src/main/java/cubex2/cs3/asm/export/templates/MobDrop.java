package cubex2.cs3.asm.export.templates;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class MobDrop
{
    public final String mob;
    public final ItemStack stack;
    public final float chance;
    public final boolean playerKillOnly;

    private Class<? extends Entity> clazz;

    public MobDrop(String mob, ItemStack stack, float chance, boolean playerKillOnly)
    {
        this.mob = mob;
        this.stack = stack;
        this.chance = chance;
        this.playerKillOnly = playerKillOnly;
    }

    public Class<? extends Entity> getEntityClass()
    {
        if (clazz == null)
            clazz = EntityList.getClass(new ResourceLocation(mob));
        return clazz;
    }
}
