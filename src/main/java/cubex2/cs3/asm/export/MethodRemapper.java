package cubex2.cs3.asm.export;


import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.commons.Remapper;
import org.objectweb.asm.commons.RemappingMethodAdapter;

public class MethodRemapper extends RemappingMethodAdapter
{

    public MethodRemapper(int access, String desc, MethodVisitor mv, Remapper remapper)
    {
        super(access, desc, mv, remapper);
    }

    protected MethodRemapper(int api, int access, String desc, MethodVisitor mv, Remapper remapper)
    {
        super(api, access, desc, mv, remapper);
    }
}
