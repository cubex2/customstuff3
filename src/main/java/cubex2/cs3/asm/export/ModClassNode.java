package cubex2.cs3.asm.export;

import cubex2.cs3.asm.export.templates.ModClass;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.registry.ContentRegistry;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodNode;

public class ModClassNode extends ClassNode implements Opcodes
{
    private final BaseContentPack pack;
    private final Class<?> clazz;
    private final String modVersion;

    public ModClassNode(Class<?> clazz, BaseContentPack pack, String version)
    {
        super(Opcodes.ASM5);
        this.clazz = clazz;
        this.pack = pack;
        modVersion = version;
    }

    @Override
    public void accept(ClassVisitor cv)
    {
        if (clazz == ModClass.class)
        {
            AnnotationVisitor av = cv.visitAnnotation("Lnet/minecraftforge/fml/common/Mod;", true);
            av.visit("modid", pack.id);
            av.visit("name", pack.name);
            av.visit("version", modVersion);
            av.visitEnd();

            Context context = new Context();

            for (ContentRegistry registry : pack.getRegistries())
            {
                registry.export(context);
            }

            for (FieldNode field : context.fields)
            {
                visitField(field.access, field.name, field.desc, field.signature, field.value);
            }

            MethodNode m;

            m = ASMUtil.findMethod(this, "preInit", null);
            m.instructions.clear();
            m.instructions.add(context.preInit);
            m.instructions.add(new InsnNode(RETURN));

            m = ASMUtil.findMethod(this, "init", null);
            m.instructions.clear();
            m.instructions.add(context.init);
            m.instructions.add(new InsnNode(RETURN));

            m = ASMUtil.findMethod(this, "postInit", null);
            m.instructions.clear();
            m.instructions.add(context.postInit);
            m.instructions.add(new InsnNode(RETURN));
        }

        super.accept(cv);
    }
}
