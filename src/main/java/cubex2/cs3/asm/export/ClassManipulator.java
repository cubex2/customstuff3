package cubex2.cs3.asm.export;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

public interface  ClassManipulator
{
    void manipulate(ClassWriter cw, ClassNode cn);
}
