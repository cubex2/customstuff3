package cubex2.cs3.asm.export;

import com.google.common.collect.Maps;
import cubex2.cs3.asm.export.templates.*;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.lib.Directories;
import cubex2.cs3.registry.ContentRegistry;
import cubex2.cs3.util.ZipHelper;
import org.apache.commons.io.FileUtils;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

public class Exporter
{
    private static Map<String, String> localisation = Maps.newHashMap();

    public static void addLocalisation(String key, String value)
    {
        localisation.put(key, value);
    }

    public static void export(BaseContentPack pack, final String version) throws IOException
    {
        localisation.clear();

        final String newPackage = "_customstuff/" + pack.id.toLowerCase();
        final String path = String.format("%s_Export/%s/", pack.name, newPackage);
        final File modDir = new File(Directories.MODS, String.format("%s_Export", pack.name));
        final ModRemapper remapper = new ModRemapper(pack, version);

        for (ContentRegistry registry : pack.getRegistries())
        {
            registry.createClasses(remapper, version, path);
        }
        remapper.setTypeReplacement(null, null);

        copyClass(Util.class, remapper, path + "Util");
        copyClass(FuelHandler.class, remapper, path + "FuelHandler");
        copyClass(Fuel.class, remapper, path + "Fuel");
        copyClass(MobDropHandler.class, remapper, path + "MobDropHandler");
        copyClass(MobDrop.class, remapper, path + "MobDrop");

        ClassWriter cw = ASMUtil.copyClass(ModClass.class, remapper, new ModClassNode(ModClass.class, pack, version));
        ASMUtil.writeClass(cw, new File(Directories.MODS, path + "ModClass.class"));

        File en_US = new File(modDir, "assets/" + pack.id.toLowerCase() + "/lang/en_US.lang");
        en_US.getParentFile().mkdirs();
        FileWriter fw = new FileWriter(en_US);
        try
        {
            for (Map.Entry<String, String> entry : localisation.entrySet())
            {
                fw.append(entry.getKey()).append("=").append(entry.getValue()).append(System.lineSeparator());
            }
        } finally
        {
            fw.close();
        }

        File dir = new File(modDir.getParentFile().getParentFile(), "CustomStuff_Exports");
        dir.mkdirs();

        try
        {
            Manifest manifest = new Manifest();
            manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
            JarOutputStream target = new JarOutputStream(new FileOutputStream(new File(dir, pack.id + "_" + version + ".jar")), manifest);
            ZipHelper.jar(modDir, modDir, target);
            target.close();
        } finally
        {
            FileUtils.deleteDirectory(modDir);
        }
    }

    private static void copyClass(Class<?> clazz, ModRemapper remapper, String path)
    {
        ClassWriter cw = ASMUtil.copyClass(clazz, remapper, new ClassNode());
        ASMUtil.writeClass(cw, new File(Directories.MODS, path + ".class"));
    }
}
