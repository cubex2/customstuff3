package cubex2.cs3.registry;

import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.ShapelessRecipe;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowShapelessRecipes;
import cubex2.cs3.lib.Strings;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;

public class ShapelessRecipeRegistry extends ContentRegistry<ShapelessRecipe>
{
    public ShapelessRecipeRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public ShapelessRecipe newDataInstance()
    {
        return new ShapelessRecipe(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowShapelessRecipes(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Shapeless Recipes";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_SHAPELESS_RECIPE;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (ShapelessRecipe recipe : getContentList())
        {
            ASMUtil.pushNewInstance(list, ShapelessOreRecipe.class);
            list.add(new InsnNode(DUP));
            ASMUtil.pushStack(list, recipe.result);
            ASMUtil.pushObjectArray(list, recipe.createRecipeObjects());
            ASMUtil.invokeSpecial(list, ShapelessOreRecipe.class, ItemStack.class, Object[].class);
            ASMUtil.invokeStatic(list, GameRegistry.class, "addRecipe", IRecipe.class);
        }
    }
}
