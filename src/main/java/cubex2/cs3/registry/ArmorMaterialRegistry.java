package cubex2.cs3.registry;

import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.asm.export.templates.ModClass;
import cubex2.cs3.common.ArmorMaterial;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowArmorMaterials;
import cubex2.cs3.lib.Strings;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.common.util.EnumHelper;
import org.objectweb.asm.tree.InsnList;

public class ArmorMaterialRegistry extends ContentRegistry<ArmorMaterial>
{
    public ArmorMaterialRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public ArmorMaterial newDataInstance()
    {
        return new ArmorMaterial(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowArmorMaterials(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Armor Materials";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_ARMOR_MATERIAL;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (ArmorMaterial material : getContentList())
        {
            context.addField(ACC_PUBLIC | ACC_STATIC, "mat_" + material.name, ItemArmor.ArmorMaterial.class);

            ASMUtil.pushConst(list, material.name);
            ASMUtil.pushConst(list, material.name);
            ASMUtil.pushInt(list, material.durability);
            ASMUtil.pushIntArray(list, material.reductionAmounts);
            ASMUtil.pushInt(list, material.enchantability);
            ASMUtil.invokeStatic(list, EnumHelper.class, "addArmorMaterial", String.class, String.class, int.class, int[].class, int.class);
            ASMUtil.putStatic(list, ModClass.class, "mat_" + material.name, ItemArmor.ArmorMaterial.class);
        }
    }
}
