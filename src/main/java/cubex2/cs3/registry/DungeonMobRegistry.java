package cubex2.cs3.registry;

import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.asm.export.templates.Util;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.DungeonMob;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowDungeonMobs;
import cubex2.cs3.lib.Strings;
import org.objectweb.asm.tree.InsnList;

public class DungeonMobRegistry extends ContentRegistry<DungeonMob>
{
    public DungeonMobRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public DungeonMob newDataInstance()
    {
        return new DungeonMob(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowDungeonMobs(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Dungeon Mobs";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_DUNGEON_MOB;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (DungeonMob mob : getContentList())
        {
            ASMUtil.pushConst(list, mob.mob);
            ASMUtil.pushInt(list, mob.rarity);
            ASMUtil.invokeStatic(list, Util.class, "addDungeonMob", String.class, int.class);
        }
    }
}
