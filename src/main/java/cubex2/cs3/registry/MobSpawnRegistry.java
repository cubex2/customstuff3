package cubex2.cs3.registry;

import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.asm.export.templates.Util;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.MobSpawn;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowMobSpawns;
import cubex2.cs3.lib.Strings;
import org.objectweb.asm.tree.InsnList;

public class MobSpawnRegistry extends ContentRegistry<MobSpawn>
{
    public MobSpawnRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public MobSpawn newDataInstance()
    {
        return new MobSpawn(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowMobSpawns(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Mob Spawns";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_MOB_SPAWN;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (MobSpawn spawn : getContentList())
        {
            String[] biomes = spawn.biomes.stream().map(b -> b.getBiomeName()).toArray(String[]::new);

            ASMUtil.pushConst(list, spawn.mob);
            ASMUtil.pushInt(list, spawn.rate);
            ASMUtil.pushInt(list, spawn.min);
            ASMUtil.pushInt(list, spawn.max);
            ASMUtil.pushInt(list, spawn.type.ordinal());
            ASMUtil.pushStringArray(list, biomes);
            ASMUtil.invokeStatic(list, Util.class, "addMobSpawn", String.class, int.class, int.class, int.class, int.class, String[].class);
        }
    }
}
