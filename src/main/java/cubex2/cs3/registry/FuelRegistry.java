package cubex2.cs3.registry;

import com.google.common.collect.Sets;
import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.asm.export.templates.FuelHandler;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.Fuel;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowFuels;
import cubex2.cs3.lib.Strings;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.IFuelHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.objectweb.asm.tree.InsnList;

import java.util.Arrays;
import java.util.Set;

public class FuelRegistry extends ContentRegistry<Fuel> implements IFuelHandler
{
    public FuelRegistry(BaseContentPack pack)
    {
        super(pack);
        GameRegistry.registerFuelHandler(this);
    }

    @Override
    public Fuel newDataInstance()
    {
        return new Fuel(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowFuels(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Fuels";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_FUEL;
    }

    @Override
    public int getBurnTime(ItemStack stack)
    {
        for (Fuel fuel : getContentList())
        {
            if (fuel.isRepresentingStack(stack, "vanilla"))
                return fuel.duration;
        }
        return 0;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (Fuel fuel : getContentList())
        {
            if (!fuel.fuelList.equals("vanilla")) continue;

            ASMUtil.pushStack(list, fuel.stack);
            ASMUtil.pushInt(list, fuel.duration);
            ASMUtil.invokeStatic(list, FuelHandler.class, "addFuel", ItemStack.class, int.class);
        }
    }

    public String[] getFuelLists()
    {
        Set<String> lists = Sets.newHashSet();
        for (Fuel fuel : getContentList())
        {
            lists.add(fuel.fuelList);
        }
        lists.add("vanilla");

        String[] ret = lists.toArray(new String[lists.size()]);
        Arrays.sort(ret);
        return ret;
    }
}
