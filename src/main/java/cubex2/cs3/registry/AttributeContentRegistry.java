package cubex2.cs3.registry;

import com.google.common.collect.Lists;
import cubex2.cs3.common.AttributeContent;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.util.Util;
import net.minecraft.nbt.NBTTagCompound;

import java.util.List;

public abstract class AttributeContentRegistry<T extends AttributeContent> extends ContentRegistry<T>
{
    private List<String> favourites = Lists.newLinkedList();

    public AttributeContentRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    public boolean isFavouriteAttribute(String attribute)
    {
        return favourites.contains(attribute);
    }

    /**
     * Switches the favourite state of the attribute.
     *
     * @return TRUE if the attribute is now a favourite, FALSE otherwise.
     */
    public boolean switchFavourite(String attribute)
    {
        if (favourites.contains(attribute))
        {
            favourites.remove(attribute);
            return false;
        } else
        {
            favourites.add(attribute);
            return true;
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);

        Util.writeListToNBT("Favourites", favourites, Util.NBT_STRING_WRITER, compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);

        favourites.clear();
        Util.readListFromNBT("Favourites", favourites, Util.NBT_STRING_READER, compound);
    }
}
