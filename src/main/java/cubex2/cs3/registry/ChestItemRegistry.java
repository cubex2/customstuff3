package cubex2.cs3.registry;

import cubex2.cs3.asm.export.Context;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.ChestItem;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowChestItems;
import cubex2.cs3.lib.Strings;
import org.objectweb.asm.tree.InsnList;

public class ChestItemRegistry extends ContentRegistry<ChestItem>
{
    public ChestItemRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public ChestItem newDataInstance()
    {
        return new ChestItem(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowChestItems(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Chest Items";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_CHEST_ITEM;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        /* TODO 1.9 for (ChestItem chest : getContentList())
        {
            ASMUtil.pushConst(list, chest.chest);
            ASMUtil.invokeStatic(list, ChestGenHooks.class, "getInfo", String.class);
            ASMUtil.pushNewInstance(list, WeightedRandomChestContent.class);
            list.add(new InsnNode(DUP));
            ASMUtil.pushStack(list, chest.stack);
            ASMUtil.pushInt(list, chest.minCount);
            ASMUtil.pushInt(list, chest.maxCount);
            ASMUtil.pushInt(list, chest.rarity);
            ASMUtil.invokeSpecial(list, WeightedRandomChestContent.class, ItemStack.class, int.class, int.class, int.class);
            ASMUtil.invokeVirtual(list, ChestGenHooks.class, "addItem", WeightedRandomChestContent.class);
        }*/
    }
}
