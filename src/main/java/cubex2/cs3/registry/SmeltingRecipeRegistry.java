package cubex2.cs3.registry;

import com.google.common.collect.Sets;
import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.SmeltingRecipe;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowSmeltingRecipes;
import cubex2.cs3.lib.Strings;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.objectweb.asm.tree.InsnList;

import java.util.Arrays;
import java.util.Set;

public class SmeltingRecipeRegistry extends ContentRegistry<SmeltingRecipe>
{
    public SmeltingRecipeRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public SmeltingRecipe newDataInstance()
    {
        return new SmeltingRecipe(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowSmeltingRecipes(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Smelting Recipes";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_SMELTING_RECIPE;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (SmeltingRecipe recipe : getContentList())
        {
            if (!recipe.recipeList.equals("vanilla")) continue;

            ASMUtil.pushStack(list, recipe.input);
            ASMUtil.pushStack(list, recipe.result);
            ASMUtil.pushFloat(list, 0f);
            ASMUtil.invokeStatic(list, GameRegistry.class, "addSmelting", ItemStack.class, ItemStack.class, float.class);
        }
    }

    public String[] getRecipeLists()
    {
        Set<String> lists = Sets.newHashSet();
        for (SmeltingRecipe recipe : getContentList())
        {
            lists.add(recipe.recipeList);
        }
        lists.add("vanilla");

        String[] ret = lists.toArray(new String[lists.size()]);
        Arrays.sort(ret);
        return ret;
    }
}
