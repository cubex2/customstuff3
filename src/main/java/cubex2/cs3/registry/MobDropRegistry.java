package cubex2.cs3.registry;

import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.asm.export.templates.MobDropHandler;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.MobDrop;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowMobDrops;
import cubex2.cs3.lib.Strings;
import net.minecraft.item.ItemStack;
import org.objectweb.asm.tree.InsnList;

public class MobDropRegistry extends ContentRegistry<MobDrop>
{
    public MobDropRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public MobDrop newDataInstance()
    {
        return new MobDrop(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowMobDrops(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Mob Drops";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_MOB_DROP;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (MobDrop drop : getContentList())
        {
            ASMUtil.pushConst(list, drop.mob);
            ASMUtil.pushStack(list, drop.stack);
            ASMUtil.pushFloat(list, drop.chance);
            ASMUtil.pushBoolean(list, drop.playerKillOnly);
            ASMUtil.invokeStatic(list, MobDropHandler.class, "addDrop", String.class, ItemStack.class, float.class, boolean.class);
        }
    }
}
