package cubex2.cs3.registry;

import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.OreDictionaryEntry;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowOreDictionaryEntries;
import cubex2.cs3.lib.Strings;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodInsnNode;

public class OreDictEntryRegistry extends ContentRegistry<OreDictionaryEntry>
{
    public OreDictEntryRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public OreDictionaryEntry newDataInstance()
    {
        return new OreDictionaryEntry(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowOreDictionaryEntries(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Ore Dictionary";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_ORE_DICT_ENTRY;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (OreDictionaryEntry entry : getContentList())
        {
            ASMUtil.pushConst(list, entry.oreClass);
            ASMUtil.pushStack(list, entry.stack);
            ASMUtil.invokeStatic(list, OreDictionary.class, "registerOre", String.class, ItemStack.class);
        }
    }
}
