package cubex2.cs3.registry;

import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.asm.export.Exporter;
import cubex2.cs3.asm.export.ModRemapper;
import cubex2.cs3.asm.export.templates.ModClass;
import cubex2.cs3.asm.export.templates.TemplateCreativeTab;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.CreativeTab;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowCreativeTabs;
import cubex2.cs3.lib.Directories;
import cubex2.cs3.lib.Strings;
import net.minecraft.item.ItemStack;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.*;

import java.io.File;

public class CreativeTabRegistry extends ContentRegistry<CreativeTab>
{
    public CreativeTabRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public CreativeTab newDataInstance()
    {
        return new CreativeTab(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowCreativeTabs(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Creative Tabs";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_CREATIVE_TAB;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (CreativeTab tab : getContentList())
        {
            String className = "cubex2/cs3/asm/export/templates/CreativeTab_" + tab.name;
            context.addField(ACC_PUBLIC | ACC_STATIC, "tab_" + tab.name, className);

            ASMUtil.pushNewInstance(list, className);
            list.add(new InsnNode(DUP));
            ASMUtil.pushConst(list, tab.label);
            ASMUtil.invokeSpecial(list, className, TemplateCreativeTab.class, String.class);
            ASMUtil.putStatic(list, ModClass.class, "tab_" + tab.name, "L" + className + ";");
        }
    }

    @Override
    public void createClasses(ModRemapper remapper, String version, String path)
    {
        for (CreativeTab tab : getContentList())
        {
            Exporter.addLocalisation("itemGroup." + tab.name, tab.label);

            remapper.setTypeReplacement("TemplateCreativeTab", "CreativeTab_" + tab.name);
            ClassWriter cw = ASMUtil.copyClass(TemplateCreativeTab.class, remapper, new ClassNode(ASM5)
            {
                @Override
                public void accept(ClassVisitor cv)
                {
                    MethodNode m = ASMUtil.findMethod(this, "init", "()V");
                    m.instructions.clear();
                    m.instructions.add(new VarInsnNode(ALOAD, 0));
                    ASMUtil.pushStack(m.instructions, tab.icon);
                    ASMUtil.putField(m.instructions, "cubex2/cs3/asm/export/templates/CreativeTab_" + tab.name, "stack", ItemStack.class);
                    m.instructions.add(new InsnNode(RETURN));

                    super.accept(cv);
                }
            });

            ASMUtil.writeClass(cw, new File(Directories.MODS, path + "CreativeTab_" + tab.name + ".class"));
        }
    }
}
