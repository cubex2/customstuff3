package cubex2.cs3.registry;

import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.asm.export.templates.Util;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.GrassPlant;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowGrassPlants;
import cubex2.cs3.lib.Strings;
import net.minecraft.item.ItemStack;
import org.objectweb.asm.tree.InsnList;

public class GrassPlantRegistry extends ContentRegistry<GrassPlant>
{
    public GrassPlantRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public GrassPlant newDataInstance()
    {
        return new GrassPlant(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowGrassPlants(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Grass Plants";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_GRASS_PLANT;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (GrassPlant plant : getContentList())
        {
            ASMUtil.pushStack(list, plant.block);
            ASMUtil.pushInt(list, plant.weight);
            ASMUtil.invokeStatic(list, Util.class, "addFlowerEntry", ItemStack.class, int.class);
        }

    }
}
