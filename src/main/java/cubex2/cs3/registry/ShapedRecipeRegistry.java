package cubex2.cs3.registry;

import cubex2.cs3.asm.export.ASMUtil;
import cubex2.cs3.asm.export.Context;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.ShapedRecipe;
import cubex2.cs3.ingame.gui.Window;
import cubex2.cs3.ingame.gui.WindowShapedRecipes;
import cubex2.cs3.lib.Strings;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.TypeInsnNode;

public class ShapedRecipeRegistry extends ContentRegistry<ShapedRecipe>
{
    public ShapedRecipeRegistry(BaseContentPack pack)
    {
        super(pack);
    }

    @Override
    public ShapedRecipe newDataInstance()
    {
        return new ShapedRecipe(pack);
    }

    @Override
    public Window createListWindow()
    {
        return new WindowShapedRecipes(pack);
    }

    @Override
    public String getNameForEditPack()
    {
        return "Shaped Recipes";
    }

    @Override
    public String getName()
    {
        return Strings.REGISTRY_SHAPED_RECIPE;
    }

    @Override
    public void export(Context context)
    {
        InsnList list = context.init;

        for (ShapedRecipe recipe : getContentList())
        {
            ASMUtil.pushNewInstance(list, ShapedOreRecipe.class);
            list.add(new InsnNode(DUP));
            ASMUtil.pushStack(list, recipe.result);
            ASMUtil.pushObjectArray(list, recipe.createRecipeObjects());
            ASMUtil.invokeSpecial(list, ShapedOreRecipe.class, ItemStack.class, Object[].class);
            ASMUtil.invokeStatic(list, GameRegistry.class, "addRecipe", IRecipe.class);
        }
    }
}
