package cubex2.cs3.compat;

import com.google.common.collect.Sets;
import cubex2.cs3.block.EnumBlockAssets;
import cubex2.cs3.block.EnumBlockType;
import cubex2.cs3.block.attributes.*;
import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.WrappedBlock;
import cubex2.cs3.common.WrappedItem;
import cubex2.cs3.item.attributes.ItemAttributes;
import cubex2.cs3.lib.Directories;
import cubex2.cs3.util.IconWrapper;
import cubex2.cs3.util.ResourceText;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MC18Updater
{
    public static void updatePack(BaseContentPack pack)
    {
        File blockStates = new File(pack.getDirectory(), "assets/" + pack.id.toLowerCase() + "/" + Directories.BLOCK_STATES);
        File blockModels = new File(pack.getDirectory(), "assets/" + pack.id.toLowerCase() + "/" + Directories.MODELS + "/" + Directories.BLOCK_MODELS);
        File itemModels = new File(pack.getDirectory(), "assets/" + pack.id.toLowerCase() + "/" + Directories.MODELS + "/" + Directories.ITEM_MODELS);

        for (WrappedBlock block : pack.getContentRegistry(WrappedBlock.class).getContentList())
        {
            BlockAttributes container = block.container;
            try
            {
                File stateFile = new File(blockStates, block.getName() + ".json");
                File modelFile = new File(blockModels, block.getName() + ".json");
                File itemModelFile = new File(itemModels, "block_" + block.getName() + ".json");

                EnumBlockAssets assets = EnumBlockAssets.fromBlockType(block.getType());

                if (assets.stateName != null)
                {
                    defaultState(block, "state_block_" + assets.stateName, stateFile);
                }

                for (String model : assets.models)
                {
                    String postFix = model.length() == 0 ? "" : "_" + model;
                    defaultModel(block, "model_block_" + assets.modelName + postFix, new File(blockModels, block.getName() + postFix + ".json"));
                }

                if (assets.item != null)
                {
                    if (assets.item.startsWith("ib:"))
                    {
                        String tail = assets.item.substring(3).replace(":", " : ");
                        String[] split = tail.split(":");

                        String template = split[0].trim();
                        if (template.length() == 0)
                            template = "model_itemblock_normal";
                        else
                            template = "model_itemblock_" + template;

                        String postFix = split[1].trim();

                        defaultItemBlockModel(block, template, postFix, itemModelFile);
                    }
                }

                FileWriter w;
                if (block.getType() == EnumBlockType.FACING)
                {
                    if (!container.hasCustomModels || !modelFile.exists())
                    {
                        FacingAttributes facing = (FacingAttributes) container;
                        boolean hasTop = facing.textureTop.iconString.length() > 0;
                        boolean hasBottom = facing.textureBottom.iconString.length() > 0;

                        w = new FileWriter(modelFile);
                        w.write(ResourceText.fromPath("templates/model_block_facing.json")
                                            .replace("%FRONT%", facing.textureFront.getIconForModel("blocks"))
                                            .replace("%BACK%", ((FacingAttributes) container).textureBack.getIconForModel("blocks"))
                                            .replace("%UP%", hasTop ? container.textureTop.getIconForModel("blocks") : facing.textureSides.getIconForModel("blocks"))
                                            .replace("%DOWN%", hasBottom ? container.textureBottom.getIconForModel("blocks") : facing.textureSides.getIconForModel("blocks"))
                                            .replace("%SIDE%", facing.textureSides.getIconForModel("blocks"))
                                            .replace("%PARTICLE%", facing.textureFront.getIconForModel("blocks")));
                        w.close();
                    }

                } else if (block.getType() == EnumBlockType.CROSS_TEXTURE)
                {
                    defaultItemModel(container.textureBottom, "blocks", itemModelFile, container.hasCustomModels);
                } else if (block.getType() == EnumBlockType.CROSS_TEXTURE_POST)
                {
                    CrossTexturePostAttributes post = (CrossTexturePostAttributes) container;

                    if (!container.hasCustomModels || !modelFile.exists())
                    {
                        w = new FileWriter(modelFile);
                        w.write(ResourceText.fromPath("templates/model_block_cross_texture_post.json")
                                            .replace("%SIDES%", post.textureSides.getIconForModel("blocks"))
                                            .replace("%HEIGHT%", "16")
                                            .replace("%THICKNESS_MIN%", "" + (8 - post.thickness * 8))
                                            .replace("%THICKNESS_MAX%", "" + (8 + post.thickness * 8)));
                        w.close();
                    }

                    defaultItemModel(post.textureSides, "blocks", itemModelFile, container.hasCustomModels);
                } else if (block.getType() == EnumBlockType.DOOR)
                {
                    DoorAttributes door = (DoorAttributes) container;

                    defaultItemModel(door.iconFile, "items", itemModelFile, container.hasCustomModels);
                } else if (block.getType() == EnumBlockType.FLUID)
                {
                    FluidAttributes fluid = (FluidAttributes) container;

                    if (!container.hasCustomBlockState || !stateFile.exists())
                    {
                        w = new FileWriter(stateFile);
                        w.write(ResourceText.fromPath("templates/state_block_fluid.json")
                                            .replace("%ID%", block.getPack().id.toLowerCase())
                                            .replace("%BLOCK%", block.getName().toLowerCase(Locale.ENGLISH)));
                        w.close();
                    }

                    defaultItemModel(fluid.textureStill, "blocks", itemModelFile, container.hasCustomModels);
                } else if (block.getType() == EnumBlockType.LADDER)
                {
                    defaultItemModel(container.textureBottom, "blocks", itemModelFile, container.hasCustomModels);
                } else if (block.getType() == EnumBlockType.PANE)
                {
                    defaultItemModel(container.textureBottom, "blocks", itemModelFile, container.hasCustomModels);
                } else if (block.getType() == EnumBlockType.POST)
                {
                    if (!container.hasCustomModels || !modelFile.exists())
                    {
                        PostAttributes post = (PostAttributes) container;
                        boolean hasTop = post.textureTop.iconString.length() > 0;
                        boolean hasBottom = post.textureBottom.iconString.length() > 0;

                        w = new FileWriter(modelFile);
                        w.write(ResourceText.fromPath("templates/model_block_post.json")
                                            .replace("%FRONT%", post.textureFront.getIconForModel("blocks"))
                                            .replace("%BACK%", ((FacingAttributes) container).textureBack.getIconForModel("blocks"))
                                            .replace("%UP%", hasTop ? container.textureTop.getIconForModel("blocks") : post.textureSides.getIconForModel("blocks"))
                                            .replace("%DOWN%", hasBottom ? container.textureBottom.getIconForModel("blocks") : post.textureSides.getIconForModel("blocks"))
                                            .replace("%SIDE%", post.textureSides.getIconForModel("blocks"))
                                            .replace("%PARTICLE%", post.textureFront.getIconForModel("blocks"))
                                            .replace("%HEIGHT%", "16")
                                            .replace("%THICKNESS_MIN%", "" + (8 - post.thickness * 8))
                                            .replace("%THICKNESS_MAX%", "" + (8 + post.thickness * 8)));
                        w.close();
                    }
                } else if (block.getType() == EnumBlockType.TORCH)
                {
                    defaultItemModel(container.textureBottom, "blocks", itemModelFile, container.hasCustomModels);
                }
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        for (WrappedItem item : pack.getContentRegistry(WrappedItem.class).getContentList())
        {
            ItemAttributes container = item.container;
            try
            {
                File modelFile = new File(itemModels, item.getName() + ".json");
                if (container.full3d)
                {
                    defaultItemModel(container.icon, container.renderScale, "model_item_full3d", "items", modelFile, container.hasCustomModel);
                } else
                {
                    defaultItemModel(container.icon, container.renderScale, "model_item_normal", "items", modelFile, container.hasCustomModel);
                }
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private static void defaultModel(WrappedBlock block, String template, File file) throws IOException
    {
        BlockAttributes container = block.container;
        if (container.hasCustomModels && file.exists()) return;

        FileWriter w = new FileWriter(file);
        w.write(ResourceText.fromPath("templates/" + template + ".json")
                            .replace("%ID%", block.getPack().id.toLowerCase())
                            .replace("%BLOCK%", block.getName())
                            .replace("%DOWN%", container.textureBottom.getIconForModel("blocks"))
                            .replace("%UP%", container.textureTop.getIconForModel("blocks"))
                            .replace("%NORTH%", container.textureNorth.getIconForModel("blocks"))
                            .replace("%EAST%", container.textureEast.getIconForModel("blocks"))
                            .replace("%SOUTH%", container.textureSouth.getIconForModel("blocks"))
                            .replace("%WEST%", container.textureWest.getIconForModel("blocks"))
                            .replace("%PARTICLE%", container.textureNorth.getIconForModel("blocks")));
        w.close();
    }

    private static void defaultState(WrappedBlock block, String template, File file) throws IOException
    {
        if (block.container.hasCustomBlockState && file.exists()) return;

        FileWriter w = new FileWriter(file);
        w.write(ResourceText.fromPath("templates/" + template + ".json")
                            .replace("%ID%", block.getPack().id.toLowerCase())
                            .replace("%BLOCK%", block.getName()));
        w.close();
    }

    private static void defaultItemModel(IconWrapper icon, String folder, File file, boolean hasCustomModel) throws IOException
    {
        defaultItemModel(icon, "model_item_normal", folder, file, hasCustomModel);
    }

    private static void defaultItemModel(IconWrapper icon, String template, String folder, File file, boolean hasCustomModel) throws IOException
    {
        defaultItemModel(icon, 1f, template, folder, file, hasCustomModel);
    }

    private static void defaultItemModel(IconWrapper icon, float scale, String template, String folder, File file, boolean hasCustomModel) throws IOException
    {
        if (hasCustomModel && file.exists()) return;

        String temp = ResourceText.fromPath("templates/" + template + ".json");
        Matcher m = Pattern.compile("%SCALE:([0-9]+\\.[0-9]+)%").matcher(temp);
        Set<String> set = Sets.newHashSet();
        while (m.find())
        {
            set.add(m.group());
        }
        for (String s : set)
        {
            String floatString = s.split(":")[1];
            floatString = floatString.substring(0, floatString.length() - 1);
            temp = temp.replace(s, "" + scale * Float.parseFloat(floatString));
        }

        FileWriter w = new FileWriter(file);
        w.write(temp.replace("%TEXTURE%", icon.getIconForModel(folder)));
        w.close();
    }

    private static void defaultItemBlockModel(WrappedBlock block, File file) throws IOException
    {
        defaultItemBlockModel(block, "model_itemblock_normal", "", file);
    }

    private static void defaultItemBlockModel(WrappedBlock block, String template, String parentPostfix, File file) throws IOException
    {
        if (block.container.hasCustomBlockState && file.exists()) return;

        FileWriter w = new FileWriter(file);
        w.write(ResourceText.fromPath("templates/" + template + ".json")
                            .replace("%ID%", block.getPack().id.toLowerCase())
                            .replace("%BLOCK%", block.getName() + parentPostfix));
        w.close();
    }
}
