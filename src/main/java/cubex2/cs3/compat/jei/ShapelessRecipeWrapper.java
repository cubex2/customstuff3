package cubex2.cs3.compat.jei;

import cubex2.cs3.common.ShapelessRecipe;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.BlankRecipeWrapper;
import mezz.jei.api.recipe.IStackHelper;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ShapelessRecipeWrapper extends BlankRecipeWrapper
{
    private final ShapelessRecipe recipe;
    private final IJeiHelpers jeiHelpers;

    public ShapelessRecipeWrapper(IJeiHelpers jeiHelpers, ShapelessRecipe recipe)
    {
        this.recipe = recipe;
        this.jeiHelpers = jeiHelpers;
    }

    @Override
    public void getIngredients(IIngredients ingredients)
    {
        IStackHelper stackHelper = jeiHelpers.getStackHelper();
        ItemStack recipeOutput = recipe.getRecipeOutput();

        try
        {
            List<List<ItemStack>> inputs = stackHelper.expandRecipeItemStackInputs(recipe.inputArray);
            ingredients.setInputLists(ItemStack.class, inputs);
            if (recipeOutput != null)
            {
                ingredients.setOutput(ItemStack.class, recipeOutput);
            }
        } catch (RuntimeException e)
        {
            e.printStackTrace();
        }
    }


}
