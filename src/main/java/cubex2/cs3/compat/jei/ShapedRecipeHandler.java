package cubex2.cs3.compat.jei;

import cubex2.cs3.common.ShapedRecipe;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;
import mezz.jei.api.recipe.VanillaRecipeCategoryUid;

import java.util.List;

public class ShapedRecipeHandler implements IRecipeHandler<ShapedRecipe>
{
    private final IJeiHelpers jeiHelpers;

    public ShapedRecipeHandler(IJeiHelpers jeiHelpers) {this.jeiHelpers = jeiHelpers;}

    @Override
    public Class<ShapedRecipe> getRecipeClass()
    {
        return ShapedRecipe.class;
    }

    @Override
    public String getRecipeCategoryUid(ShapedRecipe recipe)
    {
        return VanillaRecipeCategoryUid.CRAFTING;
    }

    @Override
    public IRecipeWrapper getRecipeWrapper(ShapedRecipe recipe)
    {
        return new ShapedRecipeWrapper(jeiHelpers, recipe);
    }

    @Override
    public boolean isRecipeValid(ShapedRecipe recipe)
    {
        if (recipe.getRecipeOutput() == null)
        {
            return false;
        }
        int inputCount = 0;
        for (Object input : recipe.inputArray)
        {
            if (input instanceof List)
            {
                if (((List) input).isEmpty())
                {
                    // missing items for an oreDict name. This is normal behavior, but the recipe is invalid.
                    return false;
                }
            }
            if (input != null)
            {
                inputCount++;
            }
        }
        if (inputCount > 9)
        {
            return false;
        }
        if (inputCount == 0)
        {
            return false;
        }
        return true;
    }
}
