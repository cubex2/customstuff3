package cubex2.cs3.compat.jei;

import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.BaseContentPackLoader;
import cubex2.cs3.common.ShapedRecipe;
import cubex2.cs3.common.ShapelessRecipe;
import cubex2.cs3.registry.ContentRegistry;
import mezz.jei.api.BlankModPlugin;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;

import java.util.stream.Collectors;

@JEIPlugin
public class JeiPlugin extends BlankModPlugin
{
    @Override
    public void register(IModRegistry registry)
    {
        IJeiHelpers jeiHelpers = registry.getJeiHelpers();

        registry.addRecipeHandlers(new ShapedRecipeHandler(jeiHelpers));
        registry.addRecipeHandlers(new ShapelessRecipeHandler(jeiHelpers));

        for (BaseContentPack pack : BaseContentPackLoader.instance().getContentPacks())
        {
            {
                ContentRegistry<ShapedRecipe> reg = pack.getContentRegistry(ShapedRecipe.class);
                registry.addRecipes(reg.getContentList().stream().map(r -> new ShapedRecipeWrapper(jeiHelpers, r)).collect(Collectors.toList()));
            }

            {
                ContentRegistry<ShapelessRecipe> reg = pack.getContentRegistry(ShapelessRecipe.class);
                registry.addRecipes(reg.getContentList().stream().map(r -> new ShapelessRecipeWrapper(jeiHelpers, r)).collect(Collectors.toList()));
            }

        }
    }
}
