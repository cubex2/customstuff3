package cubex2.cs3.compat.jei;

import cubex2.cs3.common.ShapedRecipe;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.BlankRecipeWrapper;
import mezz.jei.api.recipe.IStackHelper;
import mezz.jei.api.recipe.wrapper.IShapedCraftingRecipeWrapper;
import net.minecraft.item.ItemStack;

import java.util.Arrays;
import java.util.List;

public class ShapedRecipeWrapper extends BlankRecipeWrapper implements IShapedCraftingRecipeWrapper
{
    private final ShapedRecipe recipe;
    private final IJeiHelpers jeiHelpers;

    public ShapedRecipeWrapper(IJeiHelpers jeiHelpers, ShapedRecipe recipe)
    {
        this.recipe = recipe;
        this.jeiHelpers = jeiHelpers;
    }

    @Override
    public int getWidth()
    {
        return recipe.width;
    }

    @Override
    public int getHeight()
    {
        return recipe.height;
    }

    @Override
    public void getIngredients(IIngredients ingredients)
    {
        IStackHelper stackHelper = jeiHelpers.getStackHelper();
        ItemStack recipeOutput = recipe.getRecipeOutput();

        try
        {
            List<List<ItemStack>> inputs = stackHelper.expandRecipeItemStackInputs(Arrays.asList(recipe.inputArray));
            ingredients.setInputLists(ItemStack.class, inputs);
            if (recipeOutput != null)
            {
                ingredients.setOutput(ItemStack.class, recipeOutput);
            }
        } catch (RuntimeException e)
        {
            e.printStackTrace();
        }
    }


}
