package cubex2.cs3.compat.jei;

import cubex2.cs3.common.ShapelessRecipe;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;
import mezz.jei.api.recipe.VanillaRecipeCategoryUid;

import java.util.List;

public class ShapelessRecipeHandler implements IRecipeHandler<ShapelessRecipe>
{
    private final IJeiHelpers jeiHelpers;

    public ShapelessRecipeHandler(IJeiHelpers jeiHelpers) {this.jeiHelpers = jeiHelpers;}

    @Override
    public Class<ShapelessRecipe> getRecipeClass()
    {
        return ShapelessRecipe.class;
    }

    @Override
    public String getRecipeCategoryUid(ShapelessRecipe recipe)
    {
        return VanillaRecipeCategoryUid.CRAFTING;
    }

    @Override
    public IRecipeWrapper getRecipeWrapper(ShapelessRecipe recipe)
    {
        return new ShapelessRecipeWrapper(jeiHelpers, recipe);
    }

    @Override
    public boolean isRecipeValid(ShapelessRecipe recipe)
    {
        if (recipe.getRecipeOutput() == null)
        {
            return false;
        }
        int inputCount = 0;
        for (Object input : recipe.inputArray)
        {
            if (input instanceof List)
            {
                if (((List) input).isEmpty())
                {
                    // missing items for an oreDict name. This is normal behavior, but the recipe is invalid.
                    return false;
                }
            }
            if (input != null)
            {
                inputCount++;
            }
        }
        if (inputCount > 9)
        {
            return false;
        }
        if (inputCount == 0)
        {
            return false;
        }
        return true;
    }
}
