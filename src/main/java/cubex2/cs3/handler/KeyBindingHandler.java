package cubex2.cs3.handler;


import cubex2.cs3.Config;
import cubex2.cs3.ingame.gui.GuiBase;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

@SideOnly(Side.CLIENT)
public class KeyBindingHandler
{
    public static final KeyBinding openGuiKey = new KeyBinding("CS3", Keyboard.KEY_C, "General");

    public static final KeyBindingHandler INSTANCE = new KeyBindingHandler();

    private KeyBindingHandler()
    {

    }

    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event)
    {
        if (Config.GUI_ENABLED && event.phase == TickEvent.Phase.END && event.side == Side.CLIENT && openGuiKey.isPressed() && FMLClientHandler.instance().getClient().inGameHasFocus && FMLClientHandler.instance().getClient().isSingleplayer())
        {
            Keyboard.enableRepeatEvents(true);
            FMLClientHandler.instance().showGuiScreen(GuiBase.INSTANCE);
        }
    }
}
