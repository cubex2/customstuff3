package cubex2.cs3.handler.event;

import cubex2.cs3.common.BaseContentPack;
import cubex2.cs3.common.BaseContentPackLoader;
import cubex2.cs3.common.WrappedItem;
import cubex2.cs3.item.EnumItemType;
import cubex2.cs3.item.ItemCSBucket;
import cubex2.cs3.item.attributes.BucketAttributes;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class FillBucketHandler
{
    @SubscribeEvent
    public void onFillBucket(FillBucketEvent event)
    {
        if (event.getTarget() == null || event.getTarget().typeOfHit != RayTraceResult.Type.BLOCK)
            return;

        World world = event.getWorld();
        BlockPos pos = event.getTarget().getBlockPos();
        IBlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        for (BaseContentPack pack : BaseContentPackLoader.instance().getContentPacks())
        {
            for (WrappedItem wrappedItem : pack.getContentRegistry(WrappedItem.class).getContentList())
            {
                if (wrappedItem.getType() == EnumItemType.BUCKET)
                {
                    ItemCSBucket bucket = (ItemCSBucket) wrappedItem.item;
                    if (block == ((BucketAttributes) wrappedItem.container).fluid && state.getValue(BlockLiquid.LEVEL) == 0)
                    {
                        world.setBlockToAir(event.getTarget().getBlockPos());
                        event.setFilledBucket(new ItemStack(bucket));
                        event.setResult(Event.Result.ALLOW);
                        return;
                    }
                }
            }
        }
    }
}
