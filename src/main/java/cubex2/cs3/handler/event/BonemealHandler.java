package cubex2.cs3.handler.event;

import cubex2.cs3.block.IBlockCS;
import net.minecraftforge.event.entity.player.BonemealEvent;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class BonemealHandler
{
    @SubscribeEvent
    public void onUseBonemeal(BonemealEvent event)
    {
        if (event.getBlock()instanceof IBlockCS)
        {
            IBlockCS block = (IBlockCS) event.getBlock();
            if (block.getWrappedBlock().onBonemeal(event.getWorld(), event.getPos(), event.getEntityPlayer()))
            {
                event.setResult(Event.Result.ALLOW);
            }
        }
    }
}
