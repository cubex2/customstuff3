package cubex2.cs3;

import cubex2.cs3.asm.ICSMod;
import cubex2.cs3.common.BaseContentPackLoader;
import cubex2.cs3.common.ShapedRecipe;
import cubex2.cs3.common.ShapelessRecipe;
import cubex2.cs3.lib.Directories;
import cubex2.cs3.lib.ModInfo;
import cubex2.cs3.network.PacketOpenUserContainerGuiClient;
import cubex2.cs3.network.PacketOpenUserContainerGuiServer;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.oredict.RecipeSorter;
import org.apache.logging.log4j.Logger;

import static net.minecraftforge.oredict.RecipeSorter.Category.SHAPED;
import static net.minecraftforge.oredict.RecipeSorter.Category.SHAPELESS;

@Mod(modid = ModInfo.ID, name = ModInfo.NAME, version = ModInfo.VERSION, dependencies = ModInfo.DEPENDENCIES)
public class CustomStuff3
{
    @Mod.Instance(ModInfo.ID)
    public static CustomStuff3 instance;

    @SidedProxy(clientSide = "cubex2.cs3.ClientProxy", serverSide = "cubex2.cs3.CommonProxy")
    public static CommonProxy proxy;

    public static Logger logger;

    public static SimpleNetworkWrapper packetPipeline;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        logger = FMLLog.getLogger();
        packetPipeline = NetworkRegistry.INSTANCE.newSimpleChannel("CustomStuff3");
        registerPackets();

        Directories.init(event.getModConfigurationDirectory().getParentFile());

        Config.init(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        RecipeSorter.register("CS3:Shaped", ShapedRecipe.class, SHAPED, "after:minecraft:shaped before:minecraft:shapeless");
        RecipeSorter.register("CS3:Shapeless", ShapelessRecipe.class, SHAPELESS, "after:minecraft:shapeless");

        proxy.registerEntities();
        proxy.registerTileEntities();
        proxy.initRendering();
        proxy.registerKeyBindings();
        proxy.registerEventHandlers();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {

    }

    public static void onPreInitPack(ICSMod pack)
    {
        BaseContentPackLoader.instance().onPreInitPack(pack);
    }

    public static void onInitPack(ICSMod pack)
    {
        BaseContentPackLoader.instance().onInitPack(pack);
    }

    public static void onPostInitPack(ICSMod pack)
    {
        BaseContentPackLoader.instance().onPostInitPack(pack);
    }

    private void registerPackets()
    {
        registerPacket(PacketOpenUserContainerGuiServer.Handler.class, PacketOpenUserContainerGuiServer.class, Side.SERVER);
        registerPacket(PacketOpenUserContainerGuiClient.Handler.class, PacketOpenUserContainerGuiClient.class, Side.CLIENT);
    }

    public static <REQ extends IMessage, REPLY extends IMessage> void registerPacket(Class<? extends IMessageHandler<REQ, REPLY>> messageHandler, Class<REQ> requestMessageType, Side side)
    {
        packetPipeline.registerMessage(messageHandler, requestMessageType, nextPacketId(), side);
    }

    private static int packetId = Byte.MIN_VALUE;

    private static int nextPacketId()
    {
        if (packetId >= Byte.MAX_VALUE + 1)
        {
            throw new RuntimeException("No more packet ids left!");
        }
        return packetId++;
    }
}
