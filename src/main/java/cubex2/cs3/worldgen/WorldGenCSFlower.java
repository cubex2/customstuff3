package cubex2.cs3.worldgen;

import cubex2.cs3.block.BlockCS;
import cubex2.cs3.block.BlockCSFlat;
import cubex2.cs3.common.WrappedWorldGen;
import cubex2.cs3.util.SimulatedWorld;
import cubex2.cs3.worldgen.attributes.WorldGenFlowerAttributes;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkGenerator;
import net.minecraft.world.chunk.IChunkProvider;

import java.util.Random;

public class WorldGenCSFlower extends WorldGenCS
{
    private WorldGenFlowerAttributes worldGen;

    public WorldGenCSFlower(WrappedWorldGen wrappedWorldGen)
    {
        super(wrappedWorldGen);
        this.worldGen = (WorldGenFlowerAttributes) wrappedWorldGen.container;
    }

    @Override
    protected void doGenerate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider)
    {
        int generations = MathHelper.floor(worldGen.generationsPerChunk);
        for (int i = 0; i < generations; i++)
        {
            int x = chunkX * 16 + random.nextInt(16) + 8;
            int y = random.nextInt(worldGen.maxHeight + 1 - worldGen.minHeight) + worldGen.minHeight;
            int z = chunkZ * 16 + random.nextInt(16) + 8;
            if (shouldGenerate(x, z, world))
            {
                generateAt(random, world, x, y, z);
            }
        }
        if (random.nextFloat() <= worldGen.generationsPerChunk - generations)
        {
            int x = chunkX * 16 + random.nextInt(16) + 8;
            int y = random.nextInt(worldGen.maxHeight + 1 - worldGen.minHeight) + worldGen.minHeight;
            int z = chunkZ * 16 + random.nextInt(16) + 8;
            if (shouldGenerate(x, z, world))
            {
                generateAt(random, world, x, y, z);
            }
        }
    }

    public void generateAt(Random random, World world, int x, int y, int z)
    {
        Block block = Block.getBlockFromItem(container.generatedBlock.getItem());
        int meta = container.generatedBlock.getItemDamage();
        if (block instanceof BlockCS)
        {
            meta = ((BlockCS) block).getMetaForFlowerGen(meta);
        }

        for (int i = 0; i < worldGen.blockRate; ++i)
        {
            int genX = x + random.nextInt(8) - random.nextInt(8);
            int genY = y + random.nextInt(4) - random.nextInt(4);
            int genZ = z + random.nextInt(8) - random.nextInt(8);

            BlockPos pos = new BlockPos(genX, genY, genZ);
            if (world.isAirBlock(pos) && canPlaceBlock(world, pos.down()))
            {
                boolean canStay = block instanceof BlockCSFlat ? world.isSideSolid(pos.down(), EnumFacing.UP) : block.canPlaceBlockAt(world, pos);
                if (canStay && canPlaceBlock(world, pos.down()))
                {
                    world.setBlockState(pos, block.getStateFromMeta(meta), 2);
                }
            }
        }
    }

    private boolean canPlaceBlock(World world, BlockPos pos)
    {
        IBlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        if (block != null)
        {
            return canReplaceBlock(world.provider.getDimension(), block, block.getMetaFromState(state));
        }
        return false;
    }

    @Override
    public void generateInSimWorld(SimulatedWorld world, Random random)
    {

    }
}
