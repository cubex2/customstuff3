package cubex2.cs3.tileentity;

import com.google.common.collect.Maps;
import cubex2.cs3.common.WrappedTileEntity;
import cubex2.cs3.tileentity.attributes.TileEntityInventoryAttributes;
import cubex2.cs3.tileentity.data.FurnaceModule;
import cubex2.cs3.util.Util;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;

import java.util.Map;

public class TileEntityInventory extends TileEntityCS implements IInventory
{

    public NonNullList<ItemStack> inventoryContents;
    private int slotCount;
    private TileEntityInventoryAttributes invContainer;

    private Map<FurnaceModule, FurnaceData> furnaceData = Maps.newHashMap();

    public TileEntityInventory(WrappedTileEntity wrappedTileEntity)
    {
        super(wrappedTileEntity);
        invContainer = (TileEntityInventoryAttributes) wrappedTileEntity.container;
        slotCount = invContainer.slotCount;
        inventoryContents = NonNullList.withSize(slotCount, ItemStack.EMPTY);
    }

    public TileEntityInventory()
    {
    }

    public FurnaceData getFurnaceData(FurnaceModule module)
    {
        return furnaceData.get(module);
    }

    public int getCookProgressScaled(String name, int width)
    {
        FurnaceModule module = invContainer.furnaceModules.getModule(name);
        if (module == null)
            return 0;
        return furnaceData.get(module).cookTime * width / module.cookTime;
    }

    public int getBurnTimeRemainingScaled(String name, int width)
    {
        FurnaceModule module = invContainer.furnaceModules.getModule(name);
        if (module == null)
            return 0;

        FurnaceData data = getFurnaceData(module);

        if (data.currentBurnTime == 0)
        {
            data.currentBurnTime = module.cookTime;
        }

        return data.burnTime * width / data.currentBurnTime;
    }

    public TileEntityInventoryAttributes getContainer()
    {
        return invContainer;
    }


    @Override
    public void update()
    {
        super.update();

        for (FurnaceModule module : invContainer.furnaceModules.list)
        {
            FurnaceData data = furnaceData.get(module);
            if (data == null)
            {
                data = new FurnaceData(0, 0, 0);
                furnaceData.put(module, data);
            }

            boolean flag = data.burnTime > 0;
            boolean flag1 = false;

            if (data.burnTime > 0)
            {
                --data.burnTime;
            }

            if (!world.isRemote)
            {
                if (data.burnTime != 0 || !inventoryContents.get(module.fuelSlot).isEmpty() && !inventoryContents.get(module.inputSlot).isEmpty())
                {
                    if (data.burnTime == 0 && canSmelt(module))
                    {
                        data.currentBurnTime = data.burnTime = invContainer.getPack().fuelHandler.getItemBurnTime(inventoryContents.get(module.fuelSlot), module.fuelList);

                        if (data.burnTime > 0)
                        {
                            flag1 = true;

                            if (!this.inventoryContents.get(module.fuelSlot).isEmpty())
                            {
                                this.inventoryContents.get(module.fuelSlot).shrink(1);

                                if (this.inventoryContents.get(module.fuelSlot).getCount() == 0)
                                {
                                    this.inventoryContents.set(module.fuelSlot, inventoryContents.get(module.fuelSlot).getItem().getContainerItem(inventoryContents.get(module.fuelSlot)));
                                }
                            }
                        }
                    }

                    if (isBurning(data) && canSmelt(module))
                    {
                        ++data.cookTime;

                        if (data.cookTime == module.cookTime)
                        {
                            data.cookTime = 0;
                            smeltItem(module);
                            flag1 = true;
                        }
                    } else
                    {
                        data.cookTime = 0;
                    }
                }

                if (flag != data.burnTime > 0)
                {
                    flag1 = true;
                    // TODO update furnace state
                }
            }

            if (flag1)
            {
                markDirty();
            }
        }
    }

    public void smeltItem(FurnaceModule module)
    {
        if (this.canSmelt(module))
        {
            ItemStack itemstack = invContainer.getPack().smeltingRecipeHandler.getSmeltingResult(inventoryContents.get(module.inputSlot), module.recipeList);

            if (this.inventoryContents.get(module.outputSlot).isEmpty())
            {
                this.inventoryContents.set(module.outputSlot, itemstack.copy());
            } else if (this.inventoryContents.get(module.outputSlot).getItem() == itemstack.getItem())
            {
                this.inventoryContents.get(module.outputSlot).grow(itemstack.getCount());// Forge BugFix: Results may have multiple items
            }

            this.inventoryContents.get(module.inputSlot).shrink(1);

            if (this.inventoryContents.get(module.inputSlot).getCount() <= 0)
            {
                this.inventoryContents.set(module.inputSlot, ItemStack.EMPTY);
            }
        }
    }

    private boolean isBurning(FurnaceData data)
    {
        return data.burnTime > 0;
    }

    private boolean canSmelt(FurnaceModule module)
    {
        if (this.inventoryContents.get(module.inputSlot).isEmpty())
        {
            return false;
        } else
        {
            ItemStack itemstack = invContainer.getPack().smeltingRecipeHandler.getSmeltingResult(inventoryContents.get(module.inputSlot), module.recipeList);
            if (itemstack.isEmpty()) return false;
            if (this.inventoryContents.get(module.outputSlot).isEmpty()) return true;
            if (!this.inventoryContents.get(module.outputSlot).isItemEqual(itemstack)) return false;
            int result = inventoryContents.get(module.outputSlot).getCount() + itemstack.getCount();
            return result <= getInventoryStackLimit() && result <= this.inventoryContents.get(module.outputSlot).getMaxStackSize(); //Forge BugFix: Make it respect stack sizes properly.
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        invContainer = (TileEntityInventoryAttributes) container;

        slotCount = invContainer.slotCount;
        inventoryContents = NonNullList.withSize(slotCount, ItemStack.EMPTY);

        Util.readStacksFromNBT("CS3_Inv_Items", inventoryContents, compound);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);

        Util.writeStacksToNBT("CS3_Inv_Items", inventoryContents, compound);

        return compound;
    }

    @Override
    public int getSizeInventory()
    {
        return slotCount;
    }

    @Override
    public ItemStack getStackInSlot(int slot)
    {
        return slot >= 0 && slot < this.inventoryContents.size() ? this.inventoryContents.get(slot) : null;
    }

    @Override
    public ItemStack decrStackSize(int slot, int amount)
    {
        return ItemStackHelper.getAndSplit(inventoryContents, slot, amount);
    }

    @Override
    public ItemStack removeStackFromSlot(int slot)
    {
        return ItemStackHelper.getAndRemove(inventoryContents, slot);
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack stack)
    {
        this.inventoryContents.set(slot, stack);

        if (!stack.isEmpty() && stack.getCount() > this.getInventoryStackLimit())
        {
            stack.setCount(this.getInventoryStackLimit());
        }

        this.markDirty();
    }

    @Override
    public String getName()
    {
        return "invCS3TileEntity";
    }

    @Override
    public boolean hasCustomName()
    {
        return false;
    }

    @Override
    public ITextComponent getDisplayName()
    {
        return null;
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player)
    {
        return this.world.getTileEntity(getPos()) == this && player.getDistanceSq((double) getPos().getX() + 0.5D, (double) getPos().getY() + 0.5D, (double) getPos().getZ() + 0.5D) <= 64.0D;
    }

    @Override
    public void openInventory(EntityPlayer player)
    {

    }

    @Override
    public void closeInventory(EntityPlayer player)
    {

    }

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack)
    {
        return true;
    }

    @Override
    public int getField(int id)
    {
        return 0;
    }

    @Override
    public void setField(int id, int value)
    {

    }

    @Override
    public int getFieldCount()
    {
        return 0;
    }

    @Override
    public void clear()
    {
        inventoryContents.clear();
    }

    @Override
    public boolean isEmpty()
    {
        for (ItemStack stack : inventoryContents)
        {
            if (!stack.isEmpty())
                return false;
        }
        return true;
    }

    public static class FurnaceData
    {
        public int burnTime;
        public int currentBurnTime;
        public int cookTime;

        public FurnaceData(int burnTime, int currentBurnTime, int cookTime)
        {
            this.burnTime = burnTime;
            this.currentBurnTime = currentBurnTime;
            this.cookTime = cookTime;
        }
    }
}
